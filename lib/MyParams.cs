﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cotton.lib
{
    public class MyParams
    {
        public String paramsName;
        public String paramsValue;
        public MyParams(String arg1, String arg2)
        {
            this.paramsName = arg1;
            this.paramsValue = arg2;
        }
    }
}