﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Configuration;

namespace cotton.lib
{
    public class EmailClass
    {
        //        String FromName = ConfigurationSettings.AppSettings["email_user_from_name"];
        //        String FromEmail = ConfigurationSettings.AppSettings["email_user_from_email"];
        //        String CC = ConfigurationSettings.AppSettings["email_user_cc"];
        public static String getUrl() {
            return Properties.Settings.Default.link;
        }
        public static String SendMail(string toList,string subject, string body)//, string addFile
        {
//            MailMessage message = new MailMessage();
//            SmtpClient smtpClient = new SmtpClient();
            string msg = string.Empty;
            try
            {
                /*MailAddress fromAddress = new MailAddress("admin" + "<triadi.aprillianto@icsp.co.id>");
                //MailAddress fromAddress = new MailAddress("Admin" + "<admin.mectools@unilever.com>");
                message.From = fromAddress;
                message.To.Add("triadi.aprillianto@gmail.com");
                message.ReplyToList.Add(fromAddress);
                //message.CC.Add(Properties.Settings.Default.default_cc);
                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = body;

                //Attachment attachFile = new Attachment("E:\\upload\\" + addFile);
                //message.Attachments.Add(attachFile);

                smtpClient.Host = "mail.icsp.co.id";
                //smtpClient.Host = "smtp-in.unilever.com";
                smtpClient.Port = 25;
                smtpClient.EnableSsl = false;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new System.Net.NetworkCredential("triadi.aprillianto@icsp.co.id", "KeepITfun");
                smtpClient.Send(message); 
                */
				body = body.Replace("'","''");
                MyDatabase.FastExec("DECLARE @RC int DECLARE @From varchar(500) DECLARE @To varchar(500) DECLARE @CC varchar(500) DECLARE @Subject varchar(100) DECLARE @Body varchar(8000) DECLARE @IsHTML bit EXECUTE @RC = sp_send_cdomail @From = 'cotton.admin@unilever.com',@To = '" + toList + "',@CC = '',@Subject = '" + subject + "',@Body = '" + body + "',@IsHTML = 1");
                msg = "Ok";
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return msg;
        }
        public static String SendMailCC(string toList,string cc, string subject, string body)//, string addFile
        {
            //            MailMessage message = new MailMessage();
            //            SmtpClient smtpClient = new SmtpClient();
            string msg = string.Empty;
            try
            {
                /*MailAddress fromAddress = new MailAddress("admin" + "<triadi.aprillianto@icsp.co.id>");
                //MailAddress fromAddress = new MailAddress("Admin" + "<admin.mectools@unilever.com>");
                message.From = fromAddress;
                message.To.Add("triadi.aprillianto@gmail.com");
                message.ReplyToList.Add(fromAddress);
                //message.CC.Add(Properties.Settings.Default.default_cc);
                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = body;

                //Attachment attachFile = new Attachment("E:\\upload\\" + addFile);
                //message.Attachments.Add(attachFile);

                smtpClient.Host = "mail.icsp.co.id";
                //smtpClient.Host = "smtp-in.unilever.com";
                smtpClient.Port = 25;
                smtpClient.EnableSsl = false;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new System.Net.NetworkCredential("triadi.aprillianto@icsp.co.id", "KeepITfun");
                smtpClient.Send(message); 
                */
                body = body.Replace("'", "''");
                MyDatabase.FastExec("DECLARE @RC int DECLARE @From varchar(500) DECLARE @To varchar(500) DECLARE @CC varchar(500) DECLARE @Subject varchar(100) DECLARE @Body varchar(8000) DECLARE @IsHTML bit EXECUTE @RC = sp_send_cdomail @From = 'cotton.admin@unilever.com',@To = '" + toList + "',@CC = '"+ cc + "',@Subject = '" + subject + "',@Body = '" + body + "',@IsHTML = 1");
                msg = "Ok";
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return msg;
        }
    }
}
