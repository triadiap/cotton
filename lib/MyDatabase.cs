﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace cotton.lib
{
    public class MyDatabase
    {
        SqlConnection con;
        SqlDataReader reader;
        public static String username = "";
        public static void FastExec(String sql)
        {
            SqlConnection fCon = new SqlConnection();
            fCon.ConnectionString = Properties.Settings.Default.connectionString;
			//sql = sql.Replace("'", "''");
            SqlCommand fCommand = new SqlCommand(sql, fCon);
            fCommand.CommandTimeout = 0;
            fCon.Open();
            fCommand.ExecuteNonQuery();
            fCon.Close();
        }
        public static void FastExec(String sql, MyParams[] sqlParams)
        {
            SqlConnection fCon = new SqlConnection();
            fCon.ConnectionString = Properties.Settings.Default.connectionString;
            SqlCommand fCommand = new SqlCommand(sql, fCon);
            for (int i = 0; i < sqlParams.Length; i++)
            {
                fCommand.Parameters.Add(sqlParams[i].paramsName, SqlDbType.VarChar);
                fCommand.Parameters[sqlParams[i].paramsName].Value = sqlParams[i].paramsValue;
            }
            fCommand.CommandTimeout = 0;
            fCon.Open();
            fCommand.ExecuteNonQuery();
            fCon.Close();
        }
        public MyDatabase()
        {
            con = new SqlConnection();
            con.ConnectionString = Properties.Settings.Default.connectionString;
        }
        public SqlDataReader Query(string strQuery)
        {
            SqlCommand cmd = new SqlCommand(strQuery, con);
			cmd.CommandTimeout = 0;
            con.Open();
            reader = cmd.ExecuteReader();
            return reader;
        }
        public void ExecQuery(string strQuery)
        {
            SqlCommand cmd = new SqlCommand(strQuery, con);
            con.Open();
            cmd.ExecuteNonQuery();
        }
        public void Close()
        {
            if (reader != null)
            {
                reader.Close();
            }
            con.Close();
        }
        public static String DBNull(object tmp)
        {
            if (tmp == null)
            {
                return "";
            }
            else
            {
                return tmp.ToString();
            }
        }
        public static string getHash(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
        public static int getNumber(String type,String year) {
            String number = "";
            int intNumber = -1;
            SqlConnection fCon = new SqlConnection();
            fCon.ConnectionString = Properties.Settings.Default.connectionString;
            SqlCommand fCommand = new SqlCommand("select * from t_generate_number where [type]='"+type+"' and [year] = '"+year+"'", fCon);
            fCommand.CommandTimeout = 0;
            fCon.Open();
            SqlDataReader freader = fCommand.ExecuteReader();
            while (freader.Read()) {
                intNumber = Convert.ToInt32(freader["seq"].ToString());
            }
            freader.Close();
            fCon.Close();
            if (intNumber == -1)
            {
                intNumber = 1;
                FastExec("insert into t_generate_number ([type],[year],seq) values('" + type + "','" + year + "','" + intNumber + "')");
            }
            else
            {
                intNumber ++;
                FastExec("update t_generate_number set seq = '"+intNumber+"' where [type] = '"+type+"' and [year] = '"+year+"'");
            }
            return intNumber;
        }
    }
}