 function removeCharacter(v, ch)
 {
  var tempValue = v+"";
  var becontinue = true;
  while(becontinue == true)
  {
   var point = tempValue.indexOf(ch);
   if( point >= 0 )
   {
    var myLen = tempValue.length;
    tempValue = tempValue.substr(0,point)+tempValue.substr(point+1,myLen);
    becontinue = true;
   }else{
    becontinue = false;
   }
  }
  return tempValue;
 }
 
 /*
 *  using by function ThausandSeperator!!
 */
 function characterControl(value)
 {
  var tempValue = "";
  var len = value.length;
  for(i=0; i<len; i++)
  {
   var chr = value.substr(i,1);
   if( (chr < '0' || chr > '9') && chr != '.' && chr != ',' )
   {
    chr = '';
   }
   
   tempValue = tempValue + chr;
  }
  return tempValue;
 }
 function characterControl2(value)
 {
  var tempValue = "";
  var len = value.length;
  for(i=0; i<len; i++)
  {
   var chr = value.substr(i,1);
   if( (chr < '0' || chr > '9') && chr != '.' )
   {
    chr = '';
   }
   
   tempValue = tempValue + chr;
  }
  return tempValue;
 }
 
 /*
 * Automaticly converts the value in the textbox in a currency format with
 * thousands seperator and decimal point
 *
 * @param value : the input text
 * @param digit : decimal number after comma
 */
 function ThausandSeperator(value, digit)
 {
  var thausandSepCh = ".";
  var decimalSepCh = ",";
  
  var tempValue = "";
  var realValue = value+"";
  var devValue = "";
  realValue = characterControl(realValue);
  var comma = realValue.indexOf(decimalSepCh);
  if(comma != -1 )
  {
   tempValue = realValue.substr(0,comma);
   devValue = realValue.substr(comma);
   devValue = removeCharacter(devValue,thausandSepCh);
   devValue = removeCharacter(devValue,decimalSepCh);
   devValue = decimalSepCh+devValue;
   if( devValue.length > 5)
   {
    devValue = devValue.substr(0,5);
   }
  }else{
   tempValue = realValue;
  } tempValue = removeCharacter(tempValue,thausandSepCh);
   
  var result = "";
  var len = tempValue.length;
  while (len > 3){
  result = thausandSepCh+tempValue.substr(len-3,3)+result;
  len -=3;
 }
 result = tempValue.substr(0,len)+result;
// if (value<0){
//	return '('+result+devValue+')'
// }
// else{
//alert(result+devValue);

	return result+devValue;
 

// }
 }
 function ThausandSeperatorNoDesimal(value)
 {
  var thausandSepCh = ".";
  var decimalSepCh = ",";
  
  var tempValue = "";
  var realValue = value+"";
  var devValue = "";
  realValue = characterControl2(realValue);
  var comma = realValue.indexOf(decimalSepCh);
  if(comma != -1 )
  {
   tempValue = realValue.substr(0,comma);
   devValue = realValue.substr(comma);
   devValue = removeCharacter(devValue,thausandSepCh);
   devValue = removeCharacter(devValue,decimalSepCh);
   devValue = decimalSepCh+devValue;
   if( devValue.length > 3)
   {
    devValue = devValue.substr(0,3);
   }
  }else{
   tempValue = realValue;
  } tempValue = removeCharacter(tempValue,thausandSepCh);
   
  var result = "";
  var len = tempValue.length;
  while (len > 3){
  result = thausandSepCh+tempValue.substr(len-3,3)+result;
  len -=3;
 }
 result = tempValue.substr(0,len)+result;
// if (value<0){
//	return '('+result+devValue+')'
// }
// else{
//alert(result+devValue);

	return result+devValue;
 

// }
 }
function timeFormat(value)
 {
// alert('test');
  var thausandSepCh = ":";
      
      var tempValue = "";
      var realValue = value+"";
      var devValue = "";
      realValue = characterControl(realValue);
      var comma = -1;
      if(comma != -1 )
      {
       tempValue = realValue.substr(0,comma);
       devValue = realValue.substr(comma);
       devValue = removeCharacter(devValue,thausandSepCh);
       devValue = decimalSepCh+devValue;
       if( devValue.length >= 2)
       {
        devValue = devValue.substr(0,2);
       }
      }else{
       tempValue = realValue;
      }tempValue = removeCharacter(tempValue,thausandSepCh);
       
      var result = "";
      var len = tempValue.length;
      while (len > 2){
      result = thausandSepCh+tempValue.substr(len-2,2)+result;
      len -=2;
     }
     result = tempValue.substr(0,len)+result;
     return result+devValue;
 } 
 
function str_replace(search_target,replacement,str)
 {
  str = new String(str);
  var n_str = str.length;
  var n_search = search_target.length;
  var result = "",searching = 0;
  for(var i=0;i<n_str;i++)
  { 
   if (n_search == 1)
   {
    if (str.charAt(i) == search_target) result += replacement;
    else result+=str.charAt(i);
   }else
   {   
    searching = str.indexOf(search_target,i);
    if (searching <= i && searching >= 0) 
    {
     result += replacement;        
     i+=n_search-1;
    }
    else
    {
     result+=str.charAt(i);
    }
     
   }   
  }
  return result;
 }
 
 function convertToNumber(val){
 	var angka=val;
	var a1 = str_replace(".","",angka);
	var a2 = str_replace(",",".",a1);
	var a3 = str_replace(")","",a2);
	var a4 = str_replace("(","-",a3);
	return a4; 
 }

 function convertToString(val){
 	var angka=val;
	var a1 = str_replace(".",",",angka);
	return a1; 
 }

// Convert numbers to words
// copyright 25th July 2006, by Stephen Chapman http://javascript.about.com
// permission to use this Javascript on your web page is granted
// provided that all of the code (including this copyright notice) is
// used exactly as shown (you can change the numbering system if you wish)

// American Numbering System
var th = ['','ribu','juta', 'milyar','triliun'];
// uncomment this line for English Number System
// var th = ['','thousand','million', 'milliard','billion'];
// diEdit oleh PT. Multi Informatika Solusindo http://www.multisolusi.com/

var dg = ['nol','satu','dua','tiga','empat', 'lima','enam','tujuh','delapan','sembilan']; var tn = ['sepuluh','sebelas','dua belas','tiga belas', 'empat belas','lima belas','enam belas', 'tujuh belas','delapan belas','sembilan belas']; var tw = ['dua puluh','tiga puluh','empat puluh','lima puluh', 'enam puluh','tujuh puluh','delapan puluh','sembilan puluh']; function toWords(s){s = s.toString(); s = s.replace(/[\, ]/g,''); if (s != String(parseFloat(s))) return 'bukan angka numerik'; var x = s.indexOf('.'); if (x == -1) x = s.length; if (x > 15) return 'angka terlalu besar'; var n = s.split(''); var str = ''; var sk = 0; for (var i=0; i < x; i++) {if ((x-i)%3==2) {if (n[i] == '1') {str += tn[Number(n[i+1])] + ' '; i++; sk=1;} else if (n[i]!=0) {str += tw[n[i]-2] + ' ';sk=1;}} else if (n[i]!=0) {str += dg[n[i]] +' '; if ((x-i)%3==0) str += 'ratus ';sk=1;} if ((x-i)%3==1) {if (sk) str += th[(x-i-1)/3] + ' ';sk=0;}} if (x != s.length) {var y = s.length; str += 'koma '; for (var i=x+1; i<y; i++) str += dg[n[i]] +' ';} return str.replace(/\s+/g,' ');}


function number_format (number, decimals, dec_point, thousands_sep) {
  // http://kevin.vanzonneveld.net
  // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +     bugfix by: Michael White (http://getsprink.com)
  // +     bugfix by: Benjamin Lupton
  // +     bugfix by: Allan Jensen (http://www.winternet.no)
  // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // +     bugfix by: Howard Yeend
  // +    revised by: Luke Smith (http://lucassmith.name)
  // +     bugfix by: Diogo Resende
  // +     bugfix by: Rival
  // +      input by: Kheang Hok Chin (http://www.distantia.ca/)
  // +   improved by: davook
  // +   improved by: Brett Zamir (http://brett-zamir.me)
  // +      input by: Jay Klehr
  // +   improved by: Brett Zamir (http://brett-zamir.me)
  // +      input by: Amir Habibi (http://www.residence-mixte.com/)
  // +     bugfix by: Brett Zamir (http://brett-zamir.me)
  // +   improved by: Theriault
  // +      input by: Amirouche
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // *     example 1: number_format(1234.56);
  // *     returns 1: '1,235'
  // *     example 2: number_format(1234.56, 2, ',', ' ');
  // *     returns 2: '1 234,56'
  // *     example 3: number_format(1234.5678, 2, '.', '');
  // *     returns 3: '1234.57'
  // *     example 4: number_format(67, 2, ',', '.');
  // *     returns 4: '67,00'
  // *     example 5: number_format(1000);
  // *     returns 5: '1,000'
  // *     example 6: number_format(67.311, 2);
  // *     returns 6: '67.31'
  // *     example 7: number_format(1000.55, 1);
  // *     returns 7: '1,000.6'
  // *     example 8: number_format(67000, 5, ',', '.');
  // *     returns 8: '67.000,00000'
  // *     example 9: number_format(0.9, 0);
  // *     returns 9: '1'
  // *    example 10: number_format('1.20', 2);
  // *    returns 10: '1.20'
  // *    example 11: number_format('1.20', 4);
  // *    returns 11: '1.2000'
  // *    example 12: number_format('1.2000', 3);
  // *    returns 12: '1.200'
  // *    example 13: number_format('1 000,50', 2, '.', ' ');
  // *    returns 13: '100 050.00'
  // Strip all characters but numerical ones.
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function (n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}
