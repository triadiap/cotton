// $('.edit-petugas').click(function() {
// 	var id = $(this).attr('id');
// 	sendAjax('.page-ajax', 'proses/edit/petugas/'+id, 'get', 'html')
// });
var url = '/dishub';
$('.hapus-petugas').click(function() {
	var id = $(this).attr('id');
	sendAjax('.response-alert',url+'/proses/delete/petugas/'+id,'get','html','petugas');
});
$('#provinsi').change(function() {
	var prov = $(this).val();
	sendAjax('#kab_kota',url+'/getKabKota/'+prov,'get','html');
});
$('#kab_kota').change(function() {
	var kab = $(this).val();
	sendAjax('#kecamatan', url+'/getKecamatan/'+kab, 'get', 'html');
	// sendAjax('.show-response-petugas', url+'/getPetugas/'+ftt, 'get', 'html');
});
$('#provinsi_tempat').change(function() {
	var prov = $(this).val();
	sendAjax('#kab_kota_tempat',url+'/getKabKota/'+prov,'get','html');
});
$('#kecamatan').change(function() {
	var kecamatan = $(this).val();
	sendAjax('#kelurahan', url+'/getKelurahan/'+kecamatan, 'get', 'html');
});
$('#kelurahan').change(function() {
	var kelurahan = $(this).val();
	$.ajax({
		url 	: url+'/getKodePos/'+kelurahan,
		type 	: 'get',
		dataType: 'html',
		success : function(data)
		{
			$('#kode_pos').val(data);
		}
	});
});



$('#autocomplete').keyup(function() {
	$('.show-result').fadeIn(100);
	var cari = $(this).val();
	sendAjax('.show-result', url+'/cari/autocomplete?key='+cari, 'get', 'html');
});
$('.traking-petugas').click(function(){
	var cari = $('#id_autocomplete').val();
	sendAjax(".response-polylines","getPolylines/"+cari,"get","html");
});
$('#cekNik').keyup(function() {
	$('.show-result').fadeIn(100);
	var cari = $(this).val();
	sendAjax('.show-result', url+'/cek/nik?key='+cari, 'get', 'html');
});
$('#cekNik').focusin(function() {
	$('.show-result').show();
	$(this).attr('autocomplete','off');
});
$('#autocomplete').focusin(function() {
	$('.show-result').show();
	$(this).attr('autocomplete','off');
});
$('.cari-petugas').click(function(){
	$('.popup').fadeIn(100);
	$.ajax({
		url 	: url+'/cari/petugas',
		type 	: 'get',
		dataType: 'html',
		success : function(data) {
			$('.window').html(data);
		}
	});
});
$('.close-btn').click(function() {
	$('.popup').fadeOut(100);
});
$('.glyphicon-remove').click(function() {
	$('.popup').fadeOut(100);
});
// $('.btn-select').click(function() {
// 	var nik = $(this).attr('id');
// 	$.ajax({
// 		url 	: '/dishub/select/petugas/'+id,
// 		type 	: 'get',
// 		dataType: 'html',
// 		success : function(data)
// 		{
// 			document.getElementById('add-petugas').innerHTML = data;
// 		}
// 	});
// });
$('.btn-read').click(function(){
	var id = $(this).attr('id');
	$('.popup').fadeIn(100);
	sendAjax('.window',url+'/read/message/'+id,'get','html');
});
$('.btn-add').click(function() {
	sendAjax('.page-ajax',url+'/users/tambah','get','html');
});
$('.edit-users').click(function() {
	var id = $(this).attr('id');
	sendAjax(".page-ajax",url+"/users/edit/"+id,"get","html");
});
$('.delete-users').click(function() {
	var id = $(this).attr('id');
	sendAjax('', url+'/users/delete/'+id, 'get', 'html', 'users');
});
$('#cari').keyup(function() {
	var cari = $(this).val();
	sendAjax('.response-cari', url+'/users/cari?s='+cari, 'get', 'html');
});
$('.btn-add-petugas').click(function() {
	sendAjax('.page-ajax', url+'/petugas/tambah', 'get', 'html');
});
$('#cari_petugas').keyup(function(){
	var cari = $(this).val();
	sendAjax('.response-cari', url+'/petugas/cari?s='+cari, 'get', 'html');
});
$('#cari_penugasan').keyup(function(){
	var cari = $(this).val();
	sendAjax('.response-cari', url+'/penugasan/cari?s='+cari, 'get', 'html');
});
$('.edit-penugasan').click(function() {
	var id = $(this).attr('id');
	sendAjax('.page-ajax', '/dishub/edit/penugasan/'+id, 'get', 'html');
});
$('.btn-delete').click(function(){
	var id = $(this).attr('id');
	sendAjax('', url+'/delete/message/'+id, 'get', 'json','pesan');
});
$('.hapus-penugasan').click(function() {
	var id = $(this).attr('id');
	sendAjax('', url+'/hapus/penugasan/'+id, 'get', 'html', 'petugas');
});
$('#nikah').change(function() {
	var nikah = $(this).val();
	if(nikah == 'nikah')
	{
		$('#jumlah_anak').removeAttr('disabled');
		$('#nama_pasangan').removeAttr('disabled');
	} else {
		$('#jumlah_anak').attr('disabled','disabled');
		$('#nama_pasangan').attr('disabled','disabled');
	}
});
$('.simpan-maps').click(function(){
	  var alamat = $('#pac-input').val();
	  $('#alamat').val(alamat);
	  $('#modal-maps').slideUp();
});