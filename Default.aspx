﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="cotton.Default" %>
<% 
    if (Session["username"] == null) {
        Response.Redirect("Login.aspx");
    }
%>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Cotton V4</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="asset/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="asset/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="asset/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="asset/plugins/datatables/dataTables.bootstrap.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="asset/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    
    <!-- ./wrapper -->

    <!-- jQuery 2.2.3 -->
    <script src="asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="asset/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="asset/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="asset/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="asset/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="asset/plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="asset/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="asset/js/demo.js"></script>
    
    <link rel="stylesheet" href="asset/plugins/datepicker/datepicker3.css">
    <script src="asset/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="asset/js/bootbox.min.js"></script>
    <script src="asset/js/number.js"></script>
    <link rel="stylesheet" href="asset/plugins/select2/select2.min.css">
    <script src="asset/plugins/select2/select2.full.min.js"></script>
    <!-- Theme style -->
    <link rel="stylesheet" href="asset/css/AdminLTE.min.css">
<style>
    a ,.info-box{
        cursor:pointer;
    }
    th {
        text-align:center;
    }
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a  class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="asset/img/logo_croped.png" width="40px"/></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Cotton</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" style="background:url('asset/img/bg_header.png')">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="asset/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><%=Session["name"].ToString() %></p>
          <a href="#"><%=Session["job_title"].ToString() %></a>
            <input type="hidden" id="global_id" value="0" />
            <input type="hidden" id="global_status" value="0" />
        </div>
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
          <% cotton.lib.MyDatabase db = new cotton.lib.MyDatabase();
              System.Data.SqlClient.SqlDataReader reader = db.Query("select * from m_menu where id in(select menu_parent from m_menu inner join m_mapping_menu on m_mapping_menu.id_menu = m_menu.id where id_job_title in("+Session["id_job_title_on_behalf"].ToString()+"))");
              String load_default="";
              while (reader.Read())
              {
                  Response.Write("<li class=\"treeview\">");
                  Response.Write("<a href=\"#\">");
                  Response.Write("<i class=\""+reader["menu_icon"].ToString()+"\"></i> <span>"+reader["menu"].ToString()+"<span id='notif-"+reader["id"].ToString()+"'></span></span>");
                  Response.Write("<span class=\"pull-right-container\">");
                  Response.Write("<i class=\"fa fa-angle-left pull-right\"></i>");
                  Response.Write("</span>");
                  Response.Write("</a>");
                  Response.Write("<ul class=\"treeview-menu\">");
                  cotton.lib.MyDatabase db2 = new cotton.lib.MyDatabase();
                  System.Data.SqlClient.SqlDataReader reader2 = db2.Query("select * from m_menu inner join m_mapping_menu on m_mapping_menu.id_menu = m_menu.id where id_job_title in("+Session["id_job_title_on_behalf"].ToString()+") and menu_parent = '"+reader["id"].ToString()+"'");
                  while (reader2.Read()) {
                      Response.Write("<li onclick=\"" + reader2["link"].ToString() + "\"><a ><i class=\"" + reader2["menu_icon"] + "\"></i> "+reader2["menu"].ToString()+"<span id='notif-"+reader2["id"].ToString()+"'></span></a></li>");
                      try
                      {
                          if (reader2["default"].ToString() != "" && load_default == "")
                          {
                              load_default = reader2["default"].ToString();
                          }
                      }
                      catch (Exception ex) {

                      }
                  }
                  reader2.Close();
                  db2.Close();
                  Response.Write("</ul>");
                  Response.Write("</li>");
              }
              db.Close();
              reader.Close();
               %>
        <% if (!Session["email"].ToString().Contains("@unilever.com"))
        { %>
        <li><a onclick="go_menu('password')"><i class="fa fa-user"></i> <span>Change Password</span></a></li>
        <% } %>
        <li><a onclick="go_logout()"><i class="fa fa-power-off"></i> <span>Log out</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="background:#FFF url('asset/img/bg.png') no-repeat bottom left">
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 4
    </div>
    <strong>Copyright &copy; 2016 <a href="http://multisolusi.com">Multisolusi</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- page script -->
<script>
    var c = 0;
    var time_out;
    var timer_is_on = 0;

    function timedCount() {
        //document.getElementById("txt").value = c;
        c = c + 1;
        if (c < 900) {
            time_out = setTimeout(function () { timedCount() }, 1000);
        } else {
            go_logout();
        }
        console.log(c);
    }
    $(document).ready(function () {
        load_notif();
        timedCount();
        <% Response.Write(load_default);%>
    });
    function go_menu(link) {
        c = 0;
        $.ajax({
            url: "display/"+link+".html?="+Math.random(),
        }).done(function (msg) {
            $(".content-wrapper").html(msg);
        });
    }
    function go_logout() {
        $.ajax({
            url: "action/logout.aspx?sid="+Math.random(),
        }).done(function (msg) {
            location.reload();
        });
    }
    function load_notif() {
        $.ajax({
            url:"action/get_notif.aspx?sid="+Math.random()
        }).done(function (msg){
            data = eval('(' + msg + ')');
            $.each(data, function (key, val) {
                if (val == "0") {
                    $("#" + key).html("");
                } else {
                    $("#" + key).html("<span class='label label-danger'>" + val + "</span>");
                }
                if (key == "is_login" && val == "false") {
                    location.reload();
                }
            });
            setTimeout("load_notif()", 10000);
        })
    }
</script>
</body>
</html>
