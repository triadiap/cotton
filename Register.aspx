﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="cotton.Register" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Register</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="asset/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="asset/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="asset/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="asset/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->    
    <!-- /.login-box -->
    <!-- jQuery 3.0.0 -->
    <script src="asset/js/jquery.min.js"></script>
    <!-- Custom Function -->
    <script src="asset/js/function.js"></script>
    <!-- jQuery Auth Login -->
    <script src="asset/js/authLogin.js"></script>
    <!-- jQuery 2.2.3 -->
    <script src="asset/plugins/jQuery/jquery.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="asset/bootstrap/js/bootstrap.min.js"></script>
    <script src="asset/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="asset/js/bootbox.min.js"></script>
    <style>
        ul {
            list-style-type: none;
        }
        .border {            
            margin-left:5px;
            padding-left:20px;
            border-left:dotted 1px #DDD;
        }
        li {
            cursor:pointer;
        }
    </style>
</head>
<body class="hold-transition login-page">
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="login-logo">
                <img src="asset/img/logo_croped.png" width="100px" />
                <br />
                <a><b>Cotton</b></a>
            </div>
        </div>
        <form method="POST" id="form-login" onsubmit="do_register();return false;">
            <div class="col-md-8 col-md-offset-2 login-box-body">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group has-feedback">
                            <label for="user">Email</label>
                            <input type="email" name="email" id="email" class="form-control" placeholder="Email" onchange="email_change()" autofocus required>
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="pass">Username</label>
                            <input type="text" name="pass" id="username" class="form-control" placeholder="Username" required>
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="pass">Password</label>
                            <input type="password" name="pass" id="password" class="form-control" placeholder="Password" required>
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="pass">Name</label>
                            <input type="text" name="pass" id="name" class="form-control" placeholder="Name" required>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="pass">NIP</label>
                            <input type="text" name="pass" id="nip" class="form-control" placeholder="NIP" required>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="pass">Sales Org</label>
                            <select class="form-control" id="sales_org" onchange="load_division()" required>
                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="pass">Job Title</label>
                            <select class="form-control" id="job_title" onchange="load_division()" required>
                            </select>
                        </div>
                        <input type="hidden" id="noreg_lm" />
                        <input type="hidden" id="grade" />
                        <div class="row">
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat btn-auth">Register</button>        
                            </div>
                            <div class="col-md-12">
                                <a href="Login.aspx" style="cursor:pointer">Login</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group has-feedback">
                            <label for="pass" id="div_label">Division</label>
                            <span id="list_division">

                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
    <script>

        $.ajax({
            method: "POST",
            url: "action/register/list_sales_org.aspx"
        }).done(function (msg) {
            $("#sales_org").html(msg)
        });
        function load_division() {
            $("#list_division").html("Loading Data . . .")
            $.ajax({
                method: "POST",
                url: "action/register/list_division.aspx",
                data: {sales_org:$("#sales_org option:selected").html()}
            }).done(function (msg) {
                $("#list_division").html(msg)
            });
        }
        function load_category(division) {
            if (document.getElementById(division).checked) {
                $("#list_category_" + division).html("Loading Data . . .")
                $.ajax({
                    method: "POST",
                    url: "action/register/list_category.aspx",
                    data: { DIVCD: division, sales_org: $("#sales_org option:selected").html() }
                }).done(function (msg) {
                    $("#list_category_" + division).html(msg)
                });
                $("#div_label").html("Division - Category")
            } else {
                $("#list_category_" + division).html("");
                $("#div_label").html("Division")
            }
        }
        function load_brand(categori) {
            if (document.getElementById(categori).checked) {
                $("#div_label").html("Division - Category - Brand")
                $("#list_brand_" + categori).html("Loading Data . . .")
                $.ajax({
                    method: "POST",
                    url: "action/register/list_brand.aspx",
                    data: { CATCD: categori, sales_org: $("#sales_org option:selected").html(),id_job_title : $("#job_title").val() }
                }).done(function (msg) {
                    $("#list_brand_" + categori).html(msg)
                });
            } else {
                $("#list_brand_" + categori).html("")
                $("#div_label").html("Division - Category")
            }
        }
        function load_segment(brand) {
            if (document.getElementById(brand).checked) {
                $("#list_segment_" + brand).html("Loading Data . . .")
                $.ajax({
                    method: "POST",
                    url: "action/register/list_segment.aspx",
                    data: { BRNCD: brand, sales_org: $("#sales_org option:selected").html() }
                }).done(function (msg) {
                    $("#list_segment_" + brand).html(msg)
                });

                $("#div_label").html("Division - Category - Brand - Segments")
            } else {
                $("#list_segment_" + brand).html("")
                $("#div_label").html("Division - Category - Brand")
            }
        }
        function email_change() {
            $.ajax({
                method: "POST",
                url: "action/register/user_detail.aspx",
                data: { email: $("#email").val() }
            }).done(function (msg) {
                var data = eval('(' + msg + ')');
                if (data.status == "1") {
                    if (data.name == "" && $("#email").val().indexOf("@unilever.com") > 0) {
                        bootbox.alert("Please register in HR Portal First");
                    }
                    $("#name").val(data.name);
                    $("#username").val(data.username);
                    $("#nip").val(data.nip);
                    $("#grade").val(data.grade);
                    $("#noreg_lm").val(data.noreg_lm);
                    if (data.password != "") {
                        $("#password").val(data.password);
                        $("#password").attr("readonly", true)
                    } else {
                        $("#password").val("");
                        $("#password").attr("readonly", false)
                    }
                } else {
                    bootbox.alert("Your email already registered");
                }
            });
            $.ajax({
                method: "POST",
                url: "action/register/list_jobtitle.aspx",
                data: { email: $("#email").val() }
            }).done(function (msg) {
                $("#job_title").html(msg)
            });
        }
        function do_register() {
            var segments = $("input[name='SECNM']:checkbox:checked").map(function () {
                return this.value;
            }).get().join(",");
            name = "";
            $("input[name='BRNNM']:checkbox:checked").each(function () {
                if ($(this).attr("user") != "") {
                    name = $(this).attr("user");
                }
            });
            if (name != "") {
                if($("#job_title option:selected").attr("approver") == "0"){
                    var r = confirm("Duplicate brand with user "+name+", Are you sure?");
                    if (r == true) {
                        $(".btn-auth").html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking . . . ');
                        $(".btn").attr("disabled", true);
                        $.ajax({
                            method: "POST",
                            url: "action/register/update.aspx",
                            data: {
                                email: $("#email").val(),
                                username: $("#username").val(),
                                password: $("#password").val(),
                                name: $("#name").val(),
                                nip: $("#nip").val(),
                                job_title: $("#job_title").val(),
                                sales_org: $("#sales_org").val(),
                                grade: $("#grade").val(),
                                noreg_lm: $("#noreg_lm").val(),
                                segments: segments
                            }
                        }).done(function (msg) {
                            data = eval('(' + msg + ')');
                            if (data.status == 1) {
                                bootbox.alert("Your request has been received. Please wait admin confirmation for your account activation");
                                location.href = "Default.aspx";
                            } else {
                                $(".btn-auth").html('Register');
                                $(".btn").attr("disabled", false);
                                bootbox.alert(data.msg);
                            }
                        });
                    } else {
                    }
                } else {
                    alert("Duplicate brand with user " + name)
                }
            } else {
                $(".btn-auth").html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking . . . ');
                $(".btn").attr("disabled", true);
                $.ajax({
                    method: "POST",
                    url: "action/register/update.aspx",
                    data: {
                        email: $("#email").val(),
                        username: $("#username").val(),
                        password: $("#password").val(),
                        name: $("#name").val(),
                        nip: $("#nip").val(),
                        job_title: $("#job_title").val(),
                        sales_org: $("#sales_org").val(),
                        grade: $("#grade").val(),
                        noreg_lm: $("#noreg_lm").val(),
                        segments: segments
                    }
                }).done(function (msg) {
                    data = eval('(' + msg + ')');
                    if (data.status == 1) {
                        bootbox.alert("Your request has been received. Please wait admin confirmation for your account activation");
                        location.href = "Default.aspx";
                    } else {
                        $(".btn-auth").html('Register');
                        $(".btn").attr("disabled", false);
                        bootbox.alert(data.msg);
                    }
                });
            }
        }
    </script>
</body>
</html>