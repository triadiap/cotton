﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="cotton.Login" %>
<%
    if (Session["username"] != null) {
        Response.Redirect("Default.aspx");
    }
     %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="asset/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="asset/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="asset/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="asset/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->    
    <!-- /.login-box -->
    <!-- jQuery 3.0.0 -->
    <script src="asset/js/jquery.min.js"></script>
    <!-- Custom Function -->
    <script src="asset/js/function.js"></script>
    <!-- jQuery Auth Login -->
    <script src="asset/js/authLogin.js"></script>
    <!-- jQuery 2.2.3 -->
    <script src="asset/plugins/jQuery/jquery.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="asset/bootstrap/js/bootstrap.min.js"></script>
    <script src="asset/js/bootbox.min.js"></script>
    <script src="asset/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="asset/js/bootbox.min.js"></script>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
      <img src="asset/img/logo_croped.png" width="100px" />
      <br />
    <a><b>Cotton</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <div id="response-ajax"></div>

    <form method="POST" id="form-login" onsubmit="do_login();return false;" >
      <div class="form-group has-feedback">
        <label for="user">Username</label>
        <input type="text" name="user" id="username" class="form-control" placeholder="Username" autofocus required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <label for="pass">Password</label>
        <input type="password" name="pass" id="password" class="form-control" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-md-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat btn-auth">Sign In</button>
        </div>
        <div class="col-md-12">
        <a href="Register.aspx" style="cursor:pointer">Register</a><br />
        <a style="cursor:pointer" onclick="do_forget()">Forget Password</a>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <!-- /.social-auth-links -->  
  </div>
  <!-- /.login-box-body -->
</div>
    <script>
        function do_forget() {
            bootbox.prompt({title: "Please insert your email address",inputType: 'email', callback:function (result) {
                if (result) {
                    $.ajax({
                        method: "POST",
                        url: "action/user/forget.aspx",
                        data: { email: result }
                    }).done(function (msg) {
                        var data = eval('(' + msg + ')');
                        if (data.status == 1) {
                            bootbox.alert("Your password has been sent to " + result);
                        } else {
                            bootbox.alert(data.msg);
                        }
                    });
                }
            }});
        }
        function do_login() {
            var username = $("#username").val();
            var password = $("#password").val();
            $(".btn-auth").html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking . . . ');
            $(".btn").attr("disabled", true);
            $.ajax({
                method: "POST",
                url: "action/login.aspx",
                data: { username: username, password: password }
            }).done(function (msg) {
                var data = eval('(' + msg + ')');
                if (data.status == 1) {
                    if (data.opsi.length > 1) {
                        bootbox.prompt({
                            title: "Please select user",
                            inputType: 'select',
                            inputOptions: data.opsi,
                            callback: function (result) {
                                $.ajax({
                                    method: "POST",
                                    url: "action/login_on_behalf.aspx",
                                    data: { id: result}
                                }).done(function (msg) {
                                    location.reload();
                                });
                            }
                        });
                    } else {
                        location.reload();
                    }
                } else {
                    $(".btn-auth").html('Sign In');
                    $(".btn").attr("disabled", false);
                    bootbox.alert("Invalid username or password");
                }
            });
        }
    </script>
</body>
</html>