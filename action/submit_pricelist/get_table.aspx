﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    Response.Write("<table id='table' class='table table-bordered table-hover'>");
    Response.Write("<thead>");
    Response.Write("<tr>");
    Response.Write("<td></td>");
    Response.Write("<td>Proposal Number</td>");
    Response.Write("<td>Proposal Date</td>");
    Response.Write("<td>Subject</td>");
    Response.Write("<td>Action</td>");
    Response.Write("</tr>");
    Response.Write("</thead>");
    Response.Write("<tbody>");
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select distinct t_proposal_pricelist.*,convert(varchar,t_proposal_pricelist.created_date,103) tgl from t_proposal_pricelist inner join t_pricelist_detail on id_proposal = t_proposal_pricelist.id where t_pricelist_detail.id_pricelist is null");
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td><input type='checkbox' name='ck_proposal' id='channel_"+reader["id"]+"' value='"+reader["id"]+"'/></td>");
        Response.Write("<td>"+reader["proposal_number"]+"</td>");
        Response.Write("<td>"+reader["tgl"]+"</td>");
        Response.Write("<td>"+reader["subject"]+"</td>");
        Response.Write("<td><button type='button'  class='btn btn-primary btn-xs' onclick='download("+reader["id"].ToString()+")'><i class='fa fa-download'></i></button></td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
    Response.Write("</tbody>");
    Response.Write("</table>");
%>