﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    String division = Request.Form["division"].ToString();
    String publish_date = Request.Form["publish_date"].ToString();
    String proposal = Request.Form["proposal"].ToString();
    int intNumber = cotton.lib.MyDatabase.getNumber("pricelist" + division, DateTime.Now.Year.ToString());
    String newNumber = "";
    if (intNumber <= 9) {
        newNumber = division + "00" + intNumber + DateTime.Now.Year.ToString();
    }else if (intNumber <= 99) {
        newNumber = division + "0" + intNumber + DateTime.Now.Year.ToString();
    }else if (intNumber <= 999) {
        newNumber = division + "" + intNumber + DateTime.Now.Year.ToString();
    }
    String []tmp = publish_date.Split('/');
    String date = tmp[2] + "-" + tmp[1] + "-" + tmp[0];
    cotton.lib.MyDatabase.FastExec("insert into t_pricelist(pricelist_number,pricelist_date,id_division,status,created_by,created_date,on_behalf) values ('" + newNumber + "','"+date+"',(select id from m_division where division_code = '" + division + "'),2,'"+Session["id"].ToString()+"',getdate(),'"+Session["id_on_behalf"].ToString()+"')");
    cotton.lib.MyDatabase.FastExec("update t_pricelist_detail set id_pricelist = (select id from t_pricelist where pricelist_number = '" + newNumber + "') where id_pricelist is null and id_proposal in("+proposal+")");
    cotton.lib.MyDatabase db = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = db.Query("SELECT app_days,m_user.id FROM [dbo].[m_approval_publish_proposal] inner join m_user on m_user.id_job_title = m_approval_publish_proposal.id_app_job_title");
    while (reader.Read()) {
        cotton.lib.MyDatabase.FastExec("insert into t_approval (id_proposal,app_level,id_approver,status_approve) "
            + " select distinct t_proposal_detail.id_proposal,100,'" + reader["id"].ToString() + "',0 from t_pricelist inner join t_pricelist_detail on t_pricelist_detail.id_pricelist = t_pricelist.id"
            + "  inner join t_proposal_detail on t_proposal_detail.id_proposal = t_pricelist_detail.id_proposal"
            + " inner join m_callendar on m_callendar.id = id_callendar"
            + " where DATEDIFF(day, t_pricelist.pricelist_date, m_callendar.effective_date) <= " + reader["app_days"].ToString() + " and pricelist_number = '"+newNumber+"'");
    }
    reader.Close();
    db.Close();
    db = new cotton.lib.MyDatabase();
    String cc = "";
    reader = db.Query("select * from m_user where id = '"+Session["id"].ToString()+"'");
    while (reader.Read()) {
        cc = reader["email"].ToString();
    }
    reader.Close();
    db.Close();
    cotton.lib.MyDatabase.FastExec("insert into h_activity([table],id_ref,[action],created_by,created_date,on_behalf) values ('t_pricelist','0','Submit Pricelist "+newNumber+"','"+Session["id"].ToString()+"',getdate(),'" + Session["id_on_behalf"].ToString() + "')");
    db = new cotton.lib.MyDatabase();
    reader = db.Query("select t_proposal_pricelist.id,proposal_number,name,email from t_proposal_pricelist inner join t_approval on t_proposal_pricelist.id = id_proposal inner join m_user on m_user.id = id_approver where status_approve = '0' and id_proposal in("+proposal+")");
    String number = "";
    while (reader.Read()) {
        Response.Write("need approval");
        String body = "<style>.header{text-align:center;font-weight:bold;background:#EFEFEF}td{border:1px solid #000;}</style>";
        body += "Dear " + reader["name"].ToString()+"<br/>";
        body += "<br/>";
        body += "Proposal "+reader["proposal_number"]+" is waiting for your approval because it's interval between publish and effective date is less than 30 days. Here are the details :<br/>";
        number = reader["proposal_number"].ToString();
        body += "<table cellpadding='3' cellspacing='0'>";
        body += "<tr class=\"header\"><td rowspan='2'>#</td><td rowspan='2'>SKU</td><td rowspan='2'>Description</td><td rowspan='2'>Remark</td><td colspan='3'>Price</td><td rowspan = '2'>Area</td><td rowspan = '2'>Channel</td><td rowspan = '2'>Note</td></tr>";
        body += "<tr class=\"header\"><td rowspan='1'>RSP/pcs</td><td rowspan='1'>Toko Price/pcs</td><td rowspan='1'>Effective From</td></tr>";
        cotton.lib.MyDatabase db2 = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader2 = db2.Query("select proposed.sku,proposed.sku_name,m_proposal_remarks.remark,publised.rsp_price,publised.toko_price,convert(varchar,cal_publised.effective_date,6),publised.rsp_price-publised.toko_price,(publised.rsp_price-publised.toko_price)/publised.toko_price, proposed.rsp_price,proposed.toko_price,convert(varchar,cal_proposed.effective_date,6),proposed.rsp_price-proposed.toko_price,(proposed.rsp_price-proposed.toko_price)/proposed.toko_price,proposed.comment,dbo.get_channel(proposed.id) channel,area_name from t_proposal_detail proposed inner join m_proposal_remarks on m_proposal_remarks.id = proposed.remark inner join m_callendar cal_proposed on cal_proposed.id = proposed.id_callendar left join t_proposal_detail publised on proposed.sku = publised.sku and publised.status = '5' and publised.id_area = proposed.id_area left join m_callendar cal_publised on cal_publised.id = publised.id_callendar inner join m_area on proposed.id_area = m_area.id where proposed.id_proposal = '"+reader["id"].ToString()+"' order by proposed.sku,proposed.id");
        int i = 0;
        while (reader2.Read()) {
            i++;
            body += "<tr>";
            body += "<td>"+i+"</td>";
            body += "<td>"+reader2[0].ToString()+"</td>";
            body += "<td>"+reader2[1].ToString()+"</td>";
            body += "<td>"+reader2[2].ToString()+"</td>";
            body += "<td>"+reader2[8].ToString()+"</td>";
            body += "<td>"+reader2[9].ToString()+"</td>";
            body += "<td>"+reader2[10].ToString()+"</td>";
            body += "<td>"+reader2["area_name"].ToString()+"</td>";
            body += "<td>"+reader2["channel"].ToString()+"</td>";
            body += "<td>"+reader2[13].ToString()+"</td>";
            body += "</tr>";
        }
        reader2.Close();
        db2.Close();
        body += "</table><br/>";
        body += "<br/>";
        body += "Please click link below for detail<br/>";
        body += "<br/>";
        body += "<a href='"+cotton.lib.EmailClass.getUrl()+"'>" + cotton.lib.EmailClass.getUrl() + "</a><br/>";
        body += "Disclaimer<br/>";
        body += "Please DO NOT REPLAY to this system generated mailing";
        cotton.lib.EmailClass.SendMailCC(reader["email"].ToString(),cc, "[Cotton GSOM Approval] Proposal "+number+" Need Your Approval", body);
    }
    reader.Close();
    db.Close();
%>