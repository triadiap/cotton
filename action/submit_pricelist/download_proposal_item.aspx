﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 

    using (OfficeOpenXml.ExcelPackage pck = new OfficeOpenXml.ExcelPackage())
    {
        String id = Request.QueryString["id"].ToString();
        String sql = "select sku,sku_name,m_proposal_remarks.remark,ipack,dozen,convert(varchar,effective_date,103) tanggal,configuration,barcode,rsp_price,((toko_price * ipack)/ 1.1) toko_price_nppn_cs,	((toko_price * dozen) / 1.1) toko_price_nppn_lusin,	(toko_price / 1.1) toko_price_nppn,	((toko_price) * ipack) toko_price_ppn_cs,	((toko_price) * dozen) toko_price_ppn_lusin,	(toko_price) toko_price_ppn,	((toko_price * ipack)/1.1 ) rsp_price_nppn_cs,	((rsp_price) * ipack) rsp_price_ppn_cs,area_name,dbo.get_channel(t_proposal_detail.id) channel from t_proposal_detail inner join m_callendar on m_callendar.id = id_callendar inner join m_proposal_remarks on m_proposal_remarks.id = t_proposal_detail.remark inner join m_area on m_area.id = id_area where id_proposal = '"+id+"'";
        cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader = database.Query(sql);
        OfficeOpenXml.ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Proposal Item");
        int i = 1;
        ws.Cells["A1:A3"].Merge = true;
        ws.Cells["A1"].Value = "SKU";
        ws.Cells["B1:B3"].Merge = true;
        ws.Cells["B1"].Value = "Description";
        ws.Cells["C1:C3"].Merge = true;
        ws.Cells["C1"].Value = "Configuration";
        ws.Cells["D1:D3"].Merge = true;
        ws.Cells["D1"].Value = "Barcode";
        ws.Cells["E1:E3"].Merge = true;
        ws.Cells["E1"].Value = "Ipack";
        ws.Cells["F1:F3"].Merge = true;
        ws.Cells["F1"].Value = "Effective Date";
        ws.Cells["G1:G3"].Merge = true;
        ws.Cells["G1"].Value = "Area";
        ws.Cells["H1:H3"].Merge = true;
        ws.Cells["H1"].Value = "Channel";

        ws.Cells["I1:I3"].Merge = true;
        ws.Cells["I1"].Value = "RSP Price";

        ws.Cells["J1:O1"].Merge = true;
        ws.Cells["J1"].Value = "Toko Price";
        ws.Cells["J2:L2"].Merge = true;
        ws.Cells["J2"].Value = "NON PPN";
        ws.Cells["J3"].Value = "CS";
        ws.Cells["K3"].Value = "DOZEN";
        ws.Cells["L3"].Value = "PCS";
        ws.Cells["M2:O2"].Merge = true;
        ws.Cells["M2"].Value = "PPN";
        ws.Cells["M3"].Value = "CS";
        ws.Cells["N3"].Value = "DOZEN";
        ws.Cells["O3"].Value = "PCS";

        ws.Cells["P1:Q2"].Merge = true;
        ws.Cells["P1"].Value = "RSP Price";
        ws.Cells["P3"].Value = "NON PPN";
        ws.Cells["Q3"].Value = "PPN";

        ws.Cells["R1:R3"].Merge = true;
        ws.Cells["R1"].Value = "Remark";
        ws.Cells["A1:R3"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        ws.Cells["A1:R3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
        ws.Cells["A1:R3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        ws.Cells["A1:R3"].Style.Font.Bold = true;
        i = 3;
        while (reader.Read()) {
            i++;
            ws.Cells["A" + i].Value = reader["sku"].ToString();
            ws.Cells["B" + i].Value = reader["sku_name"].ToString();
            ws.Cells["C" + i].Value = reader["configuration"].ToString();
            ws.Cells["D" + i].Value = reader["barcode"].ToString();
            ws.Cells["E" + i].Value = reader["ipack"].ToString();
            ws.Cells["F" + i].Value = reader["tanggal"].ToString();
            ws.Cells["G" + i].Value = reader["area_name"].ToString();
            ws.Cells["H" + i].Value = reader["channel"].ToString();
            ws.Cells["I" + i].Value = Convert.ToDouble(reader["rsp_price"].ToString()).ToString("#,##0.00");
            ws.Cells["J" + i].Value = Convert.ToDouble(reader["toko_price_nppn_cs"].ToString()).ToString("#,##0.00");
            ws.Cells["K" + i].Value = Convert.ToDouble(reader["toko_price_nppn_lusin"].ToString()).ToString("#,##0.00");
            ws.Cells["L" + i].Value = Convert.ToDouble(reader["toko_price_nppn"].ToString()).ToString("#,##0.00");
            ws.Cells["M" + i].Value = Convert.ToDouble(reader["toko_price_ppn_cs"].ToString()).ToString("#,##0.00");
            ws.Cells["N" + i].Value = Convert.ToDouble(reader["toko_price_ppn_lusin"].ToString()).ToString("#,##0.00");
            ws.Cells["O" + i].Value = Convert.ToDouble(reader["toko_price_ppn"].ToString()).ToString("#,##0.00");
            ws.Cells["P" + i].Value = Convert.ToDouble(reader["rsp_price_nppn_cs"].ToString()).ToString("#,##0.00");
            ws.Cells["Q" + i].Value = Convert.ToDouble(reader["rsp_price_ppn_cs"].ToString()).ToString("#,##0.00");
            ws.Cells["R" + i].Value = reader["remark"].ToString();
        }
        ws.Cells["A1:R" + i].AutoFitColumns();
        ws.Cells["A1:R" + i].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A1:R" + i].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A1:R" + i].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A1:R" + i].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        reader.Close();
        database.Close();
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;  filename=Proposal Item.xlsx");
        Response.BinaryWrite(pck.GetAsByteArray());
        Response.End();
    }
%>