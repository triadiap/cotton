﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select distinct DIVCD,DIVNM from m_hierarchy where DIVNM != 'NULL' order by DIVCD ASC");
    Response.Write("<option value='' selected disabled>Please select</option>");
    while (reader.Read())
    {
        Response.Write("<option value='"+reader["DIVCD"].ToString()+"'>"+reader["DIVNM"].ToString()+"</option>");
    }
    reader.Close();
    database.Close();
%>