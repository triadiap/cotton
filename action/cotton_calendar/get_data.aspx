﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select id,replace(convert(char,publish_date,101),'-','/') as publish_date, replace(convert(char,effective_date,101),'-','/') as effective_date from m_callendar order by m_callendar.publish_date desc ,m_callendar.effective_date desc");
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["publish_date"] + "</td>");
        Response.Write("<td>" + reader["effective_date"] + "</td>");
        Response.Write("<td style='text-align:center'>");
        Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Edit' onclick='do_edit("+reader["id"].ToString()+")'><i class='fa fa-pencil'></i></a>");
        Response.Write("&nbsp;<a class='btn btn-xs btn-danger' data-toggle='tooltip' title='Delete' onclick='do_delete("+reader["id"].ToString()+",\""+reader["publish_date"]+"\")'><i class='fa fa-trash'></i></a>");
        Response.Write("</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>