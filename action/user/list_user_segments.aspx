﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    List<String> segments = new List<string>();
    List<String> division = new List<string>();
    String id_job_title = "0";
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    String id = "";
    if (Request.Form["id"] != null)
    {
        id = Request.Form["id"].ToString();
    }
    else {
        id = Session["id"].ToString();
    }
    String sales_org = Request.Form["sales_org"].ToString();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select DISTINCT DIVCD,SECCD,id_job_title from m_user_segments inner join v_ers on v_ers.SECCD = m_user_segments.segments inner join m_user on m_user.id = id_user where m_user.id  = '"+id+"' and VKORG = '"+sales_org+"'");
    while (reader.Read())
    {
        segments.Add(reader["SECCD"].ToString());
        division.Add(reader["DIVCD"].ToString());
        id_job_title = reader["id_job_title"].ToString();
    }
    reader.Close();
    database.Close();

    database = new cotton.lib.MyDatabase();
    reader = database.Query("select distinct DIVCD,DIVNM from v_ers where DIVNM != 'NULL' and VKORG = '"+sales_org+"'");
    while (reader.Read())
    {
        Response.Write("<ul>");
        Boolean has_category = false;
        for (int i = 0; i < division.Count; i++) {
            if (division[i] == reader["DIVCD"].ToString()) {
                has_category = true;
            }
        }
        if (has_category)
        {
            Response.Write("<li><a><input checked type='checkbox' id='"+reader["DIVCD"].ToString()+"' onclick=\"load_category('"+reader["DIVCD"].ToString()+"')\"><small>"+ reader["DIVCD"].ToString() +"</small>&nbsp;" + reader["DIVNM"].ToString() + "</a></input><span id='list_category_"+reader["DIVCD"].ToString()+"'>");
            String DIVCD = reader["DIVCD"].ToString();
            cotton.lib.MyDatabase database2 = new cotton.lib.MyDatabase();
            System.Data.SqlClient.SqlDataReader reader2 = database2.Query("select distinct CATCD,CATNM from v_ers where DIVCD = '" + DIVCD + "'  and VKORG = '"+sales_org+"'");
            while (reader2.Read())
            {
                Response.Write("<ul class='border'>");
                Boolean has_brand = false;
                for (int i2 = 0; i2 < segments.Count; i2++)
                {
                    if (segments[i2].Substring(0, 4) == reader2["CATCD"].ToString())
                    {
                        has_brand = true;
                    }
                }
                if (has_brand)
                {
                    Response.Write("<li><a>" + "<input checked type='checkbox' id='" + reader2["CATCD"] + "' onclick=\"load_brand('" + reader2["CATCD"] + "')\" ><small>" + reader2["CATCD"].ToString() + "</small> &nbsp" + reader2["CATNM"].ToString() + "</input></a><span id='list_brand_" + reader2["CATCD"].ToString() + "'>");
                    String CATCD = reader2["CATCD"].ToString();
                    cotton.lib.MyDatabase database3 = new cotton.lib.MyDatabase();
                    System.Data.SqlClient.SqlDataReader reader3 = database3.Query("select distinct BRNCD,BRNNM,MRKNM,dbo.get_names(BRNCD,"+id+","+id_job_title+") name from m_hierarchy where CATCD = '" + CATCD + "'  and VKORG = '"+sales_org+"'");
                    while (reader3.Read())
                    {
                        Response.Write("<ul class='border'>");
                        Boolean has_segments = false;
                        for (int i3 = 0; i3 < segments.Count; i3++)
                        {
                            if (segments[i3].Substring(0, 9) == reader3["BRNCD"].ToString())
                            {
                                has_segments = true;
                            }
                        }
                        if (has_segments)
                        {
                            Response.Write("<li><a>" + "<input checked type='checkbox' user = '" + reader3["name"].ToString() + "' name='BRNNM' id='" + reader3["BRNCD"].ToString() + "' onclick=\"load_segment('" + reader3["BRNCD"].ToString() + "')\"><small>" + reader3["BRNCD"].ToString() + "</small> &nbsp" + reader3["BRNNM"].ToString() + "</input><small style='color:black'>" + reader3["MRKNM"].ToString() + "</small><small style='color:red'><i>" + reader3["name"].ToString() + "</i></small></a><span id='list_segment_" + reader3["BRNCD"].ToString() + "'>");
                            String BRNCD = reader3["BRNCD"].ToString();
                            cotton.lib.MyDatabase database4 = new cotton.lib.MyDatabase();
                            System.Data.SqlClient.SqlDataReader reader4 = database4.Query("select distinct SECCD,SECNM from v_ers where BRNCD = '" + BRNCD + "'  and VKORG = '"+sales_org+"'");
                            while (reader4.Read())
                            {
                                Response.Write("<ul class='border'>");
                                Boolean checked_segments = false;
                                for (int i4 = 0; i4 < segments.Count; i4++)
                                {
                                    if (segments[i4] == reader4["SECCD"].ToString())
                                    {
                                        checked_segments = true;
                                    }
                                }
                                if (checked_segments)
                                {
                                    Response.Write("<li><input type='checkbox' name='SECNM' value='" + reader4["SECCD"].ToString() + "' checked>" + reader4["SECNM"].ToString() + "</input></li>");
                                }
                                else
                                {
                                    Response.Write("<li><input type='checkbox' name='SECNM' value='" + reader4["SECCD"].ToString() + "'>" + reader4["SECNM"].ToString() + "</input></li>");
                                }
                                Response.Write("</ul>");
                            }
                            reader4.Close();
                            database4.Close();
                        }
                        else
                        {
                            Response.Write("<li><a>" + "<input type='checkbox' user = '" + reader3["name"].ToString() + "' name='BRNNM' id='" + reader3["BRNCD"].ToString() + "' onclick=\"load_segment('" + reader3["BRNCD"].ToString() + "')\"><small>" + reader3["BRNCD"].ToString() + "</small> &nbsp" + reader3["BRNNM"].ToString() + "</input><small style='color:black'>" + reader3["MRKNM"].ToString() + "</small><small style='color:red'><i>" + reader3["name"].ToString() + "</i></small></a><span id='list_segment_" + reader3["BRNCD"].ToString() + "'>");
                        }
                        Response.Write("<span></li>");
                        Response.Write("</ul>");
                    }
                    reader3.Close();
                    database3.Close();
                }
                else
                {
                    Response.Write("<li><a>" + "<input type='checkbox' id='" + reader2["CATCD"] + "' onclick=\"load_brand('" + reader2["CATCD"] + "')\" ><small>" + reader2["CATCD"].ToString() + "</small> &nbsp" + reader2["CATNM"].ToString() + "</input></a><span id='list_brand_" + reader2["CATCD"].ToString() + "'>");
                }
                Response.Write("<span></li>");
                Response.Write("</ul>");
            }
            reader2.Close();
            database2.Close();
        }
        else {
            Response.Write("<li><a><input type='checkbox' id='"+reader["DIVCD"].ToString()+"' onclick=\"load_category('"+reader["DIVCD"].ToString()+"')\"><small>"+ reader["DIVCD"].ToString() +"</small>&nbsp;" + reader["DIVNM"].ToString() + "</a></input><span id='list_category_"+reader["DIVCD"].ToString()+"'><span></li>");
        }
        Response.Write("<span></li>");
        Response.Write("</ul>");
    }
    reader.Close();
    database.Close();
%>