﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    String id = Request.Form["id"].ToString();
    String status = Request.Form["status"].ToString();
    if (status == "1")
    {
        cotton.lib.MyDatabase.FastExec("update m_user set status = '3',created_by = '"+Session["id"].ToString()+"',created_date = getdate() where id = '"+id+"'");
        cotton.lib.MyDatabase.FastExec("exec sp_send_app_user_to_admin "+id);
    }
    if (status == "2")
    {
        cotton.lib.MyDatabase.FastExec("update m_user set status = '3' where id = '"+id+"'");
        cotton.lib.MyDatabase.FastExec("exec sp_send_app_user_to_admin "+id);
    }
    if (status == "3" || status == "4")
    {
        cotton.lib.MyDatabase.FastExec("update t_proposal_pricelist set created_by = '" + id + "' where created_by in(select id from m_user where noreg in (select noreg from m_user where id='" + id + "') and status = '5')");
        cotton.lib.MyDatabase.FastExec("update t_proposal_pricelist set on_behalf = '" + id + "' where on_behalf in(select id from m_user where noreg in (select noreg from m_user where id='" + id + "') and status = '5')");
        cotton.lib.MyDatabase.FastExec("update t_proposal_detail set created_by = '" + id + "' where created_by in(select id from m_user where noreg in (select noreg from m_user where id='" + id + "') and status = '5')");
        cotton.lib.MyDatabase.FastExec("update t_message set created_by = '" + id + "' where created_by in(select id from m_user where noreg in (select noreg from m_user where id='" + id + "') and status = '5')");
        cotton.lib.MyDatabase.FastExec("update m_user set status = '4' where noreg in (select noreg from m_user where id='" + id + "')");
        cotton.lib.MyDatabase.FastExec("update m_user set status = '5' where id = '"+id+"'");
        String to = "";
        cotton.lib.MyDatabase db = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader = db.Query("select a.name lm_name,a.email lm_email,b.id_job_title,b.*,m_job_title.job_title,sales_org_code from m_user a"
        +" inner join m_user b on a.noreg = b.noreg_lm"
        +" inner join m_job_title on m_job_title.id = b.id_job_title"
        +" inner join m_sales_org on m_sales_org.id = b.id_sales_org"
        +" where b.id = '"+id+"' and a.status = '5'");
        String body = "";
        while (reader.Read()) {
            cotton.lib.MyDatabase db2 = new cotton.lib.MyDatabase();
            System.Data.SqlClient.SqlDataReader reader2 = db2.Query("select DIVNM,CATNM,BRNNM,(select top 1 name from m_user inner join m_user_segments on m_user.id = id_user where segments = SECCD and id_job_title = '"+reader["id_job_title"].ToString()+"' and m_user.id != '"+id+"' and m_user.status = 5 ) name from m_user_segments inner join m_hierarchy on SECCD = segments where id_user = '"+id+"'");
            String divisi ="";
            String category = "";
            String brand ="";
            String duplicate ="";
            while (reader2.Read()) {
                if (!divisi.Contains(reader2["DIVNM"].ToString())) {
                    divisi += reader2["DIVNM"].ToString()+",";
                }
                if (!category.Contains(reader2["CATNM"].ToString())) {
                    category += reader2["CATNM"].ToString()+",";
                }
                if (!brand.Contains(reader2["BRNNM"].ToString())) {
                    brand += reader2["BRNNM"].ToString()+",";
                }
                if (!duplicate.Contains(reader2["name"].ToString())) {
                    duplicate += reader2["name"].ToString()+",";
                }
            }
            if (divisi.Length > 0) {
                divisi = divisi.Substring(0, divisi.Length-1);
            }
            if (category.Length > 0) {
                category = category.Substring(0, category.Length-1);
            }
            if (brand.Length > 0) {
                brand = brand.Substring(0, brand.Length-1);
            }
            if (duplicate.Length > 0) {
                duplicate = duplicate.Substring(0, duplicate.Length-1);
            }
            reader2.Close();
            db2.Close();
            body += "Dear " + reader["name"] +"<br/>";
            body += "<br/>";
            body += "Your cotton account has been successfully activated<br/>";
            if (duplicate.Length > 0)
            {
                body += "Please note that your account is duplicating "+duplicate;
            }
            body += "<br/>";
            body += "Please contact Admin if you have questions<br/>";
            body += "<br/>";
            body += "Disclaimer<br/>";
            body += "Please DO NOT REPLAY to this system generated mailing";
            to = reader["email"].ToString();
        }
        reader.Close();
        db.Close();
        cotton.lib.EmailClass.SendMail(to, "[Cotton] Account Activation", body);

    }
    if (status == "5")
    {
        cotton.lib.MyDatabase.FastExec("update m_user set status = '4' where id = '"+id+"'");
        String to = "";
        cotton.lib.MyDatabase db = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader = db.Query("select a.name lm_name,a.email lm_email,b.id_job_title,b.*,m_job_title.job_title,sales_org_code from m_user a"
        +" inner join m_user b on a.noreg = b.noreg_lm"
        +" inner join m_job_title on m_job_title.id = b.id_job_title"
        +" inner join m_sales_org on m_sales_org.id = b.id_sales_org"
        +" where b.id = '"+id+"' and a.status = '5'");
        String body = "";
        while (reader.Read()) {
            body += "Dear " + reader["name"] +"<br/>";
            body += "<br/>";
            body += "Your account has been deactivated. Please contact admin if you have any questions<br/>";
            body += "<br/>";
            body += "Disclaimer<br/>";
            body += "Please DO NOT REPLAY to this system generated mailing";
            to = reader["email"].ToString();
        }
        reader.Close();
        db.Close();
        cotton.lib.EmailClass.SendMail(to, "[Cotton] Account Deactivation", body);
    }
    if (status == "5")
    {
        cotton.lib.MyDatabase.FastExec("insert into h_activity([table],id_ref,[action],created_by,created_date,on_behalf) values ('m_user','" + id + "','Set Inactive','" + Session["id"].ToString() + "',getdate(),'" + Session["id_on_behalf"].ToString() + "')");
    }
    else {
        cotton.lib.MyDatabase.FastExec("insert into h_activity([table],id_ref,[action],created_by,created_date,on_behalf) values ('m_user','"+id+"','Approve User','"+Session["id"].ToString()+"',getdate(),'" + Session["id_on_behalf"].ToString() + "')");
    }
%>