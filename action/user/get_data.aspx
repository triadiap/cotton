﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    String sql = "";
    if (Session["id_job_title"].ToString() == "1")
    {
        sql = "select m_user.*,m_job_title.job_title,r_user_status.status user_status from m_user  inner join m_job_title on id_job_title = m_job_title.id inner join r_user_status on r_user_status.id = m_user.status order by m_user.id desc";
    }
    else if (Session["id_job_title"].ToString() == "14")
    {
        sql = "select m_user.*,m_job_title.job_title,r_user_status.status user_status from m_user  inner join m_job_title on id_job_title = m_job_title.id inner join r_user_status on r_user_status.id = m_user.status where m_user.status = '2' or (noreg_lm = '"+Session["noreg"].ToString()+"' and m_user.status = '1') order by m_user.id desc";
    }
    else {
        sql = "select m_user.*,m_job_title.job_title,r_user_status.status user_status from m_user  inner join m_job_title on id_job_title = m_job_title.id inner join r_user_status on r_user_status.id = m_user.status where noreg_lm = '"+Session["noreg"].ToString()+"' and m_user.status = '1' order by m_user.id desc";
    }
    System.Data.SqlClient.SqlDataReader reader = database.Query(sql);
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["name"] + "</td>");
        Response.Write("<td>" + reader["email"] + "</td>");
        Response.Write("<td>" + reader["job_title"] + "</td>");
        Response.Write("<td>" + reader["user_status"] + "</td>");
        Response.Write("<td style='text-align:center'>");
        if (Session["id_job_title"].ToString() == "1")
        {
            if (reader["status"].ToString() == "4" || reader["status"].ToString() == "3")
            {
                Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='View'  onclick='do_info("+reader["id"].ToString()+")'><i class='fa fa-info'></i></a>&nbsp;");
                Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Set Inactive'  onclick='do_approve(" + reader["id"].ToString() + "," + reader["status"].ToString() + ")'><i class='fa fa-thumbs-up'></i></a>");
            }
            else if(reader["status"].ToString() == "5"){
                Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='View'  onclick='do_info("+reader["id"].ToString()+")'><i class='fa fa-info'></i></a>&nbsp;");
                Response.Write("<a class='btn btn-xs btn-danger' data-toggle='tooltip' title='Set Active'  onclick='do_approve(" + reader["id"].ToString() + "," + reader["status"].ToString() + ")'><i class='fa fa-thumbs-down'></i></a>");
            }
        }
        else if (Session["id_job_title"].ToString() == "14")
        {
            Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='View'  onclick='do_info("+reader["id"].ToString()+")'><i class='fa fa-info'></i></a>&nbsp;");
            Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Approve'  onclick='do_approve("+reader["id"].ToString()+","+reader["status"].ToString()+")'><i class='fa fa-thumbs-up'></i></a>&nbsp;");
            Response.Write("<a class='btn btn-xs btn-danger' data-toggle='tooltip' title='Reject'  onclick='do_reject("+reader["id"].ToString()+","+reader["status"].ToString()+")'><i class='fa fa-thumbs-down'></i></a>");
        }
        else {
            Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='View' onclick='do_info("+reader["id"].ToString()+")'><i class='fa fa-info'></i></a>&nbsp;");
            if (reader["status"].ToString() != "2")
            {
                Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Approve' onclick='do_approve(" + reader["id"].ToString() + "," + reader["status"].ToString() + ")'><i class='fa fa-thumbs-up'></i></a>&nbsp;");
                Response.Write("<a class='btn btn-xs btn-danger' data-toggle='tooltip' title='Reject' onclick='do_reject(" + reader["id"].ToString() + "," + reader["status"].ToString() + ")'><i class='fa fa-thumbs-down'></i></a>");
            }
        }
        Response.Write("</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>