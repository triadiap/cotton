﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
	String select_option = Request.Form["select_option"].ToString();
    String id = Request.Form["id"].ToString();
    String sqlQuery = "";

    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
	System.Data.SqlClient.SqlDataReader reader = database.Query("select distinct id_sales_region from m_mapping_sales_region where id_sales_region='" + id + "'");
	while (reader.Read())
	{
		if(select_option=="edit"){
			Response.Write("{");
			Response.Write("'id_sales_region':'" + reader["id_sales_region"] + "'");
			Response.Write("}");
		}else if(select_option=="area-list"){
			String sqlDivision = "select a.*, b.id_sales_region, b.id_area, c.area_name from m_division a "
	        	+ "left join m_mapping_sales_region b on a.id=b.id_division and b.id_sales_region='" + id + "' "
	        	+ "left join m_area c on c.id=b.id_area "
	        	+ "order by a.id ";
	        cotton.lib.MyDatabase database2 = new cotton.lib.MyDatabase();
			System.Data.SqlClient.SqlDataReader reader2 = database2.Query(sqlDivision);
			while (reader2.Read())
			{
				Response.Write("<div class='form-group col-md-12'>");
	            Response.Write("    <label class='col-md-4'>" + reader2["division_code"].ToString().Trim() + "</label>");
	            Response.Write("    <div class='col-md-8'>");
	            Response.Write("    <select id='id_division" + reader2["id"] + "' name='id_division" + reader2["id"] + "' class='form-control' autocomplete='on'>");

				cotton.lib.MyDatabase databaseDiv = new cotton.lib.MyDatabase();
	            System.Data.SqlClient.SqlDataReader readerDiv = databaseDiv.Query("select * from m_area order by id");
	            Response.Write("<option value='' selected>Please select</option>");
				while (readerDiv.Read())
				{
					if(reader2["id_area"].ToString().Trim()==readerDiv["id"].ToString().Trim())
						Response.Write("<option value='"+readerDiv["id"].ToString()+"' selected>" + readerDiv["area_name"] + "</option>");
					else
						Response.Write("<option value='"+readerDiv["id"].ToString()+"'>" + readerDiv["area_name"] + "</option>");
				}
				readerDiv.Close();
				databaseDiv.Close();

	            Response.Write("    </select>");
	            Response.Write("        <div class='show-result show-result-nama no-margin'></div>");
	            Response.Write("    </div>");
	            Response.Write("</div>");
			}
			reader2.Close();
    		database2.Close();
    	}

	}
    reader.Close();
    database.Close();
%>