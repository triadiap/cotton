﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
	String select_option = Request.Form["select_option"].ToString();
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
	if (select_option=="id_sales_region"){
		System.Data.SqlClient.SqlDataReader reader = database.Query("select * from m_sales_region");
		while (reader.Read())
		{
			Response.Write("<option value='"+reader["id"].ToString()+"'>" + reader["sales_region_code"] + " - " + reader["sales_region_desc"] + "</option>");
		}
		reader.Close();
	}else if(select_option=="area-list"){
		System.Data.SqlClient.SqlDataReader reader = database.Query("select * from m_division order by id");
		while (reader.Read())
		{
			Response.Write("<div class='form-group col-md-12'>");
            Response.Write("    <label class='col-md-4'>" + reader["division_code"].ToString().Trim() + "</label>");
            Response.Write("    <div class='col-md-8'>");
            Response.Write("    <select id='id_division" + reader["id"] + "' name='id_division' class='form-control' autocomplete='on'>");

			cotton.lib.MyDatabase databaseDiv = new cotton.lib.MyDatabase();
            System.Data.SqlClient.SqlDataReader readerDiv = databaseDiv.Query("select * from m_area order by id");
            Response.Write("<option value='' selected>Please select</option>");
			while (readerDiv.Read())
			{
				Response.Write("<option value='"+readerDiv["id"].ToString()+"'>" + readerDiv["area_name"] + "</option>");
			}
			readerDiv.Close();
			databaseDiv.Close();

            Response.Write("    </select>");
            Response.Write("        <div class='show-result show-result-nama no-margin'></div>");
            Response.Write("    </div>");
            Response.Write("</div>");
		}
		reader.Close();
	}else if(select_option=="id_division_arr"){
		System.Data.SqlClient.SqlDataReader reader = database.Query("select * from m_division order by id");
		while (reader.Read())
		{
			Response.Write(reader["id"].ToString().Trim() + ",");
		}
		Response.Write("none");
		reader.Close();
	}else if(select_option=="id_area_arr"){
		System.Data.SqlClient.SqlDataReader reader = database.Query("select * from m_division order by id");
		while (reader.Read())
		{
			Response.Write("id_division"+reader["id"].ToString().Trim() + ",");
		}
		Response.Write("none");
		reader.Close();
	}
    database.Close();
%>