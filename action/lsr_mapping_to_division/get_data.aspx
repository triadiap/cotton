﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
	String sqlQuery = "";
    sqlQuery = "select distinct a.id_sales_region, b.sales_region_code, b.sales_region_desc "
		     + "from m_mapping_sales_region a "
			 + "inner join m_sales_region b on b.id=a.id_sales_region "
			 + "inner join m_division c on c.id=a.id_division "
			 + "inner join m_area d on d.id=a.id_area "
			 + "order by a.id_sales_region ";
	
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query(sqlQuery);
	int num = 1;
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + num.ToString().Trim() + "</td>");
        Response.Write("<td>" + reader["sales_region_code"] + "</td>");
        Response.Write("<td>" + reader["sales_region_desc"] + "</td>");
        cotton.lib.MyDatabase databaseDivision = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader readerDivision = databaseDivision.Query("select * from m_division order by id");
	    while (readerDivision.Read())
	    {
	    	Response.Write("<td>");
	    	String sqlArea = "select b.id_sales_region,a.id, b.id_area, c.area_name from m_division a "
        		+ "left join m_mapping_sales_region b on a.id=b.id_division and b.id_sales_region='" + reader["id_sales_region"] + "' "
        		+ "left join m_area c on c.id=b.id_area where a.id = '"+readerDivision["id"].ToString()+"'"
        		+ "order by a.id ";
		    cotton.lib.MyDatabase databaseArea = new cotton.lib.MyDatabase();
		    System.Data.SqlClient.SqlDataReader readerArea = databaseArea.Query(sqlArea);
		    while (readerArea.Read())
	    	{
				Response.Write(readerArea["area_name"]+" ");	
			}
		    readerArea.Close();
		    databaseArea.Close();
			Response.Write("</td>");
	    }
	    readerDivision.Close();
	    databaseDivision.Close();
		Response.Write("<td>");
        Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Edit' onclick='do_edit("+reader["id_sales_region"].ToString()+")'><i class='fa fa-pencil'></i></a>");
        Response.Write("&nbsp;<a class='btn btn-xs btn-danger' data-toggle='tooltip' title='Delete' onclick='do_delete("+reader["id_sales_region"].ToString()+",\""+reader["sales_region_code"]+"\")'><i class='fa fa-trash'></i></a>");
        Response.Write("</td>");
        Response.Write("</tr>");
		num=num+1;
    }
    reader.Close();
    database.Close();
%>