﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    foreach (String f in Request.Files.AllKeys) {
        var file = Request.Files[f];
        String []tmp = file.FileName.Split('\\');
        String name = tmp[tmp.Length - 1];
        file.SaveAs(Server.MapPath("~/Files/" + name));
        Response.Write(name);
    }
%>