﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase.FastExec("delete from tmp_proposal_channel where id_proposal_detail in(select id from tmp_proposal_detail where created_by = '" + Session["id"].ToString() + "')");
    cotton.lib.MyDatabase.FastExec("delete from tmp_proposal_detail where created_by = '" + Session["id"].ToString() + "'");

    String []AlphabetIndex = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
    String filename = Request.Form["name"].ToString();
    System.IO.FileInfo newFile = new System.IO.FileInfo(Server.MapPath("~/Files/"+filename));
    OfficeOpenXml.ExcelPackage pck = new OfficeOpenXml.ExcelPackage(newFile);
    var ws = pck.Workbook.Worksheets[1];
    int index = 3;
    int totalError = 0;
    int total = 0;
    while (ws.Cells["B" + index].Value != null) {
        total++;
        String sku = ws.Cells["B" + index].Value.ToString();
        String sku_name = ws.Cells["C" + index].Value.ToString();
        sku_name = sku_name.Replace("'","''");
        String dozen = ws.Cells["E" + index].Value.ToString();
        String configuration = ws.Cells["D" + index].Value.ToString();
        if(configuration == "0"){
        	String[] tmp = sku_name.TrimEnd().Split(' ');
        	configuration = tmp[tmp.Length - 1];
        }
        String ipack = ws.Cells["F" + index].Value.ToString();
        String id_callendar = "";
        try{
	        id_callendar = ws.Cells["I" + index].Value.ToString();
        }catch(Exception e){}
        String id_area = "";
        try{
        	id_area = ws.Cells["H" + index].Value.ToString();
        }catch(Exception e){}
        String remark = ws.Cells["A" + index].Value.ToString();
        String toko_price = "";
        if(ws.Cells["R" + index].Value != null){
        	toko_price = ws.Cells["R" + index].Value.ToString();
        }
        String rsp_price = "";
        if(ws.Cells["S" + index].Value != null){
        	rsp_price = ws.Cells["S" + index].Value.ToString();
        }
        String old_product ="";
        if(ws.Cells["AE" + index].Value != null){
        	old_product = ws.Cells["AE" + index].Value.ToString();
    	}
    	try{
	    	if(ws.Cells["U" + index].Value.ToString() == ""){
	        	ws.Cells["U" + index].Value = "0";
	    	}
    	}catch(Exception e){
    		ws.Cells["U" + index].Value = "0";
    	}
        String mdg_approval_date = "";
        if (ws.Cells["AD"+index].Value != null)
        {
        	DateTime dt =  DateTime.FromOADate(long.Parse(ws.Cells["AD"+index].Value.ToString()));
            mdg_approval_date = dt.Month +"/"+dt.Day+"/"+dt.Year;
        }
        String mdg_approval = "";
        try
        {
            if (ws.Cells["AC"+index].Value != null) {
                if (ws.Cells["AC"+index].Value.ToString() == "Yes")
                {
                    mdg_approval = "1";
                }
                else
                {
                    mdg_approval = "0";
                }
            }else{
                mdg_approval = "0";
            }
        }catch{

        }
        cotton.lib.MyDatabase db = new cotton.lib.MyDatabase();
        bool is_error = false;
        System.Data.SqlClient.SqlDataReader reader = db.Query("select * from m_callendar where convert(varchar(10),effective_date,120) = '" + id_callendar + "'");
        if (reader.Read())
        {
            id_callendar = reader["id"].ToString();
        }
        else {
            is_error = true;
            ws.Cells["AF"+index].Value = "Proposal can't be created in this effective callendar";
        }
        reader.Close();
        db.Close();
        db = new cotton.lib.MyDatabase();
        reader = db.Query("select * from m_proposal_remarks where  remark = '" + remark + "'");
        while (reader.Read()) {
            remark = reader["id"].ToString();
        }
        reader.Close();
        db.Close();
        db = new cotton.lib.MyDatabase();
        reader = db.Query("select * from m_area where  area_name = '" + id_area + "' and id_sales_org = '"+Session["id_sales_org"]+"'");
        if (reader.Read())
        {
            id_area = reader["id"].ToString();
        }
        else {
            is_error = true;
            ws.Cells["AF"+index].Value = "Proposal can't be created in this area";
        }
        reader.Close();
        db.Close();

        db = new cotton.lib.MyDatabase();
        reader = db.Query("select * from m_user inner join m_user_segments on m_user_segments.id_user = m_user.id inner join v_ers on v_ers.SECCD = m_user_segments.segments where m_user.id = '"+Session["id_on_behalf"]+"' and MATNR = '"+sku+"'");
        if (reader.Read())
        {

        }
        else {
            is_error = true;
            ws.Cells["AF"+index].Value = "Proposal can't be created in this product";
        }
        reader.Close();
        db.Close();

        int mapped_user_approval = 0;
        db = new cotton.lib.MyDatabase();
        reader = db.Query("select count(distinct m_user.id)  total from m_approval inner join m_user on m_user.id_job_title = id_app_job_title inner join m_user_segments on m_user_segments.id_user = m_user.id where segments in (select SECCD from v_ers where MATNR = '"+sku+"') and m_user.status = '5';");
        while (reader.Read())
        {
            mapped_user_approval = Convert.ToInt32(reader["total"].ToString());
        }
        reader.Close();
        db.Close();
        int mapped_approval = 0;
        db = new cotton.lib.MyDatabase();
        reader = db.Query("select count(app_level) total from m_approval where id_sales_org = '"+Session["id_sales_org_on_behalf"]+"';");
        while (reader.Read())
        {
            mapped_approval = Convert.ToInt32(reader["total"].ToString());
        }
        reader.Close();
        db.Close();

        int max_mapped_approval = 0;
        db = new cotton.lib.MyDatabase();
        reader = db.Query("select count(distinct app_level) total from m_approval where id_sales_org = '"+Session["id_sales_org_on_behalf"]+"';");
        while (reader.Read())
        {
            max_mapped_approval = Convert.ToInt32(reader["total"].ToString());
        }
        reader.Close();
        db.Close();

        int max_approval = 0;
        db = new cotton.lib.MyDatabase();
        reader = db.Query("select max_approval from m_sales_org where id = '"+Session["id_sales_org_on_behalf"]+"';");
        while (reader.Read())
        {
            max_approval = Convert.ToInt32(reader["max_approval"].ToString());
        }
        reader.Close();
        db.Close();
        if (max_mapped_approval < max_approval || mapped_user_approval < mapped_approval) {
            is_error = true;
            ws.Cells["AF"+index].Value = "You can not add this product, Approval level is incomplete please contact admin";
        }

        if (!is_error)
        {
            db = new cotton.lib.MyDatabase();
            reader = db.Query("SELECT * FROM [dbo].[tmp_proposal_detail] where sku = '" + sku + "' and id_area = '" + id_area + "' and status is null and created_by = '" + Session["id"].ToString() + "'");
            while (reader.Read())
            {
                is_error = true;
                ws.Cells["AF" + index].Value = "Proposal with SKU and area already added";
            }
            reader.Close();
            db.Close();
        }
        if (toko_price == "" || toko_price == "0") {
            is_error = true;
            ws.Cells["AF"+index].Value = "Product must have toko price";
        }else if (rsp_price == "" || rsp_price == "0") {
            is_error = true;
            ws.Cells["AF"+index].Value = "Product must have rsp price";
        }else if (remark == "1" && Convert.ToInt32(ws.Cells["U"+index].Value.ToString()) != 0) {
            is_error = true;
            ws.Cells["AF"+index].Value = "New sku must not have old toko price";
        }else if (remark == "3" && Convert.ToInt32(ws.Cells["U"+index].Value.ToString()) == 0) {
            is_error = true;
            ws.Cells["AF"+index].Value = "Price increase must have old toko price";
        }else if (remark == "4" && Convert.ToInt32(ws.Cells["U"+index].Value.ToString()) == 0) {
            is_error = true;
            ws.Cells["AF"+index].Value = "Price decrease must have old toko price";
        }else if (remark == "3" && (Convert.ToInt32(ws.Cells["R"+index].Value.ToString()) <= Convert.ToInt32(ws.Cells["U"+index].Value.ToString()))) {
            is_error = true;
            ws.Cells["AF"+index].Value = "Proposed price must be higher than current price";
        }else if (remark == "4" && (Convert.ToInt32(ws.Cells["R"+index].Value.ToString()) >= Convert.ToInt32(ws.Cells["U"+index].Value.ToString()))) {
            is_error = true;
            ws.Cells["AF"+index].Value = "Proposed price must be lower than current price";
        }else if ((remark == "1" || remark == "2") && mdg_approval == "0") {
            is_error = true;
            ws.Cells["AF"+index].Value = "You need mdg approval for this product";
        }else if ((remark == "1" || remark == "2") && mdg_approval_date == "") {
            is_error = true;
            ws.Cells["AF"+index].Value = "MDG Approval date must be specified";
        }else if ((remark == "2" || remark == "5") && old_product == "") {
            is_error = true;
            ws.Cells["AF"+index].Value = "Promo and Relaunch must have old product";
        }
        old_product = old_product.Replace(" ","");
        if(old_product == ""){
        	old_product = "none";
		}
        if (!is_error)
        {
            db = new cotton.lib.MyDatabase();
            reader = db.Query("SELECT * FROM [dbo].[t_proposal_detail] where sku = '" + sku + "' and id_area = '" + id_area + "' and status in (2,3,4)");
            while (reader.Read())
            {
                is_error = true;
                ws.Cells["AF" + index].Value = "Proposal with SKU and area already submited";
            }
            reader.Close();
            db.Close();
        }
        if (is_error)
        {
            totalError++;
        }
        else {
            //Response.Write("insert into tmp_proposal_detail(id, sku, dozen, configuration, ipack, id_callendar, remark, toko_price, rsp_price, created_by, id_area, sku_name, is_new)"
            //+ " values('" + (Session["created_by"] + sku) + "', '" + sku + "','" + dozen + "','" + configuration + "','" + ipack + "','" + id_callendar + "','" + remark + "','" + toko_price + "','" + rsp_price + "','" + Session["id"].ToString() + "','" + id_area + "','" + sku_name + "',1)");
            //Response.End();
            bool isbatam = false;
        	String newId = System.Diagnostics.Stopwatch.GetTimestamp()+"";
            cotton.lib.MyDatabase.FastExec("insert into tmp_proposal_detail(id,sku,dozen,configuration,ipack,id_callendar,remark,toko_price,rsp_price,created_by,id_area,sku_name,is_new,mdg_approval,mdg_approval_date,old_sku)"
            + " values('"+newId+"', '" + sku + "','" + dozen + "','" + configuration + "','" + ipack + "','" + id_callendar + "','" + remark + "','" + toko_price + "','" + rsp_price + "','" + Session["id"].ToString() + "','" + id_area + "','" + sku_name + "',1,'"+mdg_approval+"','"+mdg_approval_date+"','"+old_product+"')");
	        String newIdBatam = System.Diagnostics.Stopwatch.GetTimestamp()+"";cotton.lib.MyDatabase.FastExec("insert into tmp_proposal_detail(id,sku,dozen,configuration,ipack,id_callendar,remark,toko_price,rsp_price,created_by,id_area,sku_name,is_new,mdg_approval,mdg_approval_date,old_sku)"
            + " select '"+newIdBatam+"', '" + sku + "','" + dozen + "','" + configuration + "','" + ipack + "','" + id_callendar + "','" + remark + "',(" + toko_price + "*harga_batam)/100," + rsp_price + ",'" + Session["id"].ToString() + "','8','" + sku_name + "',1,'"+mdg_approval+"','"+mdg_approval_date+"','"+old_product+"'  from tmp_proposal_detail inner join v_ers on MATNR = sku inner join m_division on m_division.division_code = v_ers.DIVCD inner JOIN m_mapping_harga_batam on m_mapping_harga_batam.id_area = tmp_proposal_detail.id_area and id_division = m_division.id where tmp_proposal_detail.id = '"+newId+"'");
            reader = db.Query("select top 1 id from tmp_proposal_detail where created_by = '" + Session["id"].ToString() + "' and status is null order by id desc");
            db = new cotton.lib.MyDatabase();
            reader = db.Query("select top 1 id,id_area from tmp_proposal_detail where created_by = '" + Session["id"].ToString() + "' and id = '"+newIdBatam+"'");
            while (reader.Read())
            {
                isbatam = true;
            }
            reader.Close();
            db.Close();
            for (int i = 9; i < 17; i++) {
                try
                {
                    if (ws.Cells[AlphabetIndex[i] + "2"].Value != null)
                    {
                        String id_channel = ws.Cells[AlphabetIndex[i] + "2"].Value.ToString();
                        cotton.lib.MyDatabase subdb = new cotton.lib.MyDatabase();
                        System.Data.SqlClient.SqlDataReader subreader = subdb.Query("select * from m_channel where channel = '" + id_channel + "'");
                        while (subreader.Read())
                        {
                            id_channel = subreader["id"].ToString();
                        }
                        subreader.Close();
                        subdb.Close();
                        if (ws.Cells[AlphabetIndex[i] + "" + index].Value.ToString() != "")
                        {
                            //Response.Write("insert into tmp_proposal_channel(id_proposal_detail,id_channel) values ('" + id + "','" + id_channel + "')");
                            //Response.End();
                            cotton.lib.MyDatabase.FastExec("insert into tmp_proposal_channel(id_proposal_detail,id_channel) values ('" + newId + "','" + id_channel + "')");
                            //if (isbatam) {
                            //    cotton.lib.MyDatabase.FastExec("insert into tmp_proposal_channel(id_proposal_detail,id_channel) values ('" +newIdBatam+ "','" + id_channel + "')");
                            //}
                        }
                    }
                }
                catch (Exception e) {

                }
            }
            if(isbatam){
            	cotton.lib.MyDatabase dbBatam = new cotton.lib.MyDatabase();
	            System.Data.SqlClient.SqlDataReader readerBatam = dbBatam.Query("select m_mapping_area_channel.id_channel from v_ers inner join m_division on m_division.division_code = v_ers.DIVCD"
				+" inner join m_mapping_area on m_mapping_area.id_division = m_division.id and m_mapping_area.id_area = '8'"
				+" inner join m_mapping_area_channel on m_mapping_area_channel.id_mapping_area = m_mapping_area.id"
				+" where v_ers.MATNR = '"+sku+"'");
	            while (readerBatam.Read())
	            {
	                cotton.lib.MyDatabase.FastExec("insert into tmp_proposal_channel(id_proposal_detail,id_channel) values ('" + newIdBatam+ "','" + readerBatam["id_channel"].ToString() + "')");
	            }
	            readerBatam.Close();
	            dbBatam.Close();
            }
        }

        cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
	    System.Data.SqlClient.SqlDataReader reader2 = database.Query("select * from t_configuration where sku = '"+sku+"'");
	    bool isAvailable = false;
	    while (reader2.Read())
	    {
	        isAvailable = true;
	    }
	    reader.Close();
	    database.Close();
	    if (isAvailable)
	    {
	        cotton.lib.MyDatabase.FastExec("update t_configuration set configuration = '" + configuration + "',dozen = '"+dozen+"' where sku = '" + sku + "'");
	    }
	    else {
	        cotton.lib.MyDatabase.FastExec("insert into t_configuration (sku,configuration,dozen) values ('" + sku + "','" + configuration + "','"+dozen+"')");
	    }
        index++;
    }
    cotton.lib.MyDatabase.FastExec("update tmp_proposal_detail set CPGCD = (select CPGCD from v_ers where MATNR = SKU);");
    cotton.lib.MyDatabase.FastExec("update tmp_proposal_detail set VKORG = (select VKORG from v_ers where MATNR = SKU);");
    pck.Save();
    Response.Write("{total:" + total + ",failed:" + totalError + "}");
%>