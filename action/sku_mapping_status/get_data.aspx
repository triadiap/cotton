﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select id, isnull(ers_status_code,'NULL') as ers_status_code,cotton_status_code from r_sku_status");
    while (reader.Read())
    {
        Response.Write("<tr>");
        if (reader["ers_status_code"]!=""){
            Response.Write("<td>" + reader["ers_status_code"] + "</td>");
        }else{
            Response.Write("<td>(blank)</td>");
        }
        Response.Write("<td>" + reader["cotton_status_code"] + "</td>");
        Response.Write("<td style='text-align:center'>");
        Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Edit' onclick='do_edit("+reader["id"].ToString()+")'><i class='fa fa-pencil'></i></a>");
        Response.Write("&nbsp;<a class='btn btn-xs btn-danger' data-toggle='tooltip' title='Delete' onclick='do_delete("+reader["id"].ToString()+",\""+reader["ers_status_code"]+"\")'><i class='fa fa-trash'></i></a>");
        Response.Write("</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>