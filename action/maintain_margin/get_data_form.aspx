﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
	String select_option = Request.Form["select_option"].ToString();
    String id = Request.Form["id"].ToString();
    String sqlQuery = "";
	int channelnum = 1;
    cotton.lib.MyDatabase databaseChannel = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader readerChannel = databaseChannel.Query("select * from m_channel order by id");
    sqlQuery = "select distinct a.id_division, b.division_code, b.division_desc, ";
	while(readerChannel.Read()){
		sqlQuery += "(select CAST((margin * 100) as DECIMAL(18,2)) margin from m_mapping_channel_margin where id_division=a.id_division and id_channel='" + readerChannel["id"].ToString() + "') as channel" + channelnum + ", ";
		channelnum=channelnum+1 ;
	}
    readerChannel.Close();
    databaseChannel.Close();
	
	sqlQuery+="from m_mapping_channel_margin a "
			+ "inner join m_division b on b.id=id_division " 
			+ "where a.id_division='" + id + "'";

    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query(sqlQuery.Replace(", from"," from"));
	int num = 1;
	while (reader.Read())
	{
		if(select_option=="edit"){
			Response.Write("{");
			Response.Write("'id_division':'" + reader["id_division"] + "'");
			Response.Write("}");
		}else if(select_option=="id_channel"){
			cotton.lib.MyDatabase databaseChannelCB = new cotton.lib.MyDatabase();
			System.Data.SqlClient.SqlDataReader readerChannelCB = databaseChannelCB.Query("select * from m_channel order by id");
			int channelnum2 = 1;
			while(readerChannelCB.Read()){
				if(channelnum2 <= channelnum-1){
					Response.Write("<div class='form-group col-md-12'><label class='col-md-4'>" + readerChannelCB["channel"].ToString().Trim() + "</label><div class='col-md-8'><input type='text' id='id_channel" + readerChannelCB["id"].ToString().Trim() + "' value='" + reader["channel"+channelnum2].ToString().Trim() + "' class='form-control' onkeyup='this.value = characterControl2(this.value);' placeholder='% (percent)' autocomplete='off'></div></div></div>");
					channelnum2=channelnum2+1;
				}
			}
			readerChannelCB.Close();
			databaseChannelCB.Close();
		}
	}
    reader.Close();
    database.Close();
%>