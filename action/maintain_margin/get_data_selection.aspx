﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
	String select_option = Request.Form["select_option"].ToString();
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
	if (select_option=="id_division"){
		System.Data.SqlClient.SqlDataReader reader = database.Query("select a.*, sales_org_code from m_division a inner join m_sales_org b on b.id=a.id_sales_org order by sales_org_code,division_code");
		while (reader.Read())
		{
			Response.Write("<option value='"+reader["id"].ToString()+"'>" + reader["sales_org_code"] + " - " + reader["division_desc"] + "</option>");
		}
		reader.Close();
	}else if(select_option=="id_channel"){
		System.Data.SqlClient.SqlDataReader reader = database.Query("select * from m_channel order by id");
		while (reader.Read())
		{
			Response.Write("<div class='form-group col-md-12'><label class='col-md-4'>" + reader["channel"].ToString().Trim() + "</label><div class='col-md-8'><input type='text' id='id_channel" + reader["id"].ToString().Trim() + "' class='form-control' onkeyup='this.value = characterControl2(this.value);' placeholder='% (percent)' autocomplete='off'></div></div></div>");
		}
		reader.Close();
	}else if(select_option=="id_channel_arr"){
		System.Data.SqlClient.SqlDataReader reader = database.Query("select * from m_channel order by id");
		while (reader.Read())
		{
			Response.Write(reader["id"].ToString().Trim() + ",");
		}
		Response.Write("none");
		reader.Close();
	}
    database.Close();
%>