﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
	string sqlQuery = "";
	sqlQuery = "select a.id, a.app_level, a.id_app_job_title, a.id_app_grade, b.job_title, c.sales_org_code "
		+ "from m_approval a  "
		+ "inner join m_job_title b on b.id=a.id_app_job_title "
        + "inner join m_sales_org c on c.id=a.id_sales_org "
        + "order by sales_org_code,app_level,job_title, id_app_grade ";
    System.Data.SqlClient.SqlDataReader reader = database.Query(sqlQuery);
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["sales_org_code"] + "</td>");
        Response.Write("<td>" + reader["app_level"] + "</td>");
        Response.Write("<td>" + reader["job_title"] + "</td>");
        //Response.Write("<td>" + reader["id_app_grade"] + "</td>");
        Response.Write("<td style='text-align:center'>");
        Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Edit' onclick='do_edit("+reader["id"].ToString()+")'><i class='fa fa-pencil'></i></a>");
        Response.Write("&nbsp;<a class='btn btn-xs btn-danger' data-toggle='tooltip' title='Delete' onclick='do_delete("+reader["id"].ToString()+",\""+reader["job_title"]+" - "+reader["id_app_grade"]+"\")'><i class='fa fa-trash'></i></a>");
        Response.Write("</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>