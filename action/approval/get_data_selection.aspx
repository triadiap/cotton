﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
	String select_option = Request.Form["select_option"].ToString();
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
	if (select_option=="id_app_job_title"){
		System.Data.SqlClient.SqlDataReader reader = database.Query("select * from m_job_title order by job_title");
		while (reader.Read())
		{
			Response.Write("<option value='"+reader["id"].ToString()+"'>" + reader["job_title"] + "</option>");
		}
		reader.Close();
	}else if(select_option=="id_app_grade"){
		System.Data.SqlClient.SqlDataReader reader = database.Query("select distinct left(grade,1) as app_grade from v_employee order by left(grade,1)");
		while (reader.Read())
		{
			Response.Write("<option value='"+reader["app_grade"].ToString()+"'>" + reader["app_grade"] + "</option>");
		}
		reader.Close();
	}else if (select_option=="id_sales_org"){
		System.Data.SqlClient.SqlDataReader reader = database.Query("select * from m_sales_org");
		while (reader.Read())
		{
			Response.Write("<option value='"+reader["id"].ToString()+"'>" + reader["sales_org_code"] + " - " + reader["company_code"] + "</option>");
		}
		reader.Close();
	}
    database.Close();
%>