﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 

    using (OfficeOpenXml.ExcelPackage pck = new OfficeOpenXml.ExcelPackage())
    {
        String sql =  "select top 1000 h_activity.*,a.name userby,b.name onbehalfby from h_activity"
        +" inner join m_user a on a.id = h_activity.created_by" 
        +" inner join m_user b on b.id = h_activity.on_behalf"
        +" order by h_activity.id desc ";
        cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader = database.Query(sql);
        OfficeOpenXml.ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Log Activity");
        int i = 1;
        ws.Cells["A1"].Value = "Table";
        ws.Cells["B1"].Value = "Action";
        ws.Cells["C1"].Value = "Date";
        ws.Cells["D1"].Value = "User";
        ws.Cells["E1"].Value = "Ob Behalf";
        ws.Cells["A1:E1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        ws.Cells["A1:E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
        ws.Cells["A1:E1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        i = 1;
        while (reader.Read()) {
            i++;
            ws.Cells["A" + i].Value = reader["table"].ToString();
            ws.Cells["B" + i].Value = reader["action"].ToString();
            ws.Cells["C" + i].Value = reader["created_date"].ToString();
            ws.Cells["D" + i].Value = reader["userby"].ToString();
            ws.Cells["E" + i].Value = reader["onbehalfby"].ToString();
        }
        ws.Cells["A1:E" + i].AutoFitColumns();
        ws.Cells["A1:E" + i].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A1:E" + i].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A1:E" + i].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A1:E" + i].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        reader.Close();
        database.Close();
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;  filename=Activity Log.xlsx");
        Response.BinaryWrite(pck.GetAsByteArray());
        Response.End();
    }
%>