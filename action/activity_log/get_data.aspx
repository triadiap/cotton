﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
	string sqlQuery = "";
	sqlQuery = "select top 1000 h_activity.*,a.name userby,b.name onbehalfby from h_activity"
        +" inner join m_user a on a.id = h_activity.created_by" 
        +" inner join m_user b on b.id = h_activity.on_behalf"
        +" order by h_activity.id desc ";
    System.Data.SqlClient.SqlDataReader reader = database.Query(sqlQuery);
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["table"] + "</td>");
        Response.Write("<td>" + reader["action"] + "</td>");
        Response.Write("<td>" + reader["created_date"] + "</td>");
        Response.Write("<td>" + reader["userby"] + "</td>");
        Response.Write("<td>" + reader["onbehalfby"] + "</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>