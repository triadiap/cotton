﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
	string sqlQuery = "";
	sqlQuery = "select a.id, a.id_division, a.code , b.division_code "
		+ "from m_mapping_new_division_2 a "
		+ "left join m_division b on b.id=a.id_division "
		+ "order by a.id_division ";
    System.Data.SqlClient.SqlDataReader reader = database.Query(sqlQuery);
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["division_code"] + "</td>");

        String code=reader["code"].ToString().Trim();
        String division_number = "",sub_division="",category="",brand="",sub_brand="";

        if(code.Length >= 1)
            division_number=code.Substring(0,1);
        if(code.Length > 1)
            sub_division=code.Substring(0,2);
        if(code.Length >= 4)
            category=code.Substring(0,4);
        if(code.Length >= 9)
            brand=code.Substring(0,9);
        if(code.Length > 9)
            sub_brand=code;

        Response.Write("<td>" + division_number + "</td>");
        Response.Write("<td>" + sub_division + "</td>");
        Response.Write("<td>" + category + "</td>");
        Response.Write("<td>" + brand + "</td>");
        Response.Write("<td>" + sub_brand + "</td>");
        Response.Write("<td>");
        Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Edit' onclick='do_edit("+reader["id"].ToString()+")'><i class='fa fa-pencil'></i></a>");
        Response.Write("&nbsp;<a class='btn btn-xs btn-danger' data-toggle='tooltip' title='Delete' onclick='do_delete("+reader["id"].ToString()+",\""+reader["division_code"]+" - "+reader["code"]+"\")'><i class='fa fa-trash'></i></a>");
        Response.Write("</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>