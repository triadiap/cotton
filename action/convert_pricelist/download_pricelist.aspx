﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    StringBuilder sb = new StringBuilder();

    String id = Request.QueryString["id"].ToString();
    String id_division = Request.QueryString["id_division"].ToString();
    String pricelistNumber = "";
    String publishDate = "";
    String []AlphabetIndex = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
    bool migration = false;
    cotton.lib.MyDatabase mDb = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader mReader = mDb.Query("select pricelist_number,convert(varchar,pricelist_date,103) pricelist_date, isnull(created_by,1) migration  from t_pricelist where id = '"+id+"'");
    while (mReader.Read()) {
        pricelistNumber = mReader["pricelist_number"].ToString();
        publishDate = mReader["pricelist_date"].ToString();
        if(mReader["migration"].ToString() == "1"){
            migration = true;
        }
    }
    mReader.Close();
    mDb.Close();
    String sql = "";
    if (migration)
    {
        sql = "select t_proposal_detail.vkorg,sales_region_code,sales_region_desc,t_proposal_detail.sku,cast(round(((t_proposal_detail.toko_price * t_proposal_detail.ipack) / 1.1),0) as decimal(18,0)) toko_price,'IDR' currency,1 field1,'CS' field2,convert(VARCHAR,effective_date,104) publish_date,'31.12.9999' end_date from t_proposal_detail "
        + " inner join m_division on division_code = divcd"
        + " inner JOIN m_mapping_sales_region on m_mapping_sales_region.id_area = t_proposal_detail.id_area and m_division.id = m_mapping_sales_region.id_division"
        + " inner join m_sales_region on m_sales_region.id = m_mapping_sales_region.id_sales_region"
        + " inner join t_pricelist_detail on t_pricelist_detail.id_proposal = t_proposal_detail.id_proposal"
        + " inner join t_pricelist on t_pricelist.id = t_pricelist_detail.id_pricelist"
        + " inner join m_callendar on m_callendar.id = t_proposal_detail.id_callendar"
        + " inner JOIN t_proposal_detail_new ON t_proposal_detail.id_area = t_proposal_detail_new.id_area AND t_proposal_detail_new.sku = t_proposal_detail.sku AND t_proposal_detail_new.id_proposal = t_proposal_detail.id_proposal"
        + " where t_pricelist_detail.id_pricelist = '" + id + "'";
    }
    else
    {
        sql = "select vkorg,sales_region_code,sales_region_desc,sku,cast(round(((t_proposal_detail.toko_price * t_proposal_detail.ipack) / 1.1),0) as decimal(18,0)) toko_price,'IDR' currency,1 field1,'CS' field2,convert(VARCHAR,effective_date,104) publish_date,'31.12.9999' end_date from t_proposal_detail "
        + " inner join m_division on division_code = divcd"
        + " inner JOIN m_mapping_sales_region on m_mapping_sales_region.id_area = t_proposal_detail.id_area and m_division.id = m_mapping_sales_region.id_division"
        + " inner join m_sales_region on m_sales_region.id = m_mapping_sales_region.id_sales_region"
        + " inner join t_pricelist_detail on t_pricelist_detail.id_proposal = t_proposal_detail.id_proposal"
        + " inner join t_pricelist on t_pricelist.id = t_pricelist_detail.id_pricelist"
        + " inner join m_callendar on m_callendar.id = t_proposal_detail.id_callendar"
        + " where t_pricelist_detail.id_pricelist = '" + id + "'";
    }
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query(sql);
    while (reader.Read())
    {
        sb.AppendLine(reader["vkorg"].ToString() + "\t" + reader["sales_region_code"].ToString() + "\t"  + reader["sku"].ToString() + "\t" + reader["toko_price"].ToString() + "\t" + reader["currency"].ToString() + "\t" + reader["field1"].ToString() + "\t" + reader["field2"].ToString() + "\t" + reader["publish_date"].ToString() + "\t" + reader["end_date"].ToString());
    }
    reader.Close();
    database.Close();
    string text = sb.ToString();

    Response.Clear();
    Response.ClearHeaders();

    Response.AddHeader("Content-Length", text.Length.ToString());
    Response.ContentType = "text/plain";
    Response.AppendHeader("content-disposition", "attachment;filename=\""+pricelistNumber+".txt\"");

    Response.Write(text);
    Response.End();
    cotton.lib.MyDatabase.FastExec("insert into h_activity([table],id_ref,[action],created_by,created_date,on_behalf) values ('t_pricelist','"+id+"','Convert Pricelist To TXT','"+Session["id"].ToString()+"',getdate(),'" + Session["id_on_behalf"].ToString() + "')");
%>