﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select pricelist_number,proposal_number,t_proposal_detail.toko_price,t_proposal_detail.rsp_price,(((t_proposal_detail.rsp_price-t_proposal_detail.toko_price)/t_proposal_detail.toko_price)*100),t_new_proposed_price.toko_price,t_new_proposed_price.rsp_price,(((t_new_proposed_price.rsp_price - t_new_proposed_price.toko_price)/ t_new_proposed_price.toko_price)*100),t_proposal_detail.id,t_proposal_detail.sku,area_name   from t_pricelist inner join t_pricelist_detail on t_pricelist.id = id_pricelist inner join t_proposal_pricelist on t_proposal_pricelist.id = t_pricelist_detail.id_proposal inner join t_proposal_detail on t_proposal_detail.id_proposal= t_proposal_pricelist.id  inner join m_area on m_area.id = t_proposal_detail.id_area inner join t_new_proposed_price on t_new_proposed_price.id_proposal_detail = t_proposal_detail.id");
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["pricelist_number"] + "</td>");
        Response.Write("<td>" + reader["proposal_number"] + "</td>");
        Response.Write("<td>" + reader["sku"] + "</td>");
        Response.Write("<td>" + reader["area_name"] + "</td>");
        Response.Write("<td>" + Convert.ToDouble(reader[2].ToString().TrimEnd()).ToString("#,##0.00") + "</td>");
        Response.Write("<td>" + Convert.ToDouble(reader[3].ToString().TrimEnd()).ToString("#,##0.00") + "</td>");
        Response.Write("<td>" + Convert.ToDouble(reader[4].ToString().TrimEnd()).ToString("#,##0.00") + "</td>");
        Response.Write("<td>" + Convert.ToDouble(reader[5].ToString().TrimEnd()).ToString("#,##0.00") + "</td>");
        Response.Write("<td>" + Convert.ToDouble(reader[6].ToString().TrimEnd()).ToString("#,##0.00") + "</td>");
        Response.Write("<td>" +Convert.ToDouble(reader[7].ToString().TrimEnd()).ToString("#,##0.00") + "</td>");
        Response.Write("<td style='text-align:center'>");
        Response.Write("<button class='btn btn-primary btn-xs' data-toggle='tooltip' title='Approve' onclick='do_approve("+reader["id"].ToString()+")'><i class='fa  fa-thumbs-up'></i></button>&nbsp;");
        Response.Write("<button class='btn btn-danger btn-xs' data-toggle='tooltip' title='Reject' onclick='do_reject("+reader["id"].ToString()+")'><i class='fa  fa-thumbs-down'></i></button>");
        Response.Write("</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>