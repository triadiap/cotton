﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select t_delegation.id,convert(varchar,datestart,103) start,convert(varchar,datestop,103) stop,m_user.name from t_delegation inner join m_user on m_user.id = id_delegate_to where id_user = '"+Session["id"].ToString()+"'");
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["name"] + "</td>");
        Response.Write("<td>" + reader["start"] + "</td>");
        Response.Write("<td>" + reader["stop"] + "</td>");
        Response.Write("<td style='text-align:center'>");
        Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Edit' onclick='do_edit("+reader["id"].ToString()+")'><i class='fa fa-pencil'></i></a>");
        Response.Write("&nbsp;<a class='btn btn-xs btn-danger' data-toggle='tooltip' title='Delete' onclick='do_delete("+reader["id"].ToString()+",\""+reader["name"]+"\")'><i class='fa fa-trash'></i></a>");
        Response.Write("</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>