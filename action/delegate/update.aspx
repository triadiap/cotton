﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    String id = Request.Form["id"].ToString();
    String start = Request.Form["start"].ToString();
    String []tmp = start.Split('/');
    start = tmp[2] + "-" + tmp[1] + "-" + tmp[0];
    String stop = Request.Form["stop"].ToString();
    tmp = stop.Split('/');
    stop = tmp[2] + "-" + tmp[1] + "-" + tmp[0];
    cotton.lib.MyDatabase db = new cotton.lib.MyDatabase();
    //Response.Write("select * from t_delegation where ('" + start + "' >= datestart and '" + start + "' <= dateadd(day,1,datestop)) or ('" + stop + "' >= datestart and '" + stop + "' <= dateadd(day,1,datestop)) or ( datestart >= '" + start + "' and datestart <= dateadd(day,1,'" + stop + "')) or ( datestop >= '" + start + "' and datestop <= dateadd(day,1,'" + stop + "'))");
    System.Data.SqlClient.SqlDataReader reader = db.Query("select * from t_delegation where (('" + start + "' >= datestart and '" + start + "' < dateadd(day,1,datestop)) or ('" + stop + "' >= datestart and '" + stop + "' < dateadd(day,1,datestop)) or ( datestart >= '" + start + "' and datestart < dateadd(day,1,'" + stop + "')) or ( datestop >= '" + start + "' and datestop < dateadd(day,1,'" + stop + "'))) and id_user = '"+Session["id_on_behalf"].ToString()+"' and id != '"+id+"'");
    bool isError = false;
    while (reader.Read()) {
        isError = true;
    }
    reader.Close();
    db.Close();
    if (isError) {
        Response.Write("{'status':'0','msg':'Duplicate date in your delegation'}");
        Response.End();
    }
    if (id == "0")
    {
        cotton.lib.MyDatabase.FastExec("insert into t_delegation(id_user,datestart,datestop,id_delegate_to) values('" + Session["id"].ToString() + "','" + start + "','" + stop + "',(select top 1 id from m_user where noreg = (select top 1 noreg_lm from m_user where id = '" + Session["id"].ToString() +"')))");
        cotton.lib.MyDatabase.FastExec("insert into h_activity([table],id_ref,[action],created_by,created_date,on_behalf) values ('t_delegation','0','Insert Data','"+Session["id"].ToString()+"',getdate(),'" + Session["id_on_behalf"].ToString() + "')");
    }
    else {
        cotton.lib.MyDatabase.FastExec("update t_delegation set datestart = '"+start+"',datestop = '"+stop+"' where id = '"+id+"'");
        cotton.lib.MyDatabase.FastExec("insert into h_activity([table],id_ref,[action],created_by,created_date,on_behalf) values ('t_delegation','"+id+"','Update Data','"+Session["id"].ToString()+"',getdate(),'" + Session["id_on_behalf"].ToString() + "')");
    }
    String to = "";
    cotton.lib.MyDatabase dbMail = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader readerMail = dbMail.Query("select a.name lm_name,a.email lm_email,b.id_job_title,b.*,m_job_title.job_title,sales_org_code from m_user a"
    +" inner join m_user b on a.noreg = b.noreg_lm"
    +" inner join m_job_title on m_job_title.id = b.id_job_title"
    +" inner join m_sales_org on m_sales_org.id = b.id_sales_org"
    +" where b.id = '"+Session["id_on_behalf"].ToString()+"'");
    String body = "";
    while (readerMail.Read()) {
        body += "Dear " + readerMail["lm_name"].ToString() +"<br/>";
        body += "<br/>";
        body += "User "+readerMail["name"].ToString()+" has delegated his account to you for below specific time :<br/>";      
        body += "Time : "+Request.Form["start"].ToString()+" to "+Request.Form["stop"].ToString()+"<br/>";     
        body += "Role : "+readerMail["job_title"]+"<br/>";            
        body += "<br/>";
        body += "Please contact "+readerMail["email"].ToString()+" or admin if you have any questions<br/>";
        body += "<br/>";
        body += "Disclaimer<br/>";
        body += "Please DO NOT REPLAY to this system generated mailing";
        to = readerMail["lm_email"].ToString();
    }
    readerMail.Close();
    dbMail.Close();
    cotton.lib.EmailClass.SendMail(to, "[Cotton] Account Delegation", body);
    Response.Write("{'status':'1','msg':'Ok'}");
%>