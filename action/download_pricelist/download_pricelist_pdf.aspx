﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
	HttpContext.Current.Response.ContentType = "application/pdf";
    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);

    String id = Request.QueryString["id"].ToString();
    String id_division = Request.QueryString["id_division"].ToString();
    String []AlphabetIndex = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
    String []Bulan = {"", "Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Augstus", "September", "Oktober", "November", "Desember", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
    cotton.lib.MyDatabase mDb = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader mReader = mDb.Query("select * from r_generate_pricelist order by [order] asc");
    System.Collections.Generic.List<String> kolom_desc = new System.Collections.Generic.List<String>();
    System.Collections.Generic.List<String> kolom_name = new System.Collections.Generic.List<String>();
    while (mReader.Read()) {
        kolom_desc.Add(mReader["kolom_desc"].ToString());
        kolom_name.Add(mReader["kolom_name"].ToString());
    }
    mReader.Close();
    mDb.Close();
    String pricelistNumber ="";
    String publishDate = "";
    bool migration = false;
    mDb = new cotton.lib.MyDatabase();
    String getDate = "";
    mReader = mDb.Query("select pricelist_number,convert(varchar,pricelist_date,103) pricelist_date,pricelist_date get_date, isnull(created_by,1) migration  from t_pricelist where id = '"+id+"'");
    while (mReader.Read()) {
        pricelistNumber = mReader["pricelist_number"].ToString();
        publishDate = mReader["pricelist_date"].ToString();
        getDate = mReader["get_date"].ToString();
        if(mReader["migration"].ToString() == "1"){
        	migration = true;
        }
    }
	String pricelistFileName = "pricelist_"+pricelistNumber+"_"+DateTime.Now.Year+DateTime.Now.Month+DateTime.Now.Year+".pdf";
    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename="+pricelistFileName);
    mReader.Close();
    mDb.Close();
    String division = "";
    mDb = new cotton.lib.MyDatabase();
    mReader = mDb.Query("select * from m_division where id = '"+id_division+"'");
    while (mReader.Read()) {
    	division  = mReader["division_code"].ToString();
    }
    mReader.Close();
    mDb.Close();
    String []tmp = publishDate.Split('/');
    publishDate = tmp[0] + " " + Bulan[Convert.ToInt32(tmp[1])] + " " + tmp[2];
    iTextSharp.text.Font fontHeader = new iTextSharp.text.Font();
    fontHeader.Size = 11;
    iTextSharp.text.Font fontHeaderTable = new iTextSharp.text.Font();
    fontHeaderTable.Size = 8;
    iTextSharp.text.Font fontBodyTable = new iTextSharp.text.Font();
    fontBodyTable.Size = 6;
    iTextSharp.text.Font fontBodyTableBold = new iTextSharp.text.Font();
    fontBodyTableBold.Size = 6;
    fontBodyTableBold.SetStyle(iTextSharp.text.Font.BOLD);
    //System.IO.FileStream fs = new System.IO.FileStream(Server.MapPath(pricelistFileName), System.IO.FileMode.Create);
    iTextSharp.text.Document document = new iTextSharp.text.Document(iTextSharp.text.PageSize.LEGAL.Rotate());
    iTextSharp.text.pdf.PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, HttpContext.Current.Response.OutputStream);
	//iTextSharp.text.pdf.PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, fs);
    /*String sql = "select id_area,id_channel,area_name,channel,full_name from m_mapping_area"
    +" inner join m_mapping_area_channel on m_mapping_area.id = m_mapping_area_channel.id_mapping_area"
    +" inner join m_area on m_area.id = id_area"
    +" inner join m_channel on m_channel.id = id_channel"
    +" where id_division = '"+id_division+"'";*/
    String sql = "select m_area.id id_area,m_channel.id id_channel,area_name,channel,full_name from m_area inner join m_channel on 1=1 order by m_area.id";
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query(sql);
    iTextSharp.text.pdf.PdfPTable table = new iTextSharp.text.pdf.PdfPTable(17);
    iTextSharp.text.pdf.PdfPCell cell = new iTextSharp.text.pdf.PdfPCell();
    document.Open();
    while (reader.Read())
    {
        document.NewPage();
        iTextSharp.text.Image pic = iTextSharp.text.Image.GetInstance(Server.MapPath("~/asset/img/logo_pdf.png"));
        pic.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
        document.Add(pic);
        iTextSharp.text.Paragraph header = new iTextSharp.text.Paragraph("Daftar Harga",fontHeader);
        header.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
        document.Add(header);
        header = new iTextSharp.text.Paragraph("PT UNILEVER INDONESIA, Tbk ",fontHeader);
        header.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
        document.Add(header);
        header = new iTextSharp.text.Paragraph(pricelistNumber + " Tanggal : "+publishDate+" ",fontHeader);
        header.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
        document.Add(header);
        header = new iTextSharp.text.Paragraph(reader["full_name"].ToString().TrimEnd()+" - "+reader["area_name"].ToString().TrimEnd(),fontHeader);
        header.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
        document.Add(header);
        header = new iTextSharp.text.Paragraph("Daftar Harga sewaktu-waktu dapat diubah tanpa pemberitahuan  terlebih dahulu",fontBodyTable);
        document.Add(header);
        header = new iTextSharp.text.Paragraph(" ",fontBodyTable);
        document.Add(header);
        table = new iTextSharp.text.pdf.PdfPTable(18);
        float[] columnWidths = { 60, 100, 60, 60, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 60,70 };
        table.SetWidthPercentage(columnWidths, document.PageSize);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Kode Produk",fontHeaderTable));
        cell.Rowspan = 3;
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Nama produk",fontHeaderTable));
        cell.Rowspan = 3;
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Kemasan",fontHeaderTable));
        cell.Rowspan = 3;
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Barcode",fontHeaderTable));
        cell.Rowspan = 3;
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Jumlah Kemasan Per CS",fontHeaderTable));
        cell.Rowspan = 3;
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Mulai Berlaku",fontHeaderTable));
        cell.Rowspan = 3;
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Harga Eceran per Satuan",fontHeaderTable));
        cell.Rowspan = 3;
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        if (reader["channel"].ToString().Contains("GT"))
        {
            cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Harga Toko", fontHeaderTable));
        }
        else {
            cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Harga Supermarket", fontHeaderTable));
        }
        if (!reader["channel"].ToString().Contains("Dir"))
        {
            cell.Colspan = 6;
        }
        else {
            cell.Colspan = 9;
        }
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        if (!reader["channel"].ToString().Contains("Dir")) {
            cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Harga Distibutor Per CS",fontHeaderTable));
            cell.Rowspan = 2;
            cell.Colspan = 3;
            cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
            cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
            table.AddCell(cell);
        }
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Keterangan",fontHeaderTable));
        cell.Rowspan = 3;
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Old/Regular SKU",fontHeaderTable));
        cell.Rowspan = 3;
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Tanpa PPN",fontHeaderTable));
        cell.Colspan = 3;
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Dengan PPN",fontHeaderTable));
        cell.Colspan = 3;
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        if (reader["channel"].ToString().Contains("Dir"))
        {
            cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Dengan PPN & PPnBM", fontHeaderTable));
            cell.Colspan = 3;
            cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
            cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
            table.AddCell(cell);
        }
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per CS",fontHeaderTable));
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per Lusin",fontHeaderTable));
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per Satuan",fontHeaderTable));
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per CS",fontHeaderTable));
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per Lusin",fontHeaderTable));
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per Satuan",fontHeaderTable));
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        if (!reader["channel"].ToString().Contains("Dir"))
        {
            cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Tanpa PPN", fontHeaderTable));
            cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
            cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
            table.AddCell(cell);
            cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Dengan PPN", fontHeaderTable));
            cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
            cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
            table.AddCell(cell);
            cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Dengan PPN & PPnBM", fontHeaderTable));
            cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
            cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
            table.AddCell(cell);
        }
        else {
            cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per CS", fontHeaderTable));
            cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
            cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
            table.AddCell(cell);
            cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per Lusin", fontHeaderTable));
            cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
            cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
            table.AddCell(cell);
            cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per Satuan", fontHeaderTable));
            cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
            cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
            table.AddCell(cell);
        }
        cotton.lib.MyDatabase database2 = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader2 = database2.Query("select sku_code,sku_desc,v_pricelist.configuration,v_pricelist.barcode,v_pricelist.ipack,v_pricelist.eff_date,eceran_ppn,toko_fib_no_ppn,toko_lusin_no_ppn,toko_no_ppn,toko_fib_ppn,toko_lusin_ppn,toko_ppn,v_pricelist.remark,v_pricelist.id,old,distributor_no_ppn,distributor_ppn,id_pricelist,category, RD_NEW from v_pricelist" 
        +" where area ='"+reader["area_name"].ToString().TrimEnd()+"' and channel ='"+reader["channel"].ToString().TrimEnd()+"' and v_pricelist.id='"+id+"' and delisted_date is null order by category,sku_desc asc");
        int index = 0;
        String oldCat = "";
        String newCat = "";
        while (reader2.Read()) {
            index++;
            newCat = reader2["category"].ToString();
            if(newCat != oldCat){
            	cotton.lib.MyDatabase database3 = new cotton.lib.MyDatabase();
        		System.Data.SqlClient.SqlDataReader reader3 = database3.Query("select top 1 catnm from m_hierarchy where catcd = '"+newCat+"'");
        		while(reader3.Read()){
	            	cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader3["catnm"].ToString(),fontBodyTableBold));
			        cell.Colspan = 18;
			        cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
			        table.AddCell(cell);
		        }
		        reader3.Close();
		        database3.Close();
		        oldCat = newCat;
			}
            if ((reader2["id"].ToString() == reader2["id_pricelist"].ToString() && migration == false) || (migration == true && reader2["rd_new"].ToString()=="1"))
            {
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[0].ToString(), fontBodyTableBold));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[1].ToString(), fontBodyTableBold));
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[2].ToString(), fontBodyTableBold));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[3].ToString(), fontBodyTableBold));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[4].ToString(), fontBodyTableBold));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[5].ToString(), fontBodyTableBold));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2[6].ToString()).ToString("#,##0"), fontBodyTableBold));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2[7].ToString()).ToString("#,##0"), fontBodyTableBold));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2[8].ToString()).ToString("#,##0"), fontBodyTableBold));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2[9].ToString()).ToString("#,##0"), fontBodyTableBold));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2[10].ToString()).ToString("#,##0"), fontBodyTableBold));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2[11].ToString()).ToString("#,##0"), fontBodyTableBold));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2[12].ToString()).ToString("#,##0"), fontBodyTableBold));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                if (!reader["channel"].ToString().Contains("Dir"))
                {
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2["distributor_no_ppn"].ToString()).ToString("#,##0"), fontBodyTableBold));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2["distributor_ppn"].ToString()).ToString("#,##0"), fontBodyTableBold));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("-", fontBodyTableBold));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }else {
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("-", fontBodyTableBold));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("-", fontBodyTableBold));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("-", fontBodyTableBold));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[13].ToString(), fontBodyTableBold));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[15].ToString(), fontBodyTableBold));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
            }
            else
            {
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[0].ToString(), fontBodyTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[1].ToString(), fontBodyTable));
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[2].ToString(), fontBodyTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[3].ToString(), fontBodyTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[4].ToString(), fontBodyTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[5].ToString(), fontBodyTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2[6].ToString()).ToString("#,##0"), fontBodyTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2[7].ToString()).ToString("#,##0"), fontBodyTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2[8].ToString()).ToString("#,##0"), fontBodyTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2[9].ToString()).ToString("#,##0"), fontBodyTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2[10].ToString()).ToString("#,##0"), fontBodyTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2[11].ToString()).ToString("#,##0"), fontBodyTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2[12].ToString()).ToString("#,##0"), fontBodyTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                if (!reader["channel"].ToString().Contains("Dir"))
                {
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2["distributor_no_ppn"].ToString()).ToString("#,##0"), fontBodyTable));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(Convert.ToDouble(reader2["distributor_ppn"].ToString()).ToString("#,##0"), fontBodyTable));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("-", fontBodyTable));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }else {
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("-", fontBodyTable));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("-", fontBodyTable));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    table.AddCell(cell);
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("-", fontBodyTable));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    table.AddCell(cell);
                }
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[13].ToString(), fontBodyTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader2[15].ToString(), fontBodyTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                table.AddCell(cell);
            }
            if (table.TotalHeight >= 345 || index>=20) {
                document.Add(table);
                index = 0;
                document.NewPage();
                pic = iTextSharp.text.Image.GetInstance(Server.MapPath("~/asset/img/logo_pdf.png"));
                pic.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                document.Add(pic);
                header = new iTextSharp.text.Paragraph("Daftar Harga",fontHeader);
                header.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                document.Add(header);
                header = new iTextSharp.text.Paragraph("PT UNILEVER INDONESIA, Tbk ",fontHeader);
                header.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                document.Add(header);
                header = new iTextSharp.text.Paragraph(pricelistNumber + " Tanggal : "+publishDate+" ",fontHeader);
                header.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                document.Add(header);
                header = new iTextSharp.text.Paragraph(reader["full_name"].ToString().TrimEnd()+" - "+reader["area_name"].ToString().TrimEnd(),fontHeader);
                header.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                document.Add(header);
                header = new iTextSharp.text.Paragraph("Daftar Harga sewaktu-waktu dapat diubah tanpa pemberitahuan  terlebih dahulu",fontBodyTable);
                document.Add(header);
                header = new iTextSharp.text.Paragraph(" ",fontBodyTable);
                document.Add(header);
                table = new iTextSharp.text.pdf.PdfPTable(18);
                table.SetWidthPercentage(columnWidths, document.PageSize);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Kode Produk",fontHeaderTable));
                cell.Rowspan = 3;
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Nama produk",fontHeaderTable));
                cell.Rowspan = 3;
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Kemasan",fontHeaderTable));
                cell.Rowspan = 3;
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Barcode",fontHeaderTable));
                cell.Rowspan = 3;
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Jumlah Kemasan Per CS",fontHeaderTable));
                cell.Rowspan = 3;
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Mulai Berlaku",fontHeaderTable));
                cell.Rowspan = 3;
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Harga Eceran per Satuan",fontHeaderTable));
                cell.Rowspan = 3;
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                if (reader["channel"].ToString().Contains("GT"))
                {
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Harga Toko", fontHeaderTable));
                }
                else {
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Harga Supermarket", fontHeaderTable));
                }
                if (!reader["channel"].ToString().Contains("Dir"))
                {
                    cell.Colspan = 6;
                }
                else {
                    cell.Colspan =9;
                }
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                if (!reader["channel"].ToString().Contains("Dir")) {
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Harga Distibutor Per CS",fontHeaderTable));
                    cell.Rowspan = 2;
                    cell.Colspan = 3;
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                    table.AddCell(cell);
                }
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Keterangan",fontHeaderTable));
                cell.Rowspan = 3;
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Old/Regular SKU",fontHeaderTable));
                cell.Rowspan = 3;
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Tanpa PPN",fontHeaderTable));
                cell.Colspan = 3;
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Dengan PPN",fontHeaderTable));
                cell.Colspan = 3;
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                if (reader["channel"].ToString().Contains("Dir"))
                {
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Dengan PPN & PPnBM", fontHeaderTable));
                    cell.Colspan = 3;
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                    table.AddCell(cell);
                }
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per CS",fontHeaderTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per Lusin",fontHeaderTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per Satuan",fontHeaderTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per CS",fontHeaderTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per Lusin",fontHeaderTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);
                cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per Satuan",fontHeaderTable));
                cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cell);

                if (!reader["channel"].ToString().Contains("Dir"))
                {
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Tanpa PPN", fontHeaderTable));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                    table.AddCell(cell);
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Dengan PPN", fontHeaderTable));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                    table.AddCell(cell);
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Dengan PPN & PPnBM", fontHeaderTable));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                    table.AddCell(cell);
                }
                else {
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per CS", fontHeaderTable));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                    table.AddCell(cell);
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per Lusin", fontHeaderTable));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                    table.AddCell(cell);
                    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Per Satuan", fontHeaderTable));
                    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                    table.AddCell(cell);
                }
            }
        }
        reader2.Close();
        database2.Close();
        document.Add(table);
    }
    document.NewPage();
    iTextSharp.text.Image pic2 = iTextSharp.text.Image.GetInstance(Server.MapPath("~/asset/img/logo_pdf.png"));
    pic2.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
    document.Add(pic2);
    iTextSharp.text.Paragraph header2 = new iTextSharp.text.Paragraph("Daftar Harga",fontHeader);
    header2.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
    document.Add(header2);
    header2 = new iTextSharp.text.Paragraph("PT UNILEVER INDONESIA, Tbk ",fontHeader);
    header2.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
    document.Add(header2);
    header2 = new iTextSharp.text.Paragraph(pricelistNumber + " Tanggal : "+publishDate+" ",fontHeader);
    header2.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
    document.Add(header2);
    header2 = new iTextSharp.text.Paragraph("Produk Delisted",fontHeader);
    header2.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
    document.Add(header2);
    header2 = new iTextSharp.text.Paragraph(" ",fontBodyTable);
    document.Add(header2);

    // Delisted SKU
    iTextSharp.text.pdf.PdfPTable table2 = new iTextSharp.text.pdf.PdfPTable(4);
    float[] columnWidths2 = { 80, 150, 100, 100};
    table2.SetWidthPercentage(columnWidths2, document.PageSize);
    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Kode Produk",fontHeaderTable));
    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
    cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
    table2.AddCell(cell);
    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Nama produk",fontHeaderTable));
    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
    cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
    table2.AddCell(cell);
    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Kemasan",fontHeaderTable));
    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
    cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
    table2.AddCell(cell);
    cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Mulai Berlaku",fontHeaderTable));
    cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
    cell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
    table2.AddCell(cell);
    database = new cotton.lib.MyDatabase();
    reader = database.Query("select distinct sku_code,sku_desc,configuration,delisted_date from v_pricelist where id = '"+id+"' and datediff(day,delisted_date,'"+getDate+"') <= 92");
    while (reader.Read()) {
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader[0].ToString(), fontBodyTable));
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        table2.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader[1].ToString(), fontBodyTable));
        table2.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader[2].ToString(), fontBodyTable));
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        table2.AddCell(cell);
        cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(reader[3].ToString(), fontBodyTable));
        cell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
        table2.AddCell(cell);
    }
    reader.Close();
    database.Close();
    document.Add(table2);
    document.Close();
    writer.Close();
//    fs.Close();
//    Response.Redirect(pricelistFileName);

	HttpContext.Current.Response.Write(document);
    HttpContext.Current.Response.End();

%>