﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 

    using (OfficeOpenXml.ExcelPackage pck = new OfficeOpenXml.ExcelPackage())
    {
        String []AlphabetIndex = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        cotton.lib.MyDatabase mDb = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader mReader = mDb.Query("select * from r_generate_pricelist where [order] <> ''  order by [order] asc");
        System.Collections.Generic.List<String> kolom_desc = new System.Collections.Generic.List<String>();
        System.Collections.Generic.List<String> kolom_name = new System.Collections.Generic.List<String>();
        kolom_name.Add("brnnm");
        kolom_name.Add("catnm");
        kolom_desc.Add("brand");
        kolom_desc.Add("category");
        while (mReader.Read()) {
            kolom_desc.Add(mReader["kolom_desc"].ToString());
            kolom_name.Add(mReader["kolom_name"].ToString());
        }
        mReader.Close();
        mDb.Close();
        String id = Request.QueryString["id"].ToString();
        String id_division = Request.QueryString["id_division"].ToString();
        String division = "";
        mDb = new cotton.lib.MyDatabase();
        mReader = mDb.Query("select * from m_division where id = '"+id_division+"'");
        while (mReader.Read()) {
            division  = mReader["division_code"].ToString();
        }
        mReader.Close();
        mDb.Close();
        String pricelistNumber ="";
        String publishDate = "";
        bool migration = false;
        mDb = new cotton.lib.MyDatabase();
        mReader = mDb.Query("select pricelist_number,convert(varchar,pricelist_date,103) pricelist_date,isnull(created_by,1) migration from t_pricelist where id = '"+id+"'");
        while (mReader.Read()) {
            pricelistNumber = mReader["pricelist_number"].ToString();
            publishDate = mReader["pricelist_date"].ToString();
            if(mReader["migration"].ToString() == "1"){
                migration = true;
            }
        }
        mReader.Close();
        mDb.Close();
        /*String sql = "select id_area,id_channel,area_name,channel from m_mapping_area"
        +" inner join m_mapping_area_channel on m_mapping_area.id = m_mapping_area_channel.id_mapping_area"
        +" inner join m_area on m_area.id = id_area"
        +" inner join m_channel on m_channel.id = id_channel"
        +" where id_division = '"+id_division+"'";*/
        String sql = "select m_area.id id_area,m_channel.id id_channel,area_name,channel,full_name from m_area inner join m_channel on 1=1 order by m_area.id";
        cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader = database.Query(sql);
        while (reader.Read())
        {
            OfficeOpenXml.ExcelWorksheet ws = pck.Workbook.Worksheets.Add(reader["area_name"].ToString().TrimEnd() + "-"+ reader["channel"].ToString().TrimEnd());
            cotton.lib.MyDatabase database2 = new cotton.lib.MyDatabase();
            System.Data.SqlClient.SqlDataReader reader2 = database2.Query("select v_pricelist.*, RD_NEW,brnnm,catnm from v_pricelist inner join v_ers on sku_code = MATNR"
            +" where area ='"+reader["area_name"].ToString().TrimEnd()+"' and channel ='"+reader["channel"].ToString().TrimEnd()+"' and v_pricelist.id='"+id+"' and delisted_date is null order by category asc");
            int rowIndex = 8;

            ws.Cells["B1"].Value = "Daftar Harga PT UNILEVER INDONESIA, Tbk ";
            ws.Cells["B2"].Value = pricelistNumber;
            ws.Cells["C2"].Value = "Tanggal "+publishDate;
            ws.Cells["B3"].Value = reader["area_name"].ToString().TrimEnd() + "-"+ reader["channel"].ToString().TrimEnd();
            ws.Cells["B4"].Value = "Daftar Harga sewaktu-waktu dapat diubah tanpa pemberitahuan  terlebih dahulu";
            ws.Cells["B1:C3"].Style.Font.Bold = true;


            ws.Cells["B6"].Value = "Brand";
            ws.Cells["B6:B8"].Merge = true;
            ws.Cells["C6"].Value = "Category";
            ws.Cells["C6:C8"].Merge = true;
            ws.Cells["D6"].Value = "SKU";
            ws.Cells["D6:D8"].Merge = true;
            ws.Cells["E6"].Value = "Nama Produk";
            ws.Cells["E6:E8"].Merge = true;
            ws.Cells["F6"].Value = "Kemasan";
            ws.Cells["F6:F8"].Merge = true;
            ws.Cells["G6"].Value = "Barcode";
            ws.Cells["G6:G8"].Merge = true;
            ws.Cells["H6"].Value = "Jumlah";
            ws.Cells["H6:H8"].Merge = true;
            ws.Cells["I6"].Value = "Mulai";
            ws.Cells["i6:I8"].Merge = true;
            ws.Cells["J6"].Value = "Harga RSP PPN";
            ws.Cells["J6:J8"].Merge = true;
            ws.Cells["T6"].Value = "Keterangan";
            ws.Cells["T6:T8"].Merge = true;
            ws.Cells["U6"].Value = "Old / Regular SKU";
            ws.Cells["U6:U8"].Merge = true;

            ws.Cells["K6"].Value = "Harga Toko";
            ws.Cells["K6:P6"].Merge = true;
            ws.Cells["K7"].Value = "Tanpa PPN";
            ws.Cells["K7:L7"].Merge = true;
            ws.Cells["N7"].Value = "Dengan PPN";
            ws.Cells["N7:P7"].Merge = true;
            ws.Cells["K8"].Value = "Per Fib";
            ws.Cells["L8"].Value = "Per Lusin";
            ws.Cells["M8"].Value = "Per Satuan";
            ws.Cells["N8"].Value = "Per Fib";
            ws.Cells["O8"].Value = "Per Lusin";
            ws.Cells["P8"].Value = "Per Satuan";

            ws.Cells["Q6"].Value = "Harga Distributor";
            ws.Cells["Q6:S6"].Merge = true;
            ws.Cells["Q7"].Value = "Tanpa PPN";
            ws.Cells["Q7:Q8"].Merge = true;
            ws.Cells["R7"].Value = "Dengan PPN";
            ws.Cells["R7:R8"].Merge = true;
            ws.Cells["S7"].Value = "ppn bm";
            ws.Cells["S7:S8"].Merge = true;

            ws.Cells["B6:U8"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            ws.Cells["B6:U8"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightYellow);
            ws.Cells["B6:U8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            ws.Cells["B6:U8"].Style.Font.Bold = true;
            while (reader2.Read()) {
                rowIndex++;
                for (int i = 0; i < kolom_name.Count; i++) {
                    if (!kolom_name[i].Contains("none"))
                    {
                        if (kolom_name[i].Contains("ppn"))
                        {
                            ws.Cells[AlphabetIndex[i+1] + rowIndex].Value = Convert.ToDouble(reader2[kolom_name[i]].ToString()).ToString("#,##0");
                        }
                        else
                        {
                            ws.Cells[AlphabetIndex[i+1] + rowIndex].Value = reader2[kolom_name[i]].ToString();
                        }
                        if (migration == false && reader2["id"].ToString() == reader2["id_pricelist"].ToString())
                        {
                            ws.Cells[AlphabetIndex[i+1] + rowIndex].Style.Font.Bold = true;
                        }
                        if (migration == true && reader2["rd_new"].ToString() == "1")
                        {
                            ws.Cells[AlphabetIndex[i+1] + rowIndex].Style.Font.Bold = true;
                        }
                    }
                }
                //ws.Cells["T" + rowIndex].Value = migration;
                //ws.Cells["U" + rowIndex].Value = reader2["rd_new"].ToString();
            }
            ws.Cells["B6:U"+rowIndex].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ws.Cells["B6:U"+rowIndex].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ws.Cells["B6:U"+rowIndex].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ws.Cells["B6:U"+rowIndex].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ws.Cells["B6:U"+rowIndex].AutoFitColumns();
            reader2.Close();
            database2.Close();
        }
        reader.Close();
        database.Close();
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;  filename=Pricelist "+pricelistNumber+".xlsx");
        Response.BinaryWrite(pck.GetAsByteArray());
        Response.End();
    }
%>