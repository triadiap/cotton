﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
	String select_option = Request.Form["select_option"].ToString();
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
	if (select_option=="id_division"){
		System.Data.SqlClient.SqlDataReader reader = database.Query("select a.*, sales_org_code from m_division a inner join m_sales_org b on b.id=a.id_sales_org order by sales_org_code,division_code");
		while (reader.Read())
		{
			Response.Write("<option value='"+reader["id"].ToString()+"'>" + reader["sales_org_code"] + " - " + reader["division_desc"] + "</option>");
		}
		reader.Close();
	}else if(select_option=="id_area"){
		System.Data.SqlClient.SqlDataReader reader = database.Query("select * from m_area");
		while (reader.Read())
		{
			Response.Write("<option value='"+reader["id"].ToString()+"'>" + reader["area_name"] + "</option>");
		}
		reader.Close();
	}else if(select_option=="id_channel"){
		System.Data.SqlClient.SqlDataReader reader = database.Query("select * from m_channel order by id");
		while (reader.Read())
		{
			Response.Write("<div class='checkbox'><label><input type='checkbox' id='id_channel"+ reader["id"].ToString().Trim() +"'> " + reader["channel"].ToString().Trim() + "</label></div>");
		}
		reader.Close();
	}else if(select_option=="id_channel_arr"){
		System.Data.SqlClient.SqlDataReader reader = database.Query("select * from m_channel order by id");
		while (reader.Read())
		{
			Response.Write(reader["id"].ToString().Trim() + ",");
		}
		Response.Write("none");
		reader.Close();
	}
    database.Close();
%>