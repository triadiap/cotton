﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    String id = Request.Form["id"].ToString().Trim();
    String id_division = Request.Form["id_division"].ToString().Trim();
    String id_area = Request.Form["id_area"].ToString().Trim();
    String desc = Request.Form["desc"].ToString().Trim();
    String selected_channel = Request.Form["selected_channel"].ToString().Trim();
    String id_mapping_area = id;

    cotton.lib.MyDatabase db = new cotton.lib.MyDatabase();
    int count = 0;
    if (id == "0")
    {
        System.Data.SqlClient.SqlDataReader readerCek = db.Query("select TOP 1 id_division from m_mapping_area WHERE id_division = '" + id_division + "' AND id_area = '" + id_area + "' ");
        while (readerCek.Read())
        {
            count++;
        }
        readerCek.Close();
        db.Close();
        if (count == 0)
        {
            cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
            System.Data.SqlClient.SqlDataReader reader = database.Query("insert into m_mapping_area(id_division,id_area,[desc],created_by,created_date) values('" + id_division + "','" + id_area + "','" + desc + "','" + Session["id"].ToString() + "',getdate());SELECT SCOPE_IDENTITY() as id_mapping_area;");
            reader.Read();
            id_mapping_area = reader["id_mapping_area"].ToString().Trim();
            reader.Close();
            database.Close();
            cotton.lib.MyDatabase.FastExec("insert into h_activity([table],id_ref,[action],created_by,created_date,on_behalf) values ('m_mapping_area','0','Insert Data','" + Session["id"].ToString() + "',getdate(),'" + Session["id_on_behalf"].ToString() + "')");
            String sqlInsert = "";
            sqlInsert = "Delete from m_mapping_area_channel where id_mapping_area = '" + id_mapping_area + "';";
            string[] id_channels = selected_channel.Split(new[] { "," }, StringSplitOptions.None);
            foreach (var id_channel in id_channels)
            {
                if (id_channel != "")
                {
                    sqlInsert += "insert into m_mapping_area_channel(id_mapping_area, id_channel,created_by,created_date) values ('" + id_mapping_area + "', '" + id_channel + "','" + Session["id"].ToString() + "',getdate());";
                }
            }
            cotton.lib.MyDatabase.FastExec(sqlInsert);
        }
        else {
            Response.Write("Warning!<br>Data duplicate.");
        }
    }
    else {
        System.Data.SqlClient.SqlDataReader readerCek = db.Query("select TOP 1 id_division from m_mapping_area WHERE id_division = '" + id_division + "' AND id_area = '" + id_area + "' AND id != '" + id + "' ");
        while (readerCek.Read())
        {
            count++;
        }
        readerCek.Close();
        db.Close();
        if (count == 0)
        {
            cotton.lib.MyDatabase.FastExec("insert into h_activity([table],id_ref,[action],created_by,created_date,on_behalf) values ('m_mapping_area','" + id + "','Update Data','" + Session["id"].ToString() + "',getdate(),'" + Session["id_on_behalf"].ToString() + "')");
            cotton.lib.MyDatabase.FastExec("update m_mapping_area set id_division = '" + id_division + "', id_area= '" + id_area + "', [desc]= '" + desc + "' where id = '" + id + "';");

            String sqlInsert = "";
            sqlInsert = "Delete from m_mapping_area_channel where id_mapping_area = '" + id_mapping_area + "';";
            string[] id_channels = selected_channel.Split(new[] { "," }, StringSplitOptions.None);
            foreach (var id_channel in id_channels)
            {
                if (id_channel != "")
                {
                    sqlInsert += "insert into m_mapping_area_channel(id_mapping_area, id_channel,created_by,created_date) values ('" + id_mapping_area + "', '" + id_channel + "','" + Session["id"].ToString() + "',getdate());";
                }
            }
            cotton.lib.MyDatabase.FastExec(sqlInsert);
        }
        else {
            Response.Write("Warning!<br>Data duplicate.");
        }
    }
%>