﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
	String sqlQuery = "";
	int channelnum = 1;
    cotton.lib.MyDatabase databaseChannel = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader readerChannel = databaseChannel.Query("select * from m_channel order by id");
    sqlQuery = "select a.id, a.id_division, a.id_area, a.[desc], b.division_code, c.area_name, ";
	while(readerChannel.Read()){
		sqlQuery += "(select count(id) from m_mapping_area_channel where id_mapping_area=a.id and id_channel='" + readerChannel["id"].ToString() + "') as channel" + channelnum + ", ";
		channelnum=channelnum+1 ;
	}
    readerChannel.Close();
    databaseChannel.Close();
	
	sqlQuery+="from m_mapping_area a "
			+ "inner join m_division b on b.id=id_division " 
			+ "inner join m_area c on c.id=id_area ";
	
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query(sqlQuery.Replace(", from"," from"));
	int num = 1;
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + num.ToString().Trim() + "</td>");
        Response.Write("<td>" + reader["division_code"] + "</td>");
        Response.Write("<td>" + reader["area_name"] + "</td>");
        Response.Write("<td>" + reader["desc"] + "</td>");
        int channelnum2 = 1;
		while (channelnum2 < channelnum)
		{
			if(reader["channel"+channelnum2].ToString().Trim() == "0")
				Response.Write("<td></td>");	
			else
				Response.Write("<td style='text-align:center;'>X</td>");	
			channelnum2=channelnum2 + 1;
		}
		Response.Write("<td>");
        Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Edit' onclick='do_edit("+reader["id"].ToString()+")'><i class='fa fa-pencil'></i></a>");
        Response.Write("&nbsp;<a class='btn btn-xs btn-danger' data-toggle='tooltip' title='Delete' onclick='do_delete("+reader["id"].ToString()+",\""+reader["desc"]+"\")'><i class='fa fa-trash'></i></a>");
        Response.Write("</td>");
        Response.Write("</tr>");
		num=num+1;
    }
    reader.Close();
    database.Close();
%>