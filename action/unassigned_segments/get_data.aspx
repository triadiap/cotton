<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    String sqlQuery = "";
    
    sqlQuery = "select distinct seccd, secnm, brncd, brnnm, b.id_app_job_title,d.id_job_title, e.job_title from m_hierarchy a "
             + "cross join m_approval b "
             + "left join m_user_segments c on c.segments=a.seccd "
             + "left join m_user d on d.id=c.id_user and d.id_job_title=b.id_app_job_title "
             + "left join m_job_title e on e.id=b.id_app_job_title "
             + "where d.id_job_title is null "
             + "order by seccd";
    
    System.Data.SqlClient.SqlDataReader reader = database.Query(sqlQuery);
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["seccd"] + "</td>");
        Response.Write("<td>" + reader["secnm"] + "</td>");
        Response.Write("<td>" + reader["brncd"] + "</td>");
        Response.Write("<td>" + reader["brnnm"] + "</td>");
        Response.Write("<td>" + reader["job_title"] + "</td>");
        Response.Write("</tr>");
     }
    reader.Close();
    database.Close();
%>