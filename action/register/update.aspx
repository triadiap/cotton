﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 

    String email = Request.Form["email"].ToString();
    String noreg = Request.Form["nip"].ToString();
    String username = Request.Form["username"].ToString();
    String password = Request.Form["password"].ToString();
    String name = Request.Form["name"].ToString();
    String id_job_title = Request.Form["job_title"].ToString();
    String id_sales_org = Request.Form["sales_org"].ToString();
    String grade = Request.Form["grade"].ToString();
    String noreg_lm = Request.Form["noreg_lm"].ToString();
    String segments = Request.Form["segments"].ToString();

    Boolean isDuplicate = false;
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select m_user.*,job_title from m_user inner join m_job_title on m_job_title.id = id_job_title where username = '"+username+"' and status <> 6");
    if (reader.Read())
    {
        isDuplicate = true;
    }
    reader.Close();
    database.Close();
    if (isDuplicate) {
        Response.Write("{status:0,msg:'Email "+email+" already registered'}");
        Response.End();
    }
    cotton.lib.MyDatabase.FastExec("insert into m_user(noreg,email,username,password,name,id_job_title,grade,noreg_lm,status,id_sales_org)"
        + " values('"+noreg+"','"+email+"','"+username+"','"+password+"','"+name+"','"+id_job_title+"','"+grade+"','"+noreg_lm+"','1','"+id_sales_org+"')");
    var tmp = segments.Split(',');
    for (int i = 0; i < tmp.Length; i++)
    {
        cotton.lib.MyDatabase.FastExec("insert into m_user_segments(id_user,segments) values((select top 1 id from m_user where email = '"+email+"' order by id desc) ,'"+tmp[i]+"')");
    }
    cotton.lib.MyDatabase db = new cotton.lib.MyDatabase();
    reader = db.Query("select a.name lm_name,a.email lm_email,b.id_job_title,b.*,m_job_title.job_title,sales_org_code from m_user a"
    +" inner join m_user b on a.noreg = b.noreg_lm"
    +" inner join m_job_title on m_job_title.id = b.id_job_title"
    +" inner join m_sales_org on m_sales_org.id = b.id_sales_org"
    +" where b.email = '"+email+"' and b.status = '1'");
    String body = "<style>td{border:1px solid #000;}</style>";
    String to = "";
    if (reader.Read())
    {
        cotton.lib.MyDatabase db2 = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader2 = db2.Query("select DIVNM,CATNM,BRNNM,(select top 1 name from m_user inner join m_user_segments on m_user.id = id_user where segments = SECCD and id_job_title = '" + reader["id_job_title"].ToString() + "' and m_user.id != '" + reader["id"].ToString() + "' ) name from m_user_segments inner join m_hierarchy on SECCD = segments where id_user = '" + reader["id"].ToString() + "'");
        String divisi = "";
        String category = "";
        String brand = "";
        String duplicate = "";
        while (reader2.Read())
        {
            if (!divisi.Contains(reader2["DIVNM"].ToString()))
            {
                divisi += reader2["DIVNM"].ToString() + ",";
            }
            if (!category.Contains(reader2["CATNM"].ToString()))
            {
                category += reader2["CATNM"].ToString() + ",";
            }
            if (!brand.Contains(reader2["BRNNM"].ToString()))
            {
                brand += reader2["BRNNM"].ToString() + ",";
            }
            if (!duplicate.Contains(reader2["name"].ToString()))
            {
                duplicate += reader2["name"].ToString() + ",";
            }
        }
        if (divisi.Length > 0)
        {
            divisi = divisi.Substring(0, divisi.Length - 1);
        }
        if (category.Length > 0)
        {
            category = category.Substring(0, category.Length - 1);
        }
        if (brand.Length > 0)
        {
            brand = brand.Substring(0, brand.Length - 1);
        }
        if (duplicate.Length > 0)
        {
            duplicate = duplicate.Substring(0, duplicate.Length - 1);
        }
        reader2.Close();
        db2.Close();
        to = reader["lm_email"].ToString();
        body += "<style>.header{text-align:center;font-weight:bold;background:#EFEFEF}td{border:1px solid #000;}</style>Dear " + reader["lm_name"].ToString() + "<br/>";
        body += "<br/>";
        body += "Please find below account request that need your approval : <br/>";
        body += "<table cellpadding=3 cellspacing=0>";
        body += "<tr classs=\"header\"><td>#</td><td>Username</td><td>Job Tile</td><td>Sales Org</td><td>Division</td><td>Category</td><td>Brand</td><td>Duplicate With</td><td colspan='2'>Action</td></tr>";
        body += "<tr><td>1</td><td>" + reader["username"].ToString() + "</td><td>" + reader["job_title"].ToString() + "</td><td>" + reader["sales_org_code"].ToString() + "</td><td>" + divisi + "</td><td>" + category + "</td><td>" + brand + "</td><td>" + duplicate + "</td><td><a href='mailto:Cotton.Application@unilever.com?subject=FW : User with id : "+reader["id"].ToString()+"&body=User with username "+reader["name"].ToString()+" set as Approve'>Approve</a></td><td><a href = 'mailto:Cotton.Application@unilever.com?subject=FW : User with id : "+reader["id"].ToString()+"&body=User with username "+reader["name"].ToString()+" set as Reject'>Reject</a></td></tr>";
        body += "</table><br/>";
        body += "<br/>";
        body += "Please click link below for detail<br/>";
        body += "<br/>";
        body += "<a href='" + cotton.lib.EmailClass.getUrl() + "'>" + cotton.lib.EmailClass.getUrl() + "</a><br/>";
        body += "Disclaimer<br/>";
        body += "Please DO NOT REPLAY to this system generated mailing";
        cotton.lib.EmailClass.SendMail(to, "[Cotton] User Need Approval", body);
    }
    else {
        cotton.lib.MyDatabase.FastExec("update m_user set status = '3' where email = '" + email + "' and status='1'");
        cotton.lib.MyDatabase.FastExec("declare @id int select @id = (select top 1 id from m_user where email = '" + email + "') exec sp_send_app_user_to_admin @id");
    }
    reader.Close();
    db.Close();
    Response.Write("{status:1,msg:'Ok'}");
%>