﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 

    using (OfficeOpenXml.ExcelPackage pck = new OfficeOpenXml.ExcelPackage())
    {
        OfficeOpenXml.ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Calendar");
        String[] monthname = {"Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" };
        String year = Request.QueryString["year"];
        int maxIndex = 1;
        int curIndex = 1;
        cotton.lib.MyDatabase db = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader = db.Query("select convert(varchar,effective_date,103) effective_date,convert(varchar,publish_date,103) publish_date from m_callendar where YEAR(publish_date) = '"+year+"' and MONTH(publish_date) = '1';");
        int i = curIndex;
        ws.Cells["A"+i+":B"+i].Merge = true;
        ws.Cells["A"+i].Value = monthname[0];
        i++;
        ws.Cells["A"+i].Value = "Publish Date";
        ws.Cells["B"+i].Value = "Effective Date";
        ws.Cells["A"+(i-1)+":B"+i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        ws.Cells["A"+(i-1)+":B"+i].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
        ws.Cells["A"+(i-1)+":B"+i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        while (reader.Read())
        {
            i++;
            ws.Cells["A" + i].Value = reader["publish_date"].ToString();
            ws.Cells["C" + i].Value = reader["effective_date"].ToString();
        }
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        maxIndex = i;
        reader.Close();
        db.Close();

        db = new cotton.lib.MyDatabase();
        reader = db.Query("select convert(varchar,effective_date,103) effective_date,convert(varchar,publish_date,103) publish_date from m_callendar where YEAR(publish_date) = '"+year+"' and MONTH(publish_date) = '2';");
        i = curIndex;
        ws.Cells["D"+i+":E"+i].Merge = true;
        ws.Cells["D"+i].Value = monthname[1];
        i++;
        ws.Cells["D"+i].Value = "Publish Date";
        ws.Cells["E"+i].Value = "Effective Date";
        ws.Cells["D"+(i-1)+":E"+i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        ws.Cells["D"+(i-1)+":E"+i].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
        ws.Cells["D"+(i-1)+":E"+i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        while (reader.Read())
        {
            i++;
            ws.Cells["D" + i].Value = reader["publish_date"].ToString();
            ws.Cells["E" + i].Value = reader["effective_date"].ToString();
        }
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        if (maxIndex < i) {
            maxIndex = i;
        }
        curIndex = maxIndex+3;
        reader.Close();
        db.Close();


        db = new cotton.lib.MyDatabase();
        reader = db.Query("select convert(varchar,effective_date,103) effective_date,convert(varchar,publish_date,103) publish_date from m_callendar where YEAR(publish_date) = '"+year+"' and MONTH(publish_date) = '3';");
        i = curIndex;
        ws.Cells["A"+i+":B"+i].Merge = true;
        ws.Cells["A"+i].Value = monthname[2];
        i++;
        ws.Cells["A"+i].Value = "Publish Date";
        ws.Cells["B"+i].Value = "Effective Date";
        ws.Cells["A"+(i-1)+":B"+i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        ws.Cells["A"+(i-1)+":B"+i].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
        ws.Cells["A"+(i-1)+":B"+i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        while (reader.Read())
        {
            i++;
            ws.Cells["A" + i].Value = reader["publish_date"].ToString();
            ws.Cells["B" + i].Value = reader["effective_date"].ToString();
        }
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        maxIndex = i;
        reader.Close();
        db.Close();

        db = new cotton.lib.MyDatabase();
        reader = db.Query("select convert(varchar,effective_date,103) effective_date,convert(varchar,publish_date,103) publish_date from m_callendar where YEAR(publish_date) = '"+year+"' and MONTH(publish_date) = '4';");
        i = curIndex;
        ws.Cells["D"+i+":E"+i].Merge = true;
        ws.Cells["D"+i].Value = monthname[3];
        i++;
        ws.Cells["D"+i].Value = "Publish Date";
        ws.Cells["E"+i].Value = "Effective Date";
        ws.Cells["D"+(i-1)+":E"+i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        ws.Cells["D"+(i-1)+":E"+i].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
        ws.Cells["D"+(i-1)+":E"+i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        while (reader.Read())
        {
            i++;
            ws.Cells["D" + i].Value = reader["publish_date"].ToString();
            ws.Cells["E" + i].Value = reader["effective_date"].ToString();
        }
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        if (maxIndex < i) {
            maxIndex = i;
        }
        curIndex = maxIndex+3;
        reader.Close();
        db.Close();


        db = new cotton.lib.MyDatabase();
        reader = db.Query("select convert(varchar,effective_date,103) effective_date,convert(varchar,publish_date,103) publish_date from m_callendar where YEAR(publish_date) = '"+year+"' and MONTH(publish_date) = '5';");
        i = curIndex;
        ws.Cells["A"+i+":B"+i].Merge = true;
        ws.Cells["A"+i].Value = monthname[4];
        i++;
        ws.Cells["A"+i].Value = "Publish Date";
        ws.Cells["B"+i].Value = "Effective Date";
        ws.Cells["A"+(i-1)+":B"+i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        ws.Cells["A"+(i-1)+":B"+i].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
        ws.Cells["A"+(i-1)+":B"+i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        while (reader.Read())
        {
            i++;
            ws.Cells["A" + i].Value = reader["publish_date"].ToString();
            ws.Cells["B" + i].Value = reader["effective_date"].ToString();
        }
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        maxIndex = i;
        reader.Close();
        db.Close();

        db = new cotton.lib.MyDatabase();
        reader = db.Query("select convert(varchar,effective_date,103) effective_date,convert(varchar,publish_date,103) publish_date from m_callendar where YEAR(publish_date) = '"+year+"' and MONTH(publish_date) = '6';");
        i = curIndex;
        ws.Cells["D"+i+":E"+i].Merge = true;
        ws.Cells["D"+i].Value = monthname[5];
        i++;
        ws.Cells["D"+i].Value = "Publish Date";
        ws.Cells["E"+i].Value = "Effective Date";
        ws.Cells["D"+(i-1)+":E"+i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        ws.Cells["D"+(i-1)+":E"+i].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
        ws.Cells["D"+(i-1)+":E"+i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        while (reader.Read())
        {
            i++;
            ws.Cells["D" + i].Value = reader["publish_date"].ToString();
            ws.Cells["E" + i].Value = reader["effective_date"].ToString();
        }
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        if (maxIndex < i) {
            maxIndex = i;
        }
        curIndex = maxIndex+3;
        reader.Close();
        db.Close();


        db = new cotton.lib.MyDatabase();
        reader = db.Query("select convert(varchar,effective_date,103) effective_date,convert(varchar,publish_date,103) publish_date from m_callendar where YEAR(publish_date) = '"+year+"' and MONTH(publish_date) = '7';");
        i = curIndex;
        ws.Cells["A"+i+":B"+i].Merge = true;
        ws.Cells["A"+i].Value = monthname[6];
        i++;
        ws.Cells["A"+i].Value = "Publish Date";
        ws.Cells["B"+i].Value = "Effective Date";
        ws.Cells["A"+(i-1)+":B"+i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        ws.Cells["A"+(i-1)+":B"+i].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
        ws.Cells["A"+(i-1)+":B"+i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        while (reader.Read())
        {
            i++;
            ws.Cells["A" + i].Value = reader["publish_date"].ToString();
            ws.Cells["B" + i].Value = reader["effective_date"].ToString();
        }
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        maxIndex = i;
        reader.Close();
        db.Close();

        db = new cotton.lib.MyDatabase();
        reader = db.Query("select convert(varchar,effective_date,103) effective_date,convert(varchar,publish_date,103) publish_date from m_callendar where YEAR(publish_date) = '"+year+"' and MONTH(publish_date) = '8';");
        i = curIndex;
        ws.Cells["D"+i+":E"+i].Merge = true;
        ws.Cells["D"+i].Value = monthname[7];
        i++;
        ws.Cells["D"+i].Value = "Publish Date";
        ws.Cells["E"+i].Value = "Effective Date";
        ws.Cells["D"+(i-1)+":E"+i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        ws.Cells["D"+(i-1)+":E"+i].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
        ws.Cells["D"+(i-1)+":E"+i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        while (reader.Read())
        {
            i++;
            ws.Cells["D" + i].Value = reader["publish_date"].ToString();
            ws.Cells["B" + i].Value = reader["effective_date"].ToString();
        }
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        if (maxIndex < i) {
            maxIndex = i;
        }
        curIndex = maxIndex+3;
        reader.Close();
        db.Close();

        db = new cotton.lib.MyDatabase();
        reader = db.Query("select convert(varchar,effective_date,103) effective_date,convert(varchar,publish_date,103) publish_date from m_callendar where YEAR(publish_date) = '"+year+"' and MONTH(publish_date) = '9';");
        i = curIndex;
        ws.Cells["A"+i+":B"+i].Merge = true;
        ws.Cells["A"+i].Value = monthname[8];
        i++;
        ws.Cells["A"+i].Value = "Publish Date";
        ws.Cells["B"+i].Value = "Effective Date";
        ws.Cells["A"+(i-1)+":B"+i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        ws.Cells["A"+(i-1)+":B"+i].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
        ws.Cells["A"+(i-1)+":B"+i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        while (reader.Read())
        {
            i++;
            ws.Cells["A" + i].Value = reader["publish_date"].ToString();
            ws.Cells["B" + i].Value = reader["effective_date"].ToString();
        }
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        maxIndex = i;
        reader.Close();
        db.Close();

        db = new cotton.lib.MyDatabase();
        reader = db.Query("select convert(varchar,effective_date,103) effective_date,convert(varchar,publish_date,103) publish_date from m_callendar where YEAR(publish_date) = '"+year+"' and MONTH(publish_date) = '10';");
        i = curIndex;
        ws.Cells["D"+i+":E"+i].Merge = true;
        ws.Cells["D"+i].Value = monthname[9];
        i++;
        ws.Cells["D"+i].Value = "Publish Date";
        ws.Cells["E"+i].Value = "Effective Date";
        ws.Cells["D"+(i-1)+":E"+i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        ws.Cells["D"+(i-1)+":E"+i].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
        ws.Cells["D"+(i-1)+":E"+i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        while (reader.Read())
        {
            i++;
            ws.Cells["D" + i].Value = reader["publish_date"].ToString();
            ws.Cells["E" + i].Value = reader["effective_date"].ToString();
        }
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        if (maxIndex < i) {
            maxIndex = i;
        }
        curIndex = maxIndex+3;
        reader.Close();
        db.Close();


        db = new cotton.lib.MyDatabase();
        reader = db.Query("select convert(varchar,effective_date,103) effective_date,convert(varchar,publish_date,103) publish_date from m_callendar where YEAR(publish_date) = '"+year+"' and MONTH(publish_date) = '11';");
        i = curIndex;
        ws.Cells["A"+i+":B"+i].Merge = true;
        ws.Cells["A"+i].Value = monthname[10];
        i++;
        ws.Cells["A"+i].Value = "Publish Date";
        ws.Cells["B"+i].Value = "Effective Date";
        ws.Cells["A"+(i-1)+":B"+i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        ws.Cells["A"+(i-1)+":B"+i].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
        ws.Cells["A"+(i-1)+":B"+i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        while (reader.Read())
        {
            i++;
            ws.Cells["A" + i].Value = reader["publish_date"].ToString();
            ws.Cells["B" + i].Value = reader["effective_date"].ToString();
        }
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A"+curIndex+":B" + i].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        maxIndex = i;
        reader.Close();
        db.Close();

        db = new cotton.lib.MyDatabase();
        reader = db.Query("select convert(varchar,effective_date,103) effective_date,convert(varchar,publish_date,103) publish_date from m_callendar where YEAR(publish_date) = '"+year+"' and MONTH(publish_date) = '12';");
        i = curIndex;
        ws.Cells["D"+i+":E"+i].Merge = true;
        ws.Cells["D"+i].Value = monthname[11];
        i++;
        ws.Cells["D"+i].Value = "Publish Date";
        ws.Cells["E"+i].Value = "Effective Date";
        ws.Cells["D"+(i-1)+":E"+i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        ws.Cells["D"+(i-1)+":E"+i].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
        ws.Cells["D"+(i-1)+":E"+i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        while (reader.Read())
        {
            i++;
            ws.Cells["D" + i].Value = reader["publish_date"].ToString();
            ws.Cells["E" + i].Value = reader["effective_date"].ToString();
        }
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["D"+curIndex+":E" + i].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["A1:E" + i].AutoFitColumns();
        if (maxIndex < i) {
            maxIndex = i;
        }
        curIndex = maxIndex+3;
        reader.Close();
        db.Close();

        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;  filename=Cotton Calendar "+year+".xlsx");
        Response.BinaryWrite(pck.GetAsByteArray());
        Response.End();
    }
%>