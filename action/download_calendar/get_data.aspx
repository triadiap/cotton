﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select distinct YEAR(publish_date) year from m_callendar");
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["year"] + "</td>");
        Response.Write("<td style='text-align:center'>");
        Response.Write("<button class='btn btn-primary btn-xs' data-toggle='tooltip' title='Download' onclick='download(\""+reader["year"].ToString()+"\")'><i class='fa fa-download'></i></button>");
        Response.Write("</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>