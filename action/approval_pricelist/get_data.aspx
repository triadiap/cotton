﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select t_proposal_pricelist.id ,t_pricelist.pricelist_number,t_proposal_pricelist.proposal_number,t_proposal_pricelist.subject,convert(varchar,pricelist_date ,103) tanggal from t_proposal_pricelist inner join r_proposal_status on r_proposal_status.id = status inner join t_approval on t_approval.id_proposal = t_proposal_pricelist.id inner join t_pricelist_detail on t_pricelist_detail.id_proposal = t_proposal_pricelist.id inner join t_pricelist on t_pricelist.id = t_pricelist_detail.id_pricelist where id_approver in("+Session["id_on_behalf"].ToString()+") and t_approval.status_approve = '0' order by pricelist_date desc");
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["pricelist_number"] + "</td>");
        Response.Write("<td>" + reader["proposal_number"] + "</td>");
        Response.Write("<td>" + reader["subject"] + "</td>");
        Response.Write("<td>" + reader["tanggal"] + "</td>");
        Response.Write("<td style='text-align:center'>");
        Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Edit' onclick='do_edit(" + reader["id"].ToString() + ",101)'><i class='fa fa-pencil'></i></a>");
        Response.Write("</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>