﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select a.*, sales_org_code from m_division a inner join m_sales_org b on b.id=a.id_sales_org order by sales_org_code,division_code");
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["sales_org_code"] + "</td>");
        Response.Write("<td>" + reader["division_code"] + "</td>");
        Response.Write("<td>" + reader["division_desc"] + "</td>");
        Response.Write("<td style='text-align:center'>");
        Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Edit' onclick='do_edit("+reader["id"].ToString()+")'><i class='fa fa-pencil'></i></a>");
        Response.Write("&nbsp;<a class='btn btn-xs btn-danger' data-toggle='tooltip' title='Delete' onclick='do_delete("+reader["id"].ToString()+",\""+reader["division_desc"]+"\")'><i class='fa fa-trash'></i></a>");
        Response.Write("</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>