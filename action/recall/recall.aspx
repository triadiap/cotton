﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    String id = Request.Form["id"].ToString();
    cotton.lib.MyDatabase.FastExec("update t_proposal_detail set status = 6 where id_proposal = '"+id+"' ");
    cotton.lib.MyDatabase.FastExec("update t_proposal_pricelist set status = 6 where id='" + id + "'");
    cotton.lib.MyDatabase.FastExec("insert into h_activity([table],id_ref,[action],created_by,created_date,on_behalf) values ('t_proposal_pricelist','"+id+"','Recall Proposal','"+Session["id"].ToString()+"',getdate(),'" + Session["id_on_behalf"].ToString() + "')");
    cotton.lib.MyDatabase db = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader     reader = db.Query("select distinct t_proposal_pricelist.id,proposal_number,name,email from t_proposal_pricelist "
	+" inner join t_proposal_detail on t_proposal_pricelist.id = t_proposal_detail.id_proposal"
	+" inner join v_ers on matnr = sku"
	+" inner join m_user_segments on segments = seccd"
	+" inner join m_user on m_user.id_job_title in('4','15','14') and m_user.id = id_user"
	+" where t_proposal_pricelist.status = '6' and t_proposal_pricelist.id = "+id+" and m_user.status = '5'");
    String number = "";
    while (reader.Read()) {
        String body = "<style>.header{text-align:center;font-weight:bold;background:#EFEFEF}td{border:1px solid #000;}</style>";
        body += "Dear " + reader["name"].ToString()+"<br/>";
        body += "<br/>";
        body += "Proposal "+reader["proposal_number"]+" has been recalled by "+Session["name"].ToString()+"<br/>";
        number = reader["proposal_number"].ToString();
        body += "<table cellpadding='3' cellspacing='0'>";
        body += "<tr class='header'><td rowspan='2'>#</td><td rowspan='2'>SKU</td><td rowspan='2'>Description</td><td rowspan='2'>Remark</td><td colspan='3'>Price</td><td rowspan = '2'>Area</td><td rowspan = '2'>Channel</td><td rowspan = '2'>Note</td></tr>";
        body += "<tr class='header'><td rowspan='1'>RSP/pcs</td><td rowspan='1'>Toko Price/pcs</td><td rowspan='1'>Effective From</td></tr>";
        cotton.lib.MyDatabase db2 = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader2 = db2.Query("select proposed.sku,proposed.sku_name,m_proposal_remarks.remark,publised.rsp_price,publised.toko_price,convert(varchar,cal_publised.effective_date,6),publised.rsp_price-publised.toko_price,(publised.rsp_price-publised.toko_price)/publised.toko_price, proposed.rsp_price,proposed.toko_price,convert(varchar,cal_proposed.effective_date,6),proposed.rsp_price-proposed.toko_price,(proposed.rsp_price-proposed.toko_price)/proposed.toko_price,proposed.comment,dbo.get_channel(proposed.id) channel,area_name from t_proposal_detail proposed inner join m_proposal_remarks on m_proposal_remarks.id = proposed.remark inner join m_area on m_area.id = id_area inner join m_callendar cal_proposed on cal_proposed.id = proposed.id_callendar left join t_proposal_detail publised on proposed.sku = publised.sku and proposed.id_area = publised.id_area  and publised.status = '5' left join m_callendar cal_publised on cal_publised.id = publised.id_callendar where proposed.id_proposal = '"+reader["id"].ToString()+"'");
        int i = 0;
        while (reader2.Read()) {
            i++;
            body += "<tr>";
            body += "<td>"+i+"</td>";
            body += "<td>"+reader2[0].ToString()+"</td>";
            body += "<td>"+reader2[1].ToString()+"</td>";
            body += "<td>" + reader2[2].ToString() + "</td>";
            body += "<td>"+reader2[8].ToString()+"</td>";
            body += "<td>"+reader2[9].ToString()+"</td>";
            body += "<td>"+reader2[10].ToString()+"</td>";
            body += "<td>"+reader2["area_name"].ToString()+"</td>";
            body += "<td>"+reader2["channel"].ToString()+"</td>";
            body += "<td>"+reader2[13].ToString()+"</td>";
            body += "</tr>";
        }
        reader2.Close();
        db2.Close();
        body += "</table><br/>";
        body += "<br/>";
        body += "Please click link below for detail<br/>";
        body += "<br/>";
        body += "<a href='"+cotton.lib.EmailClass.getUrl()+"'>" + cotton.lib.EmailClass.getUrl() + "</a><br/>";
        body += "Disclaimer<br/>";
        body += "Please DO NOT REPLAY to this system generated mailing";
        cotton.lib.EmailClass.SendMail(reader["email"].ToString(), "[Cotton] Proposal "+number+" Has Been Recalled", body);
    }
    reader.Close();
    db.Close();

    db = new cotton.lib.MyDatabase();
    reader = db.Query("select t_proposal_pricelist.id,proposal_number,name,email from t_proposal_pricelist inner join t_approval on t_proposal_pricelist.id = id_proposal inner join m_user on m_user.id = id_approver where (id_proposal = '"+id+"' or proposal_number = '"+number+"')");
    while (reader.Read()) {
        String body = "<style>.header{text-align:center;font-weight:bold;background:#EFEFEF}td{border:1px solid #000;}</style>";
        body += "Dear " + reader["name"].ToString()+"<br/>";
        body += "<br/>";
        body += "Proposal "+reader["proposal_number"]+" has been recalled by "+Session["name"].ToString()+"<br/>";
        number = reader["proposal_number"].ToString();
        body += "<table cellpadding='3' cellspacing='0'>";
        body += "<tr class='header'><td rowspan='2'>#</td><td rowspan='2'>SKU</td><td rowspan='2'>Description</td><td rowspan='2'>Remark</td><td colspan='3'>Price</td><td rowspan = '2'>Area</td><td rowspan = '2'>Channel</td><td rowspan = '2'>Note</td></tr>";
        body += "<tr class='header'><td rowspan='1'>RSP/pcs</td><td rowspan='1'>Toko Price/pcs</td><td rowspan='1'>Effective From</td></tr>";
        cotton.lib.MyDatabase db2 = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader2 = db2.Query("select proposed.sku,proposed.sku_name,m_proposal_remarks.remark,publised.rsp_price,publised.toko_price,convert(varchar,cal_publised.effective_date,6),publised.rsp_price-publised.toko_price,(publised.rsp_price-publised.toko_price)/publised.toko_price, proposed.rsp_price,proposed.toko_price,convert(varchar,cal_proposed.effective_date,6),proposed.rsp_price-proposed.toko_price,(proposed.rsp_price-proposed.toko_price)/proposed.toko_price,proposed.comment,dbo.get_channel(proposed.id) channel,area_name from t_proposal_detail proposed inner join m_proposal_remarks on m_proposal_remarks.id = proposed.remark inner join m_area on m_area.id = id_area inner join m_callendar cal_proposed on cal_proposed.id = proposed.id_callendar left join t_proposal_detail publised on proposed.sku = publised.sku and proposed.id_area = publised.id_area  and publised.status = '5' left join m_callendar cal_publised on cal_publised.id = publised.id_callendar where proposed.id_proposal = '"+reader["id"].ToString()+"'");
        int i = 0;
        while (reader2.Read()) {
            i++;
            body += "<tr>";
            body += "<td>"+i+"</td>";
            body += "<td>"+reader2[0].ToString()+"</td>";
            body += "<td>"+reader2[1].ToString()+"</td>";
            body += "<td>" + reader2[2].ToString() + "</td>";
            body += "<td>"+reader2[8].ToString()+"</td>";
            body += "<td>"+reader2[9].ToString()+"</td>";
            body += "<td>"+reader2[10].ToString()+"</td>";
            body += "<td>"+reader2["area_name"].ToString()+"</td>";
            body += "<td>"+reader2["channel"].ToString()+"</td>";
            body += "<td>"+reader2[13].ToString()+"</td>";
            body += "</tr>";
        }
        reader2.Close();
        db2.Close();
        body += "</table><br/>";
        body += "<br/>";
        body += "Please click link below for detail<br/>";
        body += "<br/>";
        body += "<a href='"+cotton.lib.EmailClass.getUrl()+"'>" + cotton.lib.EmailClass.getUrl() + "</a><br/>";
        body += "Disclaimer<br/>";
        body += "Please DO NOT REPLAY to this system generated mailing";
        cotton.lib.EmailClass.SendMail(reader["email"].ToString(), "[Cotton] Proposal "+number+" Has Been Recalled", body);
    }
    reader.Close();
    db.Close();
%>