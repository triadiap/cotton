﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
	string sqlQuery = "";
	sqlQuery = "select A.id, id_division, division_desc, id_area, area_name, harga_batam "
		+ "from m_mapping_harga_batam A "
		+ "left join m_division B on B.id=A.id_division "
		+ "left join m_area C on C.id=A.id_area";
    System.Data.SqlClient.SqlDataReader reader = database.Query(sqlQuery);
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["division_desc"] + "</td>");
        Response.Write("<td>" + reader["area_name"] + "</td>");
        Response.Write("<td>" + reader["harga_batam"] + " %</td>");
        Response.Write("<td style='text-align:center'>");
        Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Edit' onclick='do_edit("+reader["id"].ToString()+")'><i class='fa fa-pencil'></i></a>");
        Response.Write("&nbsp;<a class='btn btn-xs btn-danger' data-toggle='tooltip' title='Delete' onclick='do_delete("+reader["id"].ToString()+",\""+reader["division_desc"]+" - "+reader["area_name"]+"\")'><i class='fa fa-trash'></i></a>");
        Response.Write("</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>