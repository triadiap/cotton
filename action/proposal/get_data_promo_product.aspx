﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    String sql = "";
    if (Request.Form["id"] == "0") {
        sql = "select u.name,a.*,b.remark proposal_remark,area_name,convert(varchar,effective_date,103) effective_date,subject,'Draft' sp_status,'' r_toko_price,'' r_rsp_price,proposal_number  from tmp_proposal_detail a"
        +" inner join m_proposal_remarks b on b.id = a.remark"
        +" inner join m_area c on c.id = a.id_area"
        +" inner join m_callendar d on d.id = a.id_callendar"
        +" inner join m_user u on u.id = a.created_by"
        +" left join t_proposal_pricelist e on e.id = a.id_proposal"
        +" where a.created_by in ("+Session["id"].ToString()+") and a.status is null and is_new <> 2 order by sku,a.id";
    } else {
        sql = "select u.name,a.*,b.remark proposal_remark,area_name,convert(varchar,effective_date,103) effective_date,subject,isnull(sp_status,'Draft') sp_status,g.toko_price r_toko_price,g.rsp_price r_rsp_price,proposal_number  from tmp_proposal_detail a"
        +" inner join m_proposal_remarks b on b.id = a.remark"
        +" inner join m_area c on c.id = a.id_area"
        +" inner join m_callendar d on d.id = a.id_callendar"
        +" inner join t_proposal_pricelist e on e.id = a.id_proposal"
        +" inner join m_user u on u.id = a.created_by"
        +" left join r_proposal_status f on f.id = a.status"
        +" left join t_new_proposed_price g on g.id_proposal_detail = a.id"
        +" where id_proposal = '"+Request.Form["id"].ToString()+"' and is_new <> 2 order by sku,a.id";
    }
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query(sql);
    String data = "[";
    while (reader.Read())
    {
        data += "{";
        data += "'id':'" + reader["id"].ToString().TrimEnd() + "',";
        if (reader["id_area"].ToString().TrimEnd() == "8")
        {
            data += "'sp_status':'',";
        }
        else
        {
            data += "'sp_status':'" + reader["sp_status"].ToString().TrimEnd() + "',";
        }
        data += "'creator':'" + reader["name"].ToString().TrimEnd().Replace("'","\\\'") + "',";
        data += "'subject':'" + reader["subject"].ToString().TrimEnd().Replace("'","\\\'") + "',";
        data += "'proposal_number':'" + reader["proposal_number"].ToString().TrimEnd() + "',";
        data += "'sku':'" + reader["sku"].ToString().TrimEnd() + "',";
        data += "'dozen':'" + reader["dozen"].ToString().TrimEnd() + "',";
        data += "'configuration':'" + reader["configuration"].ToString().TrimEnd() + "',";
        data += "'ipack':'" + reader["ipack"].ToString().TrimEnd() + "',";
        data += "'id_callendar':'" + reader["id_callendar"].ToString().TrimEnd() + "',";
        data += "'id_area':'" + reader["id_area"].ToString().TrimEnd() + "',";
        data += "'remark':'" + reader["remark"].ToString().TrimEnd() + "',";
        data += "'comment':'" + reader["comment"].ToString().TrimEnd().Replace("'","\\\'") + "',";
        if (reader["r_toko_price"].ToString() != "")
        {
            data += "'toko_price':'" + reader["r_toko_price"].ToString().TrimEnd() + "',";
        }
        else
        {
            data += "'toko_price':'" + reader["toko_price"].ToString().TrimEnd() + "',";
        }
        if (reader["r_rsp_price"].ToString() != "")
        {
            data += "'rsp_price':'" + reader["r_rsp_price"].ToString().TrimEnd() + "',";
        }
        else
        {
            data += "'rsp_price':'" + reader["rsp_price"].ToString().TrimEnd() + "',";
        }
        data += "'sku_name':'" + reader["sku_name"].ToString().TrimEnd().Replace("'","\\\'") + "',";
        data += "'old_sku':'" + reader["old_sku"].ToString().TrimEnd() + "',";
        data += "'cpgcd':'" + reader["cpgcd"].ToString().TrimEnd() + "',";
        data += "'proposal_remark':'" + reader["proposal_remark"].ToString().TrimEnd() + "',";
        data += "'area_name':'" + reader["area_name"].ToString().TrimEnd() + "',";
        data += "'mdg_approval_file':'" + reader["mdg_approval_file"].ToString().TrimEnd() + "',";
        data += "'mdg_approval_date':'" + reader["mdg_approval_date"].ToString().TrimEnd() + "',";
        data += "'mdg_approval':'" + reader["mdg_approval"].ToString().TrimEnd() + "',";
        data += "'effective_date':'" + reader["effective_date"].ToString().TrimEnd() + "',";
        cotton.lib.MyDatabase database2 = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader2 = database2.Query("select id_channel,channel from tmp_proposal_channel inner join m_channel on m_channel.id = id_channel where id_proposal_detail = '"+reader["id"].ToString().TrimEnd()+"'");
        System.Collections.Generic.List<String> channel = new System.Collections.Generic.List<String>();
        System.Collections.Generic.List<String> channel_id = new System.Collections.Generic.List<String>();
        while (reader2.Read()) {
            channel.Add(reader2["channel"].ToString());
            channel_id.Add(reader2["id_channel"].ToString());
        }
        reader2.Close();
        database2.Close();
        data += "'channel':'"+String.Join("<br>",channel)+"',";
        data += "'channel_id':'"+String.Join(",",channel_id)+"'";
        data += "},";
    }
    if (data.Length > 1) {
        data = data.Substring(0, data.Length - 1);
    }
    data += "]";
    Response.Write(data);
    reader.Close();
    database.Close();
%>