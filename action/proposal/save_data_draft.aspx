﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    String sku = Request.Form["sku"].ToString();
    String id = Request.Form["id"].ToString();
    String id_proposal = Request.Form["id_proposal"].ToString();
    String sku_name = Request.Form["sku_name"].ToString();
    sku_name = sku_name.Replace("'","''");
    String dozen = Request.Form["dozen"].ToString();
    String configuration = Request.Form["configuration"].ToString();
    String ipack = Request.Form["ipack"].ToString();
    String id_callendar = Request.Form["id_callendar"].ToString();
    String remark = Request.Form["remark"].ToString();
    remark = remark.Replace("'","''");
    String comment = Request.Form["comment"].ToString();
    comment = comment.Replace("'","''");
    String toko_price = Request.Form["toko_price"].ToString();
    String rsp_price = Request.Form["rsp_price"].ToString();
    String channel = Request.Form["channel"].ToString();
    String channel_id = Request.Form["channel"].ToString();
    String old_sku = Request.Form["old_sku"].ToString();
    String cpgcd = Request.Form["cpgcd"].ToString();
    String id_area = Request.Form["id_area"].ToString();
    String mdg_approval_file = Request.Form["mdg_approval_file"].ToString();
    String mdg_approval_date = Request.Form["mdg_approval_date"].ToString();
    String mdg_approval = Request.Form["mdg_approval"].ToString();
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("SELECT * FROM [dbo].[tmp_proposal_detail] where sku = '"+sku+"' and id_area = '"+id_area+"' and status is null and id <> '"+id+"' and created_by = '"+Session["id"].ToString()+"'");
    String err = "";
    while (reader.Read())
    {
        err = "Proposal with SKU and area already added";
    }
    reader.Close();
    database.Close();
    database = new cotton.lib.MyDatabase();
    reader = database.Query("SELECT * FROM [dbo].[t_proposal_detail] where sku = '"+sku+"' and id_area = '"+id_area+"' and status in (2,3,4) and id <> '"+id+"'");
    while (reader.Read())
    {
        err = "Proposal with SKU and area already submited";
    }
    reader.Close();
    database.Close();
    int mapped_user_approval = 0;
    database = new cotton.lib.MyDatabase();
    reader = database.Query("select count(distinct id_app_job_title) total from m_approval inner join m_user on m_user.id_job_title = id_app_job_title inner join m_user_segments on m_user_segments.id_user = m_user.id where segments in (select SECCD from v_ers where MATNR = '"+sku+"') and m_user.status = '5'");
    while (reader.Read())
    {
        mapped_user_approval = Convert.ToInt32(reader["total"].ToString());
    }
    reader.Close();
    database.Close();
    int mapped_approval = 0;
    database = new cotton.lib.MyDatabase();
    reader = database.Query("select count(id_app_job_title) total from m_approval where id_sales_org = '"+Session["id_sales_org_on_behalf"]+"';");
    while (reader.Read())
    {
        mapped_approval = Convert.ToInt32(reader["total"].ToString());
    }
    reader.Close();
    database.Close();

    int max_mapped_approval = 0;
    database = new cotton.lib.MyDatabase();
    reader = database.Query("select count(distinct app_level) total from m_approval where id_sales_org = '"+Session["id_sales_org_on_behalf"]+"';");
    while (reader.Read())
    {
        max_mapped_approval = Convert.ToInt32(reader["total"].ToString());
    }
    reader.Close();
    database.Close();

    int max_approval = 0;
    database = new cotton.lib.MyDatabase();
    reader = database.Query("select max_approval from m_sales_org where id = '"+Session["id_sales_org_on_behalf"]+"';");
    while (reader.Read())
    {
        max_approval = Convert.ToInt32(reader["max_approval"].ToString());
    }
    reader.Close();
    database.Close();
    if (max_mapped_approval < max_approval || mapped_user_approval < mapped_approval) {
        err = "You can not add this product, Approval level is incomplete please contact admin";
    }
    if (err != "") {
        Response.Write("{'status':'nok','type':'add','msg':'" + err + "','log':'"+max_mapped_approval+" "+max_approval+" "+mapped_user_approval+" "+mapped_approval+"'}");
        Response.End();
    }
    if (id == "0")
    {
        bool isbatam = false;
        String newId = System.Diagnostics.Stopwatch.GetTimestamp()+"";
        cotton.lib.MyDatabase.FastExec("insert into tmp_proposal_detail(id_proposal,id,sku,dozen,configuration,ipack,id_callendar,remark,comment,toko_price,rsp_price,old_sku,mdg_approval_file,mdg_approval_date,mdg_approval,created_by,id_area,sku_name,cpgcd,is_new,created_date)"
            + " values('"+id_proposal+"','"+newId+"', '" + sku + "','" + dozen + "','" + configuration + "','" + ipack + "','" + id_callendar + "','" + remark + "','" + comment + "','" + toko_price + "','" + rsp_price + "','" + old_sku + "','" + mdg_approval_file + "','" + mdg_approval_date + "','" + mdg_approval + "','" + Session["id"].ToString() + "','" + id_area + "','" + sku_name + "','" + cpgcd + "',1,getdate())");
        var tmp = channel_id.Split(',');
        cotton.lib.MyDatabase.FastExec("delete from tmp_proposal_channel where id_proposal_detail = '" + (Session["id"].ToString() + sku + ""+id_area) + "'");
        for (int i = 0; i < tmp.Length; i++)
        {
            cotton.lib.MyDatabase.FastExec("insert into tmp_proposal_channel(id_proposal_detail,id_channel) values ('" + newId + "','" + tmp[i] + "')");
        }
        String newIdBatam = System.Diagnostics.Stopwatch.GetTimestamp()+"";
        cotton.lib.MyDatabase.FastExec("insert into tmp_proposal_detail(id_proposal,id,sku,dozen,configuration,ipack,id_callendar,remark,comment,toko_price,rsp_price,old_sku,mdg_approval_file,mdg_approval_date,mdg_approval,created_by,id_area,sku_name,cpgcd,is_new,created_date)"
            + " select '"+id_proposal+"','"+newIdBatam+"', '" + sku + "','" + dozen + "','" + configuration + "','" + ipack + "','" + id_callendar + "','" + remark + "','" + comment + "',(" + toko_price + "*harga_batam)/100, " +rsp_price + ",'" + old_sku + "','" + mdg_approval_file + "','" + mdg_approval_date + "','" + mdg_approval + "','" + Session["id"].ToString() + "','8','" + sku_name + "','" + cpgcd + "',1,getdate()  from tmp_proposal_detail inner join v_ers on MATNR = sku inner join m_division on m_division.division_code = v_ers.DIVCD inner JOIN m_mapping_harga_batam on m_mapping_harga_batam.id_area = tmp_proposal_detail.id_area and id_division = m_division.id where tmp_proposal_detail.id = '"+newId+"'");
        database = new cotton.lib.MyDatabase();
        reader = database.Query("select top 1 id,id_area from tmp_proposal_detail where created_by = '" + Session["id"].ToString() + "' and id = '"+newId+"'");
        while (reader.Read())
        {
            isbatam = true;
        }
        reader.Close();
        database.Close();
        if (isbatam)
        {
            cotton.lib.MyDatabase dbBatam = new cotton.lib.MyDatabase();
            System.Data.SqlClient.SqlDataReader readerBatam = dbBatam.Query("select m_mapping_area_channel.id_channel from v_ers inner join m_division on m_division.division_code = v_ers.DIVCD"
			+" inner join m_mapping_area on m_mapping_area.id_division = m_division.id and m_mapping_area.id_area = '8'"
			+" inner join m_mapping_area_channel on m_mapping_area_channel.id_mapping_area = m_mapping_area.id"
			+" where v_ers.MATNR = '"+sku+"'");
            while (readerBatam.Read())
            {
                cotton.lib.MyDatabase.FastExec("insert into tmp_proposal_channel(id_proposal_detail,id_channel) values ('" + newIdBatam+ "','" + readerBatam["id_channel"].ToString() + "')");
            }
            readerBatam.Close();
            dbBatam.Close();
        }
        Response.Write("{'status':'ok','type':'edit','msg':'" + id + "'}");

    }
    else {
    	//Delete batam
    	bool isOldDeleted = true;
    	database = new cotton.lib.MyDatabase();
        reader = database.Query("select * from tmp_proposal_detail where sku = '"+sku+"' and id_area = '8' and (status in(1,6,7) or status is null)");
        while (reader.Read()) {
            cotton.lib.MyDatabase.FastExec("delete from tmp_proposal_channel where id_proposal_detail = '"+reader["id"].ToString()+"'");
        }
        reader.Close();
        database.Close();
        cotton.lib.MyDatabase.FastExec("update tmp_proposal_detail set is_new = '2' where sku = '"+sku+"' and id_area = '8' and (status in(1,6,7) or status is null)");
        database = new cotton.lib.MyDatabase();
        reader = database.Query("select * from tmp_proposal_detail where sku = '"+sku+"' and id_area = '8' and is_new <> '2'");
        while (reader.Read()) {
        	isOldDeleted = false;
        }
        reader.Close();
        database.Close();
        //cotton.lib.MyDatabase.FastExec("insert into t_proposal_detail(sku,dozen,configuration,ipack,id_callendar,remark,comment,toko_price,rsp_price,old_sku,mdg_approval_file,mdg_approval_date,mdg_approval,created_by,id_area,sku_name,old_sku_name)"
        //    + " values('" + sku + "','" + dozen + "','" + configuration + "','" + ipack + "','" + id_callendar + "','" + remark + "','" + comment + "','" + toko_price + "','" + rsp_price + "','" + old_sku + "','" + mdg_approval_file + "','" + mdg_approval_date + "','" + mdg_approval + "','" + Session["id"].ToString() + "','" + id_area + "','" + sku_name + "','" + old_sku_name + "')");
        database = new cotton.lib.MyDatabase();
        reader = database.Query("select effective_date from tmp_proposal_detail inner join m_callendar on m_callendar.id = id_callendar where tmp_proposal_detail.id = '"+id+"' and effective_date > (select top 1 effective_date from m_callendar where id = '"+id_callendar+"')");
        cotton.lib.MyDatabase.FastExec("delete from tmp_change_effective_date where id_proposal_detail = '" + id + "'");
        cotton.lib.MyDatabase.FastExec("delete tmp_change_effective_date from tmp_change_effective_date inner join tmp_proposal_detail on tmp_proposal_detail.id = tmp_change_effective_date.id_proposal_detail inner join v_ers on MATNR = sku inner join m_division on m_division.division_code = v_ers.DIVCD inner JOIN m_mapping_harga_batam on m_mapping_harga_batam.id_area = '"+id_area+"' and id_division = m_division.id where tmp_proposal_detail.id <> '"+id+"' and sku  = '"+sku+"' and tmp_proposal_detail.id_area = '8'");
        while (reader.Read()) {
            cotton.lib.MyDatabase.FastExec("insert into tmp_change_effective_date(id_proposal,id_proposal_detail,effective_date) select id_proposal,tmp_proposal_detail.id,effective_date from tmp_proposal_detail inner join m_callendar on m_callendar.id = id_callendar  where tmp_proposal_detail.id = '"+id+"' and tmp_proposal_detail.status in(2,3,4,5)");
            cotton.lib.MyDatabase.FastExec("insert into tmp_change_effective_date(id_proposal,id_proposal_detail,effective_date) select id_proposal,tmp_proposal_detail.id,effective_date from tmp_proposal_detail inner join v_ers on MATNR = sku inner join m_division on m_division.division_code = v_ers.DIVCD inner JOIN m_mapping_harga_batam on m_mapping_harga_batam.id_area = '"+id_area+"' and id_division = m_division.id inner join m_callendar on m_callendar.id = id_callendar  where tmp_proposal_detail.id <> '"+id+"' and sku  = '"+sku+"' and tmp_proposal_detail.id_area = '8' and tmp_proposal_detail.status in(2,3,4,5)");
            cotton.lib.MyDatabase.FastExec("update tmp_proposal_detail set status = '7' where id = '"+id+"' and status in(2,3,4,5)");
        }
        reader.Close();
        database.Close();
        database = new cotton.lib.MyDatabase();
        reader = database.Query("select effective_date from tmp_proposal_detail inner join m_callendar on m_callendar.id = id_callendar where tmp_proposal_detail.id = '"+id+"' and effective_date < (select top 1 effective_date from m_callendar where id = '"+id_callendar+"')");
        while (reader.Read()) {
            cotton.lib.MyDatabase.FastExec("insert into tmp_change_effective_date(id_proposal,id_proposal_detail,effective_date) select id_proposal,tmp_proposal_detail.id,effective_date from tmp_proposal_detail inner join m_callendar on m_callendar.id = id_callendar  where tmp_proposal_detail.id = '"+id+"' and tmp_proposal_detail.status in(2,3,4,5)");
            cotton.lib.MyDatabase.FastExec("insert into tmp_change_effective_date(id_proposal,id_proposal_detail,effective_date) select id_proposal,tmp_proposal_detail.id,effective_date from tmp_proposal_detail inner join v_ers on MATNR = sku inner join m_division on m_division.division_code = v_ers.DIVCD inner JOIN m_mapping_harga_batam on m_mapping_harga_batam.id_area = '"+id_area+"' and id_division = m_division.id inner join m_callendar on m_callendar.id = id_callendar  where tmp_proposal_detail.id <> '"+id+"' and sku  = '"+sku+"' and tmp_proposal_detail.id_area = '8' and tmp_proposal_detail.status in(2,3,4,5)");
        }
        reader.Close();
        database.Close();
        cotton.lib.MyDatabase.FastExec("update tmp_proposal_detail set"
            + " sku = '"+sku+"',"
            + " dozen = '"+dozen+"',"
            + " configuration = '"+configuration+"',"
            + " ipack = '"+ipack+"',"
            + " id_callendar = '"+id_callendar+"',"
            + " remark = '"+remark+"',"
            + " comment = '"+comment+"',"
            + " toko_price = '"+toko_price+"',"
            + " rsp_price = '"+rsp_price+"',"
            + " old_sku = '"+old_sku+"',"
            + " mdg_approval_file = '"+mdg_approval_file+"',"
            + " mdg_approval_date = '"+mdg_approval_date+"',"
            + " mdg_approval = '"+mdg_approval+"',"
            + " created_by = '"+Session["id"].ToString()+"',"
            + " id_area = '"+id_area+"',"
            + " sku_name = '"+sku_name+"',"
            + " cpgcd = '"+cpgcd+"'"
            + " where id = '" + id + "' and (status is null or status <> 5 or id in(select id_proposal_detail from tmp_change_effective_date where id_proposal_detail = '"+id+"'))");
        cotton.lib.MyDatabase.FastExec("delete from t_new_proposed_price where id_proposal_detail = '" + id + "'");
        cotton.lib.MyDatabase.FastExec("insert into t_new_proposed_price(id_proposal_detail,toko_price,rsp_price,status) select id,'"+toko_price+"','"+rsp_price+"',1 from tmp_proposal_detail where tmp_proposal_detail.id = '" + id + "' and status = '5'");
        cotton.lib.MyDatabase.FastExec("update tmp_proposal_detail set"
            + " sku = '"+sku+"',"
            + " dozen = '"+dozen+"',"
            + " configuration = '"+configuration+"',"
            + " ipack = '"+ipack+"',"
            + " id_callendar = '"+id_callendar+"',"
            + " remark = '"+remark+"',"
            + " comment = '"+comment+"',"
            + " toko_price = ("+toko_price+"*harga_batam)/100,"
            + " rsp_price = "+rsp_price+","
            + " old_sku = '"+old_sku+"',"
            + " mdg_approval_file = '"+mdg_approval_file+"',"
            + " mdg_approval_date = '"+mdg_approval_date+"',"
            + " mdg_approval = '"+mdg_approval+"',"
            + " created_by = '"+Session["id"].ToString()+"',"
            + " tmp_proposal_detail.id_area = '8',"
            + " sku_name = '"+sku_name+"',"
            + " cpgcd = '"+cpgcd+"'"
            + " from tmp_proposal_detail inner join v_ers on MATNR = sku inner join m_division on m_division.division_code = v_ers.DIVCD inner JOIN m_mapping_harga_batam on m_mapping_harga_batam.id_area = '"+id_area+"' and id_division = m_division.id where tmp_proposal_detail.id <> '"+id+"' and sku  = '"+sku+"' and (status is null or status <> 5 or tmp_proposal_detail.id in(select id_proposal_detail from tmp_change_effective_date where id_proposal_detail = tmp_proposal_detail.id)) and tmp_proposal_detail.id_area = '8'");        
        cotton.lib.MyDatabase.FastExec("delete from t_new_proposed_price from t_new_proposed_price inner join tmp_proposal_detail on tmp_proposal_detail.id = t_new_proposed_price.id_proposal_detail  inner join v_ers on MATNR = sku inner join m_division on m_division.division_code = v_ers.DIVCD inner JOIN m_mapping_harga_batam on m_mapping_harga_batam.id_area = '"+id_area+"' and id_division = m_division.id where tmp_proposal_detail.id <> '"+id+"' and sku  = '"+sku+"' and tmp_proposal_detail.status = 5 and tmp_proposal_detail.id_area = '8'");
        cotton.lib.MyDatabase.FastExec("insert into t_new_proposed_price(id_proposal_detail,toko_price,rsp_price,status) select tmp_proposal_detail.id,("+toko_price+"*harga_batam)/100,"+rsp_price+",1 from tmp_proposal_detail inner join v_ers on MATNR = sku inner join m_division on m_division.division_code = v_ers.DIVCD inner JOIN m_mapping_harga_batam on m_mapping_harga_batam.id_area = '"+id_area+"' and id_division = m_division.id where tmp_proposal_detail.id <> '"+id+"' and sku  = '"+sku+"' and status = 5 and tmp_proposal_detail.id_area = '8'");
        var tmp = channel_id.Split(',');
        cotton.lib.MyDatabase.FastExec("delete from tmp_proposal_channel where id_proposal_detail = '" + id + "'");
        for (int i = 0; i < tmp.Length; i++)
        {
            cotton.lib.MyDatabase.FastExec("insert into tmp_proposal_channel(id_proposal_detail,id_channel) values ('" + id + "','" + tmp[i] + "')");
        }
        cotton.lib.MyDatabase db2 = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader2 = db2.Query("select tmp_proposal_detail.id from tmp_proposal_detail inner join v_ers on MATNR = sku inner join m_division on m_division.division_code = v_ers.DIVCD inner JOIN m_mapping_harga_batam on m_mapping_harga_batam.id_area = '" + id_area + "' and id_division = m_division.id where tmp_proposal_detail.id <> '" + id + "' and sku  = '" + sku + "' and (status is null or status <> 5) and tmp_proposal_detail.id_area = '8'");
        while (reader2.Read()) {
            cotton.lib.MyDatabase.FastExec("delete from tmp_proposal_channel where id_proposal_detail = '" + reader2["id"].ToString() + "'");
            cotton.lib.MyDatabase dbBatam = new cotton.lib.MyDatabase();
            System.Data.SqlClient.SqlDataReader readerBatam = dbBatam.Query("select m_mapping_area_channel.id_channel from v_ers inner join m_division on m_division.division_code = v_ers.DIVCD"
			+" inner join m_mapping_area on m_mapping_area.id_division = m_division.id and m_mapping_area.id_area = '8'"
			+" inner join m_mapping_area_channel on m_mapping_area_channel.id_mapping_area = m_mapping_area.id"
			+" where v_ers.MATNR = '"+sku+"'");
            while (readerBatam.Read())
            {
                cotton.lib.MyDatabase.FastExec("insert into tmp_proposal_channel(id_proposal_detail,id_channel) values ('" + reader2["id"].ToString() + "','" + readerBatam["id_channel"].ToString() + "')");
            }
            readerBatam.Close();
            dbBatam.Close();
        }
        reader2.Close();
        db2.Close();
        if(isOldDeleted){
	    	bool isbatam = false;
	    	String newIdBatam = System.Diagnostics.Stopwatch.GetTimestamp()+"";
	        cotton.lib.MyDatabase.FastExec("insert into tmp_proposal_detail(id_proposal,id,sku,dozen,configuration,ipack,id_callendar,remark,comment,toko_price,rsp_price,old_sku,mdg_approval_file,mdg_approval_date,mdg_approval,created_by,id_area,sku_name,cpgcd,is_new,created_date)"
	            + " select '"+id_proposal+"','"+newIdBatam+"', '" + sku + "','" + dozen + "','" + configuration + "','" + ipack + "','" + id_callendar + "','" + remark + "','" + comment + "',(" + toko_price + "*harga_batam)/100," + rsp_price + ",'" + old_sku + "','" + mdg_approval_file + "','" + mdg_approval_date + "','" + mdg_approval + "','" + Session["id"].ToString() + "','8','" + sku_name + "','" + cpgcd + "',1,getdate()  from tmp_proposal_detail inner join v_ers on MATNR = sku inner join m_division on m_division.division_code = v_ers.DIVCD inner JOIN m_mapping_harga_batam on m_mapping_harga_batam.id_area = tmp_proposal_detail.id_area and id_division = m_division.id where tmp_proposal_detail.id = '"+id+"'");
	        database = new cotton.lib.MyDatabase();
	        reader = database.Query("select top 1 id,id_area from tmp_proposal_detail where created_by = '" + Session["id"].ToString() + "' and id = '"+newIdBatam+"'");
	        while (reader.Read())
	        {
	            isbatam = true;
	        }
	        reader.Close();
	        database.Close();
	        if (isbatam)
	        {
	            cotton.lib.MyDatabase dbBatam = new cotton.lib.MyDatabase();
	            System.Data.SqlClient.SqlDataReader readerBatam = dbBatam.Query("select m_mapping_area_channel.id_channel from v_ers inner join m_division on m_division.division_code = v_ers.DIVCD"
				+" inner join m_mapping_area on m_mapping_area.id_division = m_division.id and m_mapping_area.id_area = '8'"
				+" inner join m_mapping_area_channel on m_mapping_area_channel.id_mapping_area = m_mapping_area.id"
				+" where v_ers.MATNR = '"+sku+"'");
	            while (readerBatam.Read())
	            {
	                cotton.lib.MyDatabase.FastExec("insert into tmp_proposal_channel(id_proposal_detail,id_channel) values ('" + newIdBatam+ "','" + readerBatam["id_channel"].ToString() + "')");
	            }
	            readerBatam.Close();
	            dbBatam.Close();
	        }
        }
        Response.Write("{'status':'ok','type':'edit','msg':'" + id + "'}");
    }
    database = new cotton.lib.MyDatabase();
    reader = database.Query("select * from t_configuration where sku = '"+sku+"'");
    bool isAvailable = false;
    while (reader.Read())
    {
        isAvailable = true;
    }
    reader.Close();
    database.Close();
    if (isAvailable)
    {
        cotton.lib.MyDatabase.FastExec("update t_configuration set configuration = '" + configuration + "',dozen = '"+dozen+"' where sku = '" + sku + "'");
    }
    else {
        cotton.lib.MyDatabase.FastExec("insert into t_configuration (sku,configuration,dozen) values ('" + sku + "','" + configuration + "','"+dozen+"')");
    }
%>