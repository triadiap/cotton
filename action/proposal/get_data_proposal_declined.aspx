﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select *,convert(varchar,t_proposal_pricelist.created_date,103),sp_status tanggal from t_proposal_pricelist inner join r_proposal_status on r_proposal_status.id = status where on_behalf = '"+Session["id_on_behalf"].ToString()+"' and status in(6,7) order by t_proposal_pricelist.created_date desc");
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["proposal_number"] + "</td>");
        Response.Write("<td>" + reader["subject"] + "</td>");
        Response.Write("<td>" + reader["tanggal"] + "</td>");
        Response.Write("<td>" + reader["sp_status"] + "</td>");
		cotton.lib.MyDatabase db2 = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader2 = db2.Query("select top 1 name from dbo.h_activity inner join m_user on m_user.id = h_activity.created_by where ACTION = 'Decline Proposal'  and id_ref = '"+reader["id"].ToString()+"' order by h_activity.id desc");
        System.Collections.Generic.List<String> nm = new System.Collections.Generic.List<String>();
        while (reader2.Read()) {
            nm.Add(reader2["name"].ToString());
        }
        reader2.Close();
        Response.Write("<td>"+String.Join(",",nm)+"</td>");
        db2.Close();
        Response.Write("<td style='text-align:center'>");
        if (reader["status"].ToString() == "6" || reader["status"].ToString() == "7")
        {
            Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Edit'  onclick='do_edit(" + reader["id"].ToString() + ","+reader["status"].ToString()+")'><i class='fa fa-pencil'></i></a>");
            Response.Write("&nbsp;<a class='btn btn-xs btn-danger' data-toggle='tooltip' title='Delete'  onclick='do_delete(" + reader["id"].ToString() + ",\"" + reader["proposal_number"] + "\")'><i class='fa fa-trash'></i></a>");
        }
        else {
            Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='View'  onclick='do_view(" + reader["id"].ToString() + ")'><i class='fa fa-envelope-o'></i></a>");
        }
        Response.Write("</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>