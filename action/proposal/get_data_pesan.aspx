﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    String id = Request.Form["id"].ToString();
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select t_message.*,m_user.name from t_message inner join m_user on m_user.id = t_message.created_by where id_proposal = '"+id+"' and id_proposal != 0 order by t_message.created_date asc");
    while (reader.Read())
    {
        if (reader["created_by"].ToString() == Session["id"].ToString())
        {
            Response.Write("<div class='direct-chat-msg'>");
            Response.Write("<div class='direct-chat-info clearfix'>");
            Response.Write("<span class='direct-chat-name pull-left'>"+reader["name"].ToString()+"</span>");
            Response.Write("<span class='direct-chat-timestamp pull-right'>"+reader["created_date"].ToString()+"</span>");
            Response.Write("</div>");
            Response.Write("<div class='direct-chat-text'>");
            Response.Write(reader["message"].ToString());
            Response.Write("</div>");
            Response.Write("</div>");
        }
        else {
            Response.Write("<div class='direct-chat-msg right'>");
            Response.Write("<div class='direct-chat-info clearfix'>");
            Response.Write("<span class='direct-chat-name pull-right'>"+reader["name"].ToString()+"</span>");
            Response.Write("<span class='direct-chat-timestamp pull-left'>"+reader["created_date"].ToString()+"</span>");
            Response.Write("</div>");
            Response.Write("<div class='direct-chat-text'>");
            Response.Write(reader["message"].ToString());
            Response.Write("</div>");
            Response.Write("</div>");
        }
    }
    reader.Close();
    database.Close();
%>