﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    String id = Request.Form["id"].ToString();
    String subject = Request.Form["subject"].ToString();
    subject = subject.Replace("'","");
    String number = "";
    cotton.lib.MyDatabase db = new cotton.lib.MyDatabase();
    String sql = "select distinct v_ers.DIVCD,right(YEAR(getdate()),2) [year] from t_proposal_detail inner join v_ers on v_ers.MATNR = t_proposal_detail.sku inner join t_proposal_pricelist on t_proposal_pricelist.id = id_proposal where id_proposal ='" + id + "' and (t_proposal_pricelist.proposal_number is null or t_proposal_pricelist.proposal_number = '')";
    if (id == "0")
    {
        sql = "select distinct v_ers.DIVCD,right(YEAR(getdate()),2) [year] from t_proposal_detail inner join v_ers on v_ers.MATNR = t_proposal_detail.sku where id_proposal is null and created_by = '"+Session["id"]+"'";
    }
    System.Data.SqlClient.SqlDataReader reader = db.Query(sql);
    while (reader.Read()) {
        Random rand = new Random();
        int intNumber = cotton.lib.MyDatabase.getNumber("proposal", reader["year"].ToString());
        if (intNumber <= 9)
        {
            number = reader["DIVCD"].ToString()+reader["year"].ToString() + "00" + intNumber + "-0";
        }
        else if (intNumber <= 99)
        {
            number = reader["DIVCD"].ToString()+reader["year"].ToString() + "0" + intNumber + "-0";
        }
        else {
            number = reader["DIVCD"].ToString()+reader["year"].ToString() + intNumber + "-0";
        }
    }
    reader.Close();
    db.Close();
    if (id == "0")
    {
        cotton.lib.MyDatabase.FastExec("insert into t_proposal_pricelist(subject,status,created_by,created_date,proposal_number,on_behalf) "
            + " values('" + subject + "',2,'" + Session["id"] + "',getdate(),'" + number + "','"+Session["id_on_behalf"]+"')");
        cotton.lib.MyDatabase.FastExec("update t_proposal_detail set id_proposal = (select top 1 id from t_proposal_pricelist where created_by = '" + Session["id"].ToString() + "' order by id desc), status = 2 where status is null and created_by = '" + Session["id"].ToString() + "'");
        //cotton.lib.MyDatabase.FastExec("insert into t_approval (id_proposal,app_level,id_approver,status_approve) values((select top 1 id from t_proposal_pricelist where created_by = '" + Session["id"].ToString() + "' order by id desc),'1',(select id from m_user where noreg = (select noreg_lm from m_user b where b.id = '"+Session["id_on_behalf"].ToString()+"')),0)");
        cotton.lib.MyDatabase.FastExec("insert into t_approval (id_proposal,app_level,id_approver,status_approve) select (select top 1 id from t_proposal_pricelist where created_by = '" + Session["id"].ToString() + "' order by id desc),app_level,m_user.id,0 from m_approval inner join m_user on m_user.id_job_title = id_app_job_title where app_level = '1' and m_user.id in (select m_user_segments.id_user from m_user_segments where segments in (select SECCD from t_proposal_detail inner join v_ers on v_ers.MATNR = sku where id_proposal = (select top 1 id from t_proposal_pricelist where created_by = '" + Session["id"].ToString() + "' order by id desc))  and m_user.status = '5') and m_approval.id_sales_org = '" + Session["id_sales_org_on_behalf"] + "'");
        cotton.lib.MyDatabase.FastExec("insert into h_activity([table],id_ref,[action],created_by,created_date,on_behalf) values ('t_proposal_pricelist','0','Create Proposal','"+Session["id"].ToString()+"',getdate(),'" + Session["id_on_behalf"].ToString() + "')");
        id = "(select top 1 id from t_proposal_pricelist where created_by = '" + Session["id"].ToString() + "' order by id desc)";
    }
    else {
        cotton.lib.MyDatabase.FastExec("update tmp_change_effective_date set cur_app_level = (select max(t_approval.app_level) from t_approval where t_approval.id_proposal = tmp_change_effective_date.id_proposal) where tmp_change_effective_date.id_proposal = '" + id + "'");
        cotton.lib.MyDatabase.FastExec("update tmp_change_effective_date set cur_app_level = (select max(t_approval.app_level)+1 from t_approval where t_approval.id_proposal = tmp_change_effective_date.id_proposal) where tmp_change_effective_date.id_proposal in (select id from t_proposal_pricelist where id = '"+id+"' and status = '4')");
        db = new cotton.lib.MyDatabase();
        cotton.lib.MyDatabase.FastExec("update t_proposal_detail set status = '1' where id_proposal = '" + id + "' and status is null");
        reader = db.Query("select max(isnull(status,0)) status from t_proposal_detail where id_proposal = '" + id + "'");
        while (reader.Read()) {
            if (reader["status"].ToString() == "6" || reader["status"].ToString() == "7" || reader["status"].ToString() == "1")
            {
                cotton.lib.MyDatabase.FastExec("update t_proposal_pricelist set proposal_number = left(proposal_number,7)+'-'+Convert(VARCHAR,(right(proposal_number,1)+1)) where (proposal_number is not null and proposal_number != '') and id='" + id + "'");
                cotton.lib.MyDatabase.FastExec("update t_proposal_pricelist set proposal_number = '" + number + "' where id = '" + id + "' and (t_proposal_pricelist.proposal_number is null or t_proposal_pricelist.proposal_number = '')");
                cotton.lib.MyDatabase.FastExec("update t_proposal_pricelist set status = '2' where id = '" + id + "'");
                cotton.lib.MyDatabase.FastExec("update t_proposal_detail set status = '2' where id_proposal = '" + id + "'");
                cotton.lib.MyDatabase.FastExec("delete from t_approval where id_proposal = '" + id + "'");
                //cotton.lib.MyDatabase.FastExec("insert into t_approval (id_proposal,app_level,id_approver,status_approve) values('" + id + "','1',(select id from m_user where noreg = (select noreg_lm from m_user b where b.id = '"+Session["id_on_behalf"].ToString()+"') and status = '5'),0)");
                cotton.lib.MyDatabase.FastExec("insert into t_approval (id_proposal,app_level,id_approver,status_approve) select " + id + ",app_level,m_user.id,0 from m_approval inner join m_user on m_user.id_job_title = id_app_job_title where app_level = '1' and m_user.id in (select m_user_segments.id_user from m_user_segments where segments in (select SECCD from t_proposal_detail inner join v_ers on v_ers.MATNR = sku where id_proposal = '" + id + "')  and m_user.status = '5') and m_approval.id_sales_org = '" + Session["id_sales_org_on_behalf"] + "'");
                cotton.lib.MyDatabase.FastExec("delete from t_pricelist_detail where id_proposal = '" + id + "'");
                cotton.lib.MyDatabase.FastExec("insert into h_activity([table],id_ref,[action],created_by,created_date,on_behalf) values ('t_proposal_pricelist',"+id+",'Submit Proposal','"+Session["id"].ToString()+"',getdate(),'" + Session["id_on_behalf"].ToString() + "')");
            }
            else {
                if (reader["status"].ToString() != "0")
                {
                    cotton.lib.MyDatabase.FastExec("update t_proposal_pricelist set status = '" + reader["status"].ToString() + "' where id = '" + id + "'");
                }
                cotton.lib.MyDatabase.FastExec("insert into h_activity([table],id_ref,[action],created_by,created_date,on_behalf) values ('t_proposal_pricelist',"+id+",'Update Proposal','"+Session["id"].ToString()+"',getdate(),'" + Session["id_on_behalf"].ToString() + "')");
            }
        }
        reader.Close();
        db.Close();
        db = new cotton.lib.MyDatabase();
        reader = db.Query("select sum(status_approve) status,count(*) total,max(app_level)+1 cur_app_level from t_approval where id_proposal = '" + id + "'");
        while (reader.Read())
        {
            if (reader["status"].ToString() == reader["total"].ToString())
            {
                cotton.lib.MyDatabase.FastExec("insert into t_approval (id_proposal,app_level,id_approver,status_approve) select " + id + ",app_level,m_user.id,0 from m_approval inner join m_user on m_user.id_job_title = id_app_job_title where app_level = '" + reader["cur_app_level"].ToString() + "' and m_user.id in (select m_user_segments.id_user from m_user_segments where segments in (select SECCD from t_proposal_detail inner join v_ers on v_ers.MATNR = sku where id_proposal = '" + id + "') and m_user.status = '5') and m_approval.id_sales_org = '" + Session["id_sales_org_on_behalf"] + "'");
            }
        }
        reader.Close();
        db.Close();
        db = new cotton.lib.MyDatabase();
        reader = db.Query("select sum(status_approve) status,count(*) total,(select count(distinct status) from t_proposal_detail where id_proposal = '"+id+"' and id_area <> '8') total_status from t_approval where id_proposal = '" + id + "'");
        while (reader.Read())
        {
            if (reader["status"].ToString() == reader["total"].ToString() && reader["total_status"].ToString() == "1")
            {
                cotton.lib.MyDatabase.FastExec("update t_proposal_detail set status = '4' where id_proposal  = '" + id + "' and status !='5'");
                cotton.lib.MyDatabase.FastExec("update t_proposal_pricelist set status = '4' where id  = '" + id + "' and status !='5'");
            }
        }
        reader.Close();
        db.Close();
    }
    db = new cotton.lib.MyDatabase();
    reader = db.Query("select proposal_number from t_proposal_pricelist where id = " + id + "");
    while (reader.Read())
    {
        number = reader["proposal_number"].ToString();
    }
    reader.Close();
    db.Close();
    cotton.lib.MyDatabase.FastExec("DECLARE @ID_PROPOSAL varchar(100) Exec sp_email_proposal_need_approval_awal  @ID_PROPOSAL = '" + number+"'");
    cotton.lib.MyDatabase.FastExec("DECLARE @ID_PROPOSAL varchar(100) Exec sp_email_proposal_need_approval_effective_date  @ID_PROPOSAL = '" + number+"'");
    cotton.lib.MyDatabase.FastExec("DECLARE @ID_PROPOSAL varchar(100) Exec sp_email_proposal_effective_date  @ID_PROPOSAL = '" + number+"'");
    cotton.lib.MyDatabase.FastExec("DECLARE @ID_PROPOSAL varchar(100) Exec sp_email_proposal_effective_date_reviewer  @ID_PROPOSAL = '" + number+"'");
    cotton.lib.MyDatabase.FastExec("DECLARE @ID_PROPOSAL varchar(100) Exec sp_email_proposal_fully_approve  @ID_PROPOSAL = '" + number+"'");
    cotton.lib.MyDatabase.FastExec("DECLARE @ID_PROPOSAL varchar(100) Exec sp_email_proposal_no_need_approval_effective_date  @ID_PROPOSAL = '" + number+"'");
    db = new cotton.lib.MyDatabase();
    reader = db.Query("select distinct t_proposal_pricelist.id,proposal_number,name,email from t_proposal_pricelist "
    +" inner join t_proposal_detail on t_proposal_pricelist.id = t_proposal_detail.id_proposal"
    +" inner join v_ers on matnr = sku"
    +" inner join m_user_segments on segments = seccd"
    +" inner join m_user on m_user.id_job_title in('4','15','14') and m_user.id = id_user"
    +" where t_proposal_pricelist.status != '3' and t_proposal_pricelist.status != '4' and t_proposal_pricelist.status != '5' and t_proposal_pricelist.id not in(select distinct id_proposal from tmp_change_effective_date)  and t_proposal_pricelist.id = "+id+" and m_user.status = '5'");
    number = "";
    while (reader.Read()) {
        String body = "<style>.header{text-align:center;font-weight:bold;background:#EFEFEF}td{border:1px solid #000;}</style>";
        body += "Dear " + reader["name"].ToString()+"<br/>";
        body += "<br/>";
        body += "Please find Proposal "+reader["proposal_number"].ToString()+" below to be reviewed<br/>";
        number = reader["proposal_number"].ToString();
        body += "<table cellpadding='3' cellspacing='0'>";
        body += "<tr class='header'><td rowspan='3'>#</td><td rowspan='3'>SKU</td><td rowspan='3'>Description</td><td colspan='2'>Current</td><td colspan='5'>Proposed</td><td rowspan='3'>Area Channel</td><td rowspan='3'>Remark</td></tr><tr class='header'><td rowspan='2'>RSP/pcs</td><td rowspan='2'>Toko Price/pcs</td><td rowspan='2'>RSP/pcs</td><td rowspan='2'>Toko Price/pcs</td><td rowspan='2'>Effective From</td><td colspan='2'>Ret Margin</td></tr><tr class='header'><td>Rp</td><td>%</td></tr>";
        cotton.lib.MyDatabase db2 = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader2 = db2.Query("select * from v_email_body where id_proposal = '"+reader["id"].ToString()+"'");
        int i = 0;
        while (reader2.Read()) {
            i++;
            body += "<tr>";
            body += "<td>"+i+"</td>";
            body += "<td>"+reader2[1].ToString()+"</td>";
            body += "<td>"+reader2[2].ToString()+"</td>";
            body += "<td>"+reader2[3].ToString()+"</td>";
            body += "<td>"+reader2[4].ToString()+"</td>";
            body += "<td>"+reader2[5].ToString()+"</td>";
            body += "<td>"+reader2[6].ToString()+"</td>";
            body += "<td>"+Convert.ToDateTime(reader2[7]).ToString("yyyy-MM-dd")+"</td>";
            body += "<td>"+reader2[8].ToString()+"</td>";
            body += "<td>"+reader2[9].ToString()+"</td>";
            body += "<td>"+reader2[10].ToString()+"</td>";
            body += "<td>"+reader2[11].ToString()+"</td>";
            body += "</tr>";
        }
        reader2.Close();
        db2.Close();
        body += "</table><br/>";
        body += "<br/>";
        body += "Please click link below for detail<br/>";
        body += "<br/>";
        body += "<a href='"+cotton.lib.EmailClass.getUrl()+"'>" + cotton.lib.EmailClass.getUrl() + "</a><br/>";
        body += "Disclaimer<br/>";
        body += "Please DO NOT REPLY to this system generated mailing";
        cotton.lib.EmailClass.SendMail(reader["email"].ToString(), "[Cotton] Proposal "+number+" Has Been Proposed", body);
    }
    reader.Close();
    db.Close();
    db = new cotton.lib.MyDatabase();
    reader = db.Query("select distinct t_pricelist_detail.id_proposal,pricelist_number,name,email"
    +" from t_pricelist "
    +" inner join t_pricelist_detail on t_pricelist_detail.id_pricelist = t_pricelist.id "
    +" inner join t_proposal_detail on t_proposal_detail.id_proposal = t_pricelist_detail.id_proposal"
    +" inner join v_ers on matnr = t_proposal_detail.sku"
    +" inner join m_user_segments on segments = seccd"
    +" inner join m_user on m_user.id = id_user"
    +" inner join m_mapping_menu on m_mapping_menu.id_job_title = m_user.id_job_title and m_mapping_menu.id_menu = '44'"
    +" where t_pricelist.status = '5' and t_pricelist_detail.id_proposal = "+id);
    number = "";
    while (reader.Read()) {
        String body = "<style>.header{text-align:center;font-weight:bold;background:#EFEFEF}td{border:1px solid #000;}</style>";
        body += "Dear " + reader["name"].ToString()+"<br/>";
        body += "<br/>";
        body += "Pricelist "+reader["pricelist_number"].ToString()+" has been changed. See the details below.<br/>";
        number = reader["pricelist_number"].ToString();
        body += "<table cellpadding='3' cellspacing='0'>";
        body += "<tr class='header'><td>#</td><td>Product Code</td><td>Product Description</td><td>Current Toko Price</td><td>Current RSP Price</td><td>Proposed Toko Price</td><td>Proposed RSP Price</td><td>Area</td><td>Channel</td><td>Remark</td><td>Creator</td></tr>";
        cotton.lib.MyDatabase db2 = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader2 = db2.Query("select m_proposal_remarks.remark, dbo.get_channel(t_proposal_detail.id) channel,area_name,t_proposal_detail.sku,sku_name,pricelist_number,proposal_number,t_proposal_detail.toko_price,t_proposal_detail.rsp_price,(((t_proposal_detail.rsp_price-t_proposal_detail.toko_price)/t_proposal_detail.toko_price)*100),t_new_proposed_price.toko_price new_toko_price,t_new_proposed_price.rsp_price new_rsp_price,(((t_new_proposed_price.rsp_price - t_new_proposed_price.toko_price)/ t_new_proposed_price.toko_price)*100),t_proposal_detail.id,m_user.name  from t_pricelist inner join t_pricelist_detail on t_pricelist.id = id_pricelist inner join t_proposal_pricelist on t_proposal_pricelist.id = t_pricelist_detail.id_proposal inner join t_proposal_detail on t_proposal_detail.id_proposal= t_proposal_pricelist.id inner join t_new_proposed_price on t_new_proposed_price.id_proposal_detail = t_proposal_detail.id INNER JOIN m_area on m_area.id = id_area INNER JOIN m_proposal_remarks on t_proposal_detail.remark = m_proposal_remarks.id inner join m_user on m_user.id = t_proposal_detail.created_by where t_proposal_detail.id_proposal = '"+reader["id_proposal"].ToString()+"' order by sku,t_proposal_detail.id");
        int i = 0;
        while (reader2.Read()) {
            i++;
            body += "<tr>";
            body += "<td>"+i+"</td>";
            body += "<td>"+reader2["sku"].ToString()+"</td>";
            body += "<td>"+reader2["sku_name"].ToString()+"</td>";
            body += "<td>"+Convert.ToDouble(reader2["toko_price"].ToString().TrimEnd()).ToString("#,##0.00")+"</td>";
            body += "<td>"+Convert.ToDouble(reader2["rsp_price"].ToString().TrimEnd()).ToString("#,##0.00")+"</td>";
            body += "<td>"+Convert.ToDouble(reader2["new_toko_price"].ToString().TrimEnd()).ToString("#,##0.00")+"</td>";
            body += "<td>"+Convert.ToDouble(reader2["new_rsp_price"].ToString().TrimEnd()).ToString("#,##0.00")+"</td>";
            body += "<td>"+reader2["area_name"].ToString()+"</td>";
            body += "<td>"+reader2["channel"].ToString()+"</td>";
            body += "<td>"+reader2["remark"].ToString()+"</td>";
            body += "<td>"+reader2["name"].ToString()+"</td>";
            body += "</tr>";
        }
        reader2.Close();
        db2.Close();
        body += "</table><br/>";
        body += "<br/>";
        body += "Please click below link to review the detail :<br/>";
        body += "<br/>";
        body += "<a href='"+cotton.lib.EmailClass.getUrl()+"'>" + cotton.lib.EmailClass.getUrl() + "</a><br/>";
        body += "Disclaimer<br/>";
        body += "Please DO NOT REPLY to this system generated mailing";
        cotton.lib.EmailClass.SendMail(reader["email"].ToString(), "[Cotton] Pricelist "+number+" Has Price Change", body);
    }
    reader.Close();
    db.Close();
%>