﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    String id = Request.Form["id"].ToString();
    String status = Request.Form["status"].ToString();
    String all_comment = "";
    bool isReject = false;
    if(Request.Form["all_comment"] != null){
        all_comment = Request.Form["all_comment"].ToString();
    }
    String[] tmp = status.Split(',');
    for (int i = 0; i < tmp.Length; i++) {
        String[] data = tmp[i].Split(':');
        if (data[1] == "approve")
        {
            cotton.lib.MyDatabase.FastExec("update t_proposal_detail set status = '3' where id = '" + data[0] + "'");
        }
        else {
            cotton.lib.MyDatabase.FastExec("update t_proposal_detail set status = '7' where id = '" + data[0] + "'");
        }
    }
    String comment = Request.Form["comment"].ToString();
    tmp = comment.Split(',');
    for (int i = 0; i < tmp.Length; i++) {
        String[] data = tmp[i].Split(':');
        cotton.lib.MyDatabase.FastExec("update t_proposal_detail set comment = @param where id = '" + data[0] + "'",new cotton.lib.MyParams[] {new cotton.lib.MyParams("param",HttpUtility.UrlDecode(data[1]))});
    }

    cotton.lib.MyDatabase.FastExec("update t_proposal_pricelist set status = (select top 1 max(status) from t_proposal_detail where id_proposal = '" + id + "' and id_area <> '8') where id='" + id + "'");
    cotton.lib.MyDatabase db = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = db.Query("select min(status) status from t_proposal_detail where id_proposal = '" + id + "' and id_area <> '8'");
    bool isApprove = false;
    while (reader.Read()) {
        if (reader["status"].ToString() == "3")
        {
            isApprove = true;
            //cotton.lib.MyDatabase.FastExec("update t_proposal_detail set status = 3 where id_proposal = '" + id + "'");
            cotton.lib.MyDatabase.FastExec("insert into h_approval(id_proposal,approval_level,approved_by,approved_date,on_behalf) values('" + id + "',(select top 1 app_level from m_approval where id_app_job_title = '" + Session["id_job_title_on_behalf"].ToString() + "'),'" + Session["id"] + "',getdate(),'"+Session["id_on_behalf"]+"')");
            cotton.lib.MyDatabase.FastExec("update t_approval set status_approve = '1' where id_proposal = '" + id + "' and id_approver = '" + Session["id_on_behalf"] + "'");
        }
        else {
            isReject = true;
        }
    }
    reader.Close();
    db.Close();
    if (isApprove)
    {
        cotton.lib.MyDatabase.FastExec("insert into h_activity([table],id_ref,[action],created_by,created_date,on_behalf) values ('t_proposal_pricelist','" + id + "','Approve Proposal','" + Session["id"].ToString() + "',getdate(),'" + Session["id_on_behalf"].ToString() + "')");
        db = new cotton.lib.MyDatabase();
        reader = db.Query("select sum(status_approve) status,count(*) total,max(app_level)+1 cur_app_level from t_approval where id_proposal = '" + id + "'");
        while (reader.Read())
        {
            if (reader["status"].ToString() == reader["total"].ToString())
            {
                cotton.lib.MyDatabase.FastExec("insert into t_approval (id_proposal,app_level,id_approver,status_approve) select " + id + ",app_level,m_user.id,0 from m_approval inner join m_user on m_user.id_job_title = id_app_job_title where app_level = '" + reader["cur_app_level"].ToString() + "' and m_user.id in (select m_user_segments.id_user from m_user_segments where segments in (select SECCD from t_proposal_detail inner join v_ers on v_ers.MATNR = sku where id_proposal = '" + id + "') and m_user.status = '5') and m_approval.id_sales_org = '" + Session["id_sales_org_on_behalf"] + "'");
            }
        }
        reader.Close();
        db.Close();
        db = new cotton.lib.MyDatabase();
        reader = db.Query("select sum(status_approve) status,count(*) total,(select count(distinct status) from t_proposal_detail where id_proposal = '"+id+"' and id_area <> '8') total_status from t_approval where id_proposal = '" + id + "'");
        while (reader.Read())
        {
            if (reader["status"].ToString() == reader["total"].ToString() && reader["total_status"].ToString() == "1")
            {
                cotton.lib.MyDatabase.FastExec("update t_proposal_detail set status = '4' where id_proposal  = '" + id + "'");
                cotton.lib.MyDatabase.FastExec("update t_proposal_pricelist set status = '4' where id  = '" + id + "'");
            }else{
                Response.Write("Reject");
            }
        }
        reader.Close();
        db.Close();
    }
    else {
        cotton.lib.MyDatabase.FastExec("insert into h_activity([table],id_ref,[action],created_by,created_date,on_behalf) values ('t_proposal_pricelist','"+id+"','Decline Proposal','"+Session["id"].ToString()+"',getdate(),'" + Session["id_on_behalf"].ToString() + "')");
    }
    String number = "";
    db = new cotton.lib.MyDatabase();
    reader = db.Query("select proposal_number from t_proposal_pricelist where id = '" + id + "'");
    while (reader.Read())
    {
        number = reader["proposal_number"].ToString();
    }
    reader.Close();
    db.Close();
    db = new cotton.lib.MyDatabase();
    reader = db.Query("select t_proposal_pricelist.id,proposal_number,name,email from t_proposal_pricelist inner join m_user on m_user.id = t_proposal_pricelist.on_behalf where t_proposal_pricelist.status = '7' and t_proposal_pricelist.id = '"+id+"'");
    while (reader.Read()) {
        String body = "<style>td{border:1px solid #000;}</style>";
        body += "Dear " + reader["name"].ToString()+"<br/>";
        body += "<br/>";
        body += "Your Proposal "+reader["proposal_number"].ToString()+" has been declined.<br/>";
        body += "With comment :<br/>"+all_comment+"<br/>";
        body += "<br/>";
        body += "Please click link below for detail<br/>";
        body += "<br/>";
        body += "<a href='"+cotton.lib.EmailClass.getUrl()+"'>" + cotton.lib.EmailClass.getUrl() + "</a><br/>";
        body += "Disclaimer<br/>";
        body += "Please DO NOT REPLAY to this system generated mailing";
        cotton.lib.EmailClass.SendMail(reader["email"].ToString(), "[Cotton] Your Proposal Has Been Rejected", body);
    }
    reader.Close();
    db.Close();

    db = new cotton.lib.MyDatabase();
    reader = db.Query("select t_proposal_pricelist.id,proposal_number,name,email from t_proposal_pricelist inner join t_approval on t_proposal_pricelist.id = id_proposal inner join m_user on m_user.id = id_approver where t_proposal_pricelist.status = '7' and t_proposal_pricelist.id = '"+id+"' and app_level <= (select max(app_level) from t_approval where id_proposal = '"+id+"' and status_approve = '1')");
    while (reader.Read()) {
        String body = "<style>td{border:1px solid #000;}</style>";
        body += "Dear " + reader["name"].ToString()+"<br/>";
        body += "<br/>";
        body += "Your Proposal "+reader["proposal_number"].ToString()+" has been declined.<br/>";
        body += "With comment :<br/>"+all_comment+"<br/>";
        body += "<br/>";
        body += "Please click link below for detail<br/>";
        body += "<br/>";
        body += "<a href='"+cotton.lib.EmailClass.getUrl()+"'>" + cotton.lib.EmailClass.getUrl() + "</a><br/>";
        body += "Disclaimer<br/>";
        body += "Please DO NOT REPLAY to this system generated mailing";
        cotton.lib.EmailClass.SendMail(reader["email"].ToString(), "[Cotton] Your Proposal Has Been Rejected", body);
    }
    reader.Close();
    db.Close();
    if(isReject){
        cotton.lib.MyDatabase.FastExec("delete from t_approval where id_proposal = '" + id + "'");
    }
    cotton.lib.MyDatabase.FastExec("DECLARE @ID_PROPOSAL varchar(100) Exec sp_email_proposal_need_approval  @ID_PROPOSAL = '" + number+"'");
    cotton.lib.MyDatabase.FastExec("DECLARE @ID_PROPOSAL varchar(100) Exec sp_email_proposal_need_approval_effective_date  @ID_PROPOSAL = '" + number+"'");
    cotton.lib.MyDatabase.FastExec("DECLARE @ID_PROPOSAL varchar(100) Exec sp_email_proposal_fully_approve  @ID_PROPOSAL = '" + number+"'");
    cotton.lib.MyDatabase.FastExec("DECLARE @ID_PROPOSAL varchar(100) Exec sp_email_proposal_effective_date  @ID_PROPOSAL = '" + number+"'");
    cotton.lib.MyDatabase.FastExec("delete from tmp_change_effective_date where id_proposal = '" + id + "' and cur_app_level = (select max(app_level) from t_approval where id_proposal = '"+id+"')");
%>