﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select *,convert(varchar,t_proposal_pricelist.created_date,103) tanggal from t_proposal_pricelist where on_behalf = '"+Session["id_on_behalf"].ToString()+"' and status = '1' order by t_proposal_pricelist.created_date desc");
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["proposal_number"] + "</td>");
        Response.Write("<td>" + reader["subject"] + "</td>");
        Response.Write("<td>" + reader["tanggal"] + "</td>");
        Response.Write("<td style='text-align:center'>");
        Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Edit'  onclick='do_edit("+reader["id"].ToString()+","+reader["status"].ToString()+")'><i class='fa fa-pencil'></i></a>");
        Response.Write("&nbsp;<a class='btn btn-xs btn-danger' data-toggle='tooltip' title='Delete'  onclick='do_delete("+reader["id"].ToString()+",\""+reader["proposal_number"]+"\")'><i class='fa fa-trash'></i></a>");
        Response.Write("</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>