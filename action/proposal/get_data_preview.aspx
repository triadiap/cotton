﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    String sql = "";
    if (Request.Form["id"] == "0") {
        sql = "select sku,	sku_name,	rsp_price,	((toko_price * ipack )/ 1.1) toko_price_nppn_cs,	((toko_price* dozen)  / 1.1) toko_price_nppn_lusin,	(toko_price / 1.1) toko_price_nppn,	((toko_price) * ipack) toko_price_ppn_cs,	((toko_price) * dozen) toko_price_ppn_lusin,	(toko_price) toko_price_ppn,	((toko_price * ipack) / 1.1) rsp_price_nppn_cs,	((rsp_price) * ipack) rsp_price_ppn_cs from tmp_proposal_detail where created_by = '"+Session["id"].ToString()+"' and status is null";
    } else {        
        sql = "select sku,	sku_name,	rsp_price,	((toko_price * ipack) / 1.1) toko_price_nppn_cs,	((toko_price * dozen) / 1.1) toko_price_nppn_lusin,	(toko_price / 1.1) toko_price_nppn,	((toko_price) * ipack) toko_price_ppn_cs,	((toko_price) * dozen) toko_price_ppn_lusin,	(toko_price) toko_price_ppn,	((toko_price * ipack) / 1.1) rsp_price_nppn_cs,	((rsp_price) * ipack) rsp_price_ppn_cs from tmp_proposal_detail where id_proposal = '"+Request.Form["id"].ToString()+"'";
    }
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query(sql);
    String data = "[";
    while (reader.Read())
    {
        data += "{";
        data += "'sku':'" + reader["sku"].ToString().TrimEnd() + "',";
        data += "'sku_desc':'" + reader["sku_name"].ToString().TrimEnd() + "',";
        data += "'rsp_price':'" + Convert.ToDouble(reader["rsp_price"].ToString().TrimEnd()).ToString("#,##0.00")+"',";
        data += "'toko_price_nppn_cs':'" + Convert.ToDouble(reader["toko_price_nppn_cs"].ToString().TrimEnd()).ToString("#,##0.00") + "',";
        data += "'toko_price_nppn_lusin':'" + Convert.ToDouble(reader["toko_price_nppn_lusin"].ToString().TrimEnd()).ToString("#,##0.00") + "',";
        data += "'toko_price_nppn':'" + Convert.ToDouble(reader["toko_price_nppn"].ToString().TrimEnd()).ToString("#,##0.00") + "',";
        data += "'toko_price_ppn_cs':'" + Convert.ToDouble(reader["toko_price_ppn_cs"].ToString().TrimEnd()).ToString("#,##0.00") + "',";
        data += "'toko_price_ppn_lusin':'" + Convert.ToDouble(reader["toko_price_ppn_lusin"].ToString().TrimEnd()).ToString("#,##0.00") + "',";
        data += "'toko_price_ppn':'" + Convert.ToDouble(reader["toko_price_ppn"].ToString().TrimEnd()).ToString("#,##0.00") + "',";
        data += "'rsp_price_nppn_cs':'" + Convert.ToDouble(reader["rsp_price_nppn_cs"].ToString().TrimEnd()).ToString("#,##0.00") + "',";
        data += "'rsp_price_ppn_cs':'" + Convert.ToDouble(reader["rsp_price_ppn_cs"].ToString().TrimEnd()).ToString("#,##0.00") + "'";
        data += "},";
    }
    if (data.Length > 1) {
        data = data.Substring(0, data.Length - 1);
    }
    data += "]";
    Response.Write(data);
    reader.Close();
    database.Close();
%>