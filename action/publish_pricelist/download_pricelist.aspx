﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 

    using (OfficeOpenXml.ExcelPackage pck = new OfficeOpenXml.ExcelPackage())
    {
        String id = Request.QueryString["id"].ToString();
        String id_division = Request.QueryString["id_division"].ToString();
        String []AlphabetIndex = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        cotton.lib.MyDatabase mDb = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader mReader = mDb.Query("select * from r_generate_pricelist where [order] <> '' and kolom_name <> 'none' order by [order] asc");
        List<String> kolom_desc = new List<String>();
        List<String> kolom_name = new List<String>();
        while (mReader.Read()) {
            kolom_desc.Add(mReader["kolom_desc"].ToString());
            kolom_name.Add(mReader["kolom_name"].ToString());
        }
        mReader.Close();
        mDb.Close();
        String pricelistNumber ="";
        String publishDate = "";
        mDb = new cotton.lib.MyDatabase();
        mReader = mDb.Query("select pricelist_number,convert(varchar,pricelist_date,103) pricelist_date  from t_pricelist where id = '"+id+"'");
        while (mReader.Read()) {
            pricelistNumber = mReader["pricelist_number"].ToString();
            publishDate = mReader["pricelist_date"].ToString();
        }
        mReader.Close();
        mDb.Close();
        String division = "";
        mDb = new cotton.lib.MyDatabase();
        mReader = mDb.Query("select * from m_division where id = '"+id_division+"'");
        while (mReader.Read()) {
        	division  = mReader["division_code"].ToString();
        }
        mReader.Close();
        mDb.Close();
        /*String sql = "select id_area,id_channel,area_name,channel from m_mapping_area"
        +" inner join m_mapping_area_channel on m_mapping_area.id = m_mapping_area_channel.id_mapping_area"
        +" inner join m_area on m_area.id = id_area"
        +" inner join m_channel on m_channel.id = id_channel"
        +" where id_division = '"+id_division+"'";*/
        String sql = "select m_area.id id_area,m_channel.id id_channel,area_name,channel,full_name from m_area inner join m_channel on 1=1 order by m_area.id";
        cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader = database.Query(sql);
        while (reader.Read())
        {
            OfficeOpenXml.ExcelWorksheet ws = pck.Workbook.Worksheets.Add(reader["area_name"].ToString().TrimEnd() + "-"+ reader["channel"].ToString().TrimEnd());
            cotton.lib.MyDatabase database2 = new cotton.lib.MyDatabase();
            System.Data.SqlClient.SqlDataReader reader2 = database2.Query("select * from v_pricelist_all where (id = '"+id+"' or status = '5') and (select count(*) from t_approval where t_approval.id_proposal = v_pricelist_all.id_proposal and t_approval.status_approve = '0') = 0 and division = (select division_code from m_division where id = '"+id_division+"') area ='"+reader["area_name"].ToString().TrimEnd()+"' and channel ='"+reader["channel"].ToString().TrimEnd()+"' and division = '"+division+"' order by category asc");
            int rowIndex = 1;
            for (int i = 0; i < kolom_desc.Count; i++) {
                ws.Cells[AlphabetIndex[i] + rowIndex].Value = kolom_desc[i];
            }
            while (reader2.Read()) {
                rowIndex++;
                for (int i = 0; i < kolom_name.Count; i++) {
                	if(kolom_name[i].Contains("ppn")){
                    	ws.Cells[AlphabetIndex[i] + rowIndex].Value = Convert.ToDouble(reader2[kolom_name[i]].ToString()).ToString("#,##0");  
                	}else{
                    	ws.Cells[AlphabetIndex[i] + rowIndex].Value = reader2[kolom_name[i]].ToString();                    
                    }
                    if (reader2["id"].ToString() == id) {
                        ws.Cells[AlphabetIndex[i] + rowIndex].Style.Font.Bold = true;
                    }
                }
            }
            reader2.Close();
            database2.Close();
        }
        reader.Close();
        database.Close();
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;  filename=Pricelist "+pricelistNumber+".xlsx");
        Response.BinaryWrite(pck.GetAsByteArray());
        Response.End();
    }
%>