﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    String id = Request.Form["id"].ToString();
    String id_division = Request.Form["id_division"].ToString();
    cotton.lib.MyDatabase.FastExec("delete from t_pricelist_detail where id_proposal in(select t_pricelist_detail.id_proposal from t_pricelist_detail inner join t_approval on t_approval.id_proposal = t_pricelist_detail.id_proposal where status_approve = '0' and id_pricelist = '"+id+"')");
    cotton.lib.MyDatabase.FastExec("delete from t_approval from t_pricelist_detail inner join t_approval on t_approval.id_proposal = t_pricelist_detail.id_proposal where status_approve = '0' and id_pricelist = '"+id+"'");
    cotton.lib.MyDatabase.FastExec("update old set old.status = 8 from t_proposal_detail old inner join t_proposal_detail new on new.sku = old.sku and old.id_area = new.id_area where old.status = 5 and new.id_proposal in (select id_proposal from t_pricelist_detail where id_pricelist = '"+id+"')");
    cotton.lib.MyDatabase.FastExec("update t_proposal_detail set status = '5' where id in(select t_proposal_detail.id from t_proposal_detail inner join t_pricelist_detail on t_proposal_detail.id_proposal = t_pricelist_detail.id_proposal where id_pricelist = '"+id+"')");
    cotton.lib.MyDatabase.FastExec("update t_pricelist set status = '5' where id = '" + id + "'");
    cotton.lib.MyDatabase.FastExec("update t_proposal_pricelist set status = '5' where id in(select id_proposal from t_pricelist_detail where id_pricelist = '"+id+"')");
    cotton.lib.MyDatabase.FastExec("insert into h_activity([table],id_ref,[action],created_by,created_date,on_behalf) values ('t_pricelist','"+id+"','Publish Pricelist','"+Session["id"].ToString()+"',getdate(),'" + Session["id_on_behalf"].ToString() + "')");
    cotton.lib.MyDatabase.FastExec("insert into t_pricelist_items(id_pricelist,id_proposal_detail,delisted_date) select '"+id+"',t_proposal_detail.id,m_delisted.delisted_date from t_proposal_detail left join m_delisted on m_delisted.sku = t_proposal_detail.sku where status = '5' and divcd in(select division_code from m_division where m_division.id = '"+id_division+"')");
    cotton.lib.MyDatabase db = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = db.Query("select t_pricelist.id,pricelist_number,name,email from t_pricelist inner join m_user on m_user.email <> '' and id_job_title in(3,9,11,16,17,19,21,7,23) and m_user.status = '5' where t_pricelist.id = '"+id+"'");
    String number = "";
    while (reader.Read()) {
        String body = "<style>td{border:1px solid #000;}</style>";
        body += "Dear " + reader["name"].ToString()+"<br/>";
        body += "<br/>";
        body += "Pricelist "+reader["pricelist_number"].ToString()+" has been published.<br/>";
        number = reader["pricelist_number"].ToString();
        body += "<br/>";
        body += "Please click link below for detail<br/>";
        body += "<br/>";
        body += "<a href='"+cotton.lib.EmailClass.getUrl()+"'>" + cotton.lib.EmailClass.getUrl() + "</a><br/>";
        body += "Disclaimer<br/>";
        body += "Please DO NOT REPLY to this system generated mailing";
        cotton.lib.EmailClass.SendMail(reader["email"].ToString(), "[Cotton] Pricelist "+number+" Has been Published", body);
    }
    reader.Close();
    db.Close();
%>