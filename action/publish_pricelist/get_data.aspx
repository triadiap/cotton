﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select *,convert(varchar,pricelist_date,103) tgl from t_pricelist inner join r_proposal_status on r_proposal_status.id = t_pricelist.status where t_pricelist.status != '5' order by t_pricelist.id desc");
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["pricelist_number"] + "</td>");
        Response.Write("<td>" + reader["tgl"] + "</td>");
        Response.Write("<td>" + reader["sp_status"] + "</td>");
        Response.Write("<td style='text-align:center'>");
        Response.Write("<button class='btn btn-primary btn-xs' data-toggle='tooltip' title='Download' onclick='download("+reader["id"].ToString()+","+reader["id_division"].ToString()+")'><i class='fa fa-download'></i></button>&nbsp;");
        Response.Write("<button class='btn btn-primary btn-xs' data-toggle='tooltip' title='Publish' onclick='publish(" + reader["id"].ToString() + "," + reader["id_division"].ToString() + ")'><i class='fa fa-send'></i></button>");
        Response.Write("</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>