﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select *,convert(varchar,t_proposal_pricelist.created_date,103) tanggal,sp_status from t_proposal_pricelist inner join r_proposal_status on r_proposal_status.id = status inner join t_approval on t_approval.id_proposal = t_proposal_pricelist.id where id_approver in("+Session["id_on_behalf"].ToString()+") and t_approval.status_approve = '0' order by t_proposal_pricelist.created_date desc");
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + reader["proposal_number"] + "</td>");
        Response.Write("<td>" + reader["subject"] + "</td>");
        Response.Write("<td>" + reader["tanggal"] + "</td>");
        Response.Write("<td>" + reader["sp_status"] + "</td>");
        cotton.lib.MyDatabase db2 = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader2 = db2.Query("select top 1 name from dbo.h_approval inner join m_user on approved_by = m_user.id where id_proposal = '"+reader["id"].ToString()+"' and approval_level <= (select min(app_level) from t_approval where id_proposal='"+reader["id"].ToString()+"' and status_approve = 0) and approved_by <> '"+Session["id"].ToString()+"'  order by h_approval.id desc");
        System.Collections.Generic.List<String> nm = new System.Collections.Generic.List<String>();
        while (reader2.Read()) {
            nm.Add(reader2["name"].ToString());
        }
        reader2.Close();
        Response.Write("<td>"+String.Join(",",nm)+"</td>");
        db2.Close();
        Response.Write("<td style='text-align:center'>");
        if (reader["status"].ToString() == "2" || reader["status"].ToString() == "3")
        {
            Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Edit' onclick='do_edit(" + reader["id"].ToString() + ","+reader["status"].ToString()+")'><i class='fa fa-pencil'></i></a>");
        }
        Response.Write("</td>");
        Response.Write("</tr>");
    }
    reader.Close();
    database.Close();
%>