<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
	String sqlQuery = "";
	int channelnum = 1;
    cotton.lib.MyDatabase databaseChannel = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader readerChannel = databaseChannel.Query("select * from m_channel order by id");
    sqlQuery = "select distinct a.sku_code, ";
	while(readerChannel.Read()){
		sqlQuery += "(select count(id) from m_mapping_exc_sku where sku_code=a.sku_code and id_channel='" + readerChannel["id"].ToString() + "') as channel" + channelnum + ", ";
		channelnum=channelnum+1 ;
	}
    readerChannel.Close();
    databaseChannel.Close();
	
	sqlQuery+="from m_mapping_exc_sku a "
			+ "inner join v_ers b on b.matnr=sku_code and vmsta='z1' ";
	
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query(sqlQuery.Replace(", from"," from"));
	int num = 1;
    while (reader.Read())
    {
        Response.Write("<tr>");
        Response.Write("<td>" + num.ToString().Trim() + "</td>");
        Response.Write("<td>" + reader["sku_code"] + "</td>");
        int channelnum2 = 1;
		while (channelnum2 < channelnum)
		{
			if(reader["channel"+channelnum2].ToString().Trim() == "0")
				Response.Write("<td></td>");	
			else
				Response.Write("<td style='text-align:center;'>X</td>");	
			channelnum2=channelnum2 + 1;
		}
		Response.Write("<td>");
        Response.Write("<a class='btn btn-xs btn-primary' data-toggle='tooltip' title='Edit' onclick='do_edit("+reader["sku_code"].ToString()+")'><i class='fa fa-pencil'></i></a>");
        Response.Write("&nbsp;<a class='btn btn-xs btn-danger' data-toggle='tooltip' title='Delete' onclick='do_delete("+reader["sku_code"].ToString()+",\""+reader["sku_code"]+"\")'><i class='fa fa-trash'></i></a>");
        Response.Write("</td>");
        Response.Write("</tr>");
		num=num+1;
    }
    reader.Close();
    database.Close();
%>