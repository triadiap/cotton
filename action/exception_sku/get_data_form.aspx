<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
	String select_option = Request.Form["select_option"].ToString();
    String id = Request.Form["id"].ToString();
    String sqlQuery = "";
	int channelnum = 1;
    cotton.lib.MyDatabase databaseChannel = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader readerChannel = databaseChannel.Query("select * from m_channel order by id");
    sqlQuery = "select distinct a.sku_code, ";
	while(readerChannel.Read()){
		sqlQuery += "(select count(id) from m_mapping_exc_sku where sku_code=a.sku_code and id_channel='" + readerChannel["id"].ToString() + "') as channel" + channelnum + ", ";
		channelnum=channelnum+1 ;
	}
    readerChannel.Close();
    databaseChannel.Close();
	
	sqlQuery+="from m_mapping_exc_sku a "
			+ "inner join v_ers b on b.matnr=sku_code and vmsta='z1' ";
	
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query(sqlQuery.Replace(", from"," from"));
	int num = 1;
	while (reader.Read())
	{
		if(select_option=="edit"){
			Response.Write("{");
			Response.Write("'sku_code':'" + reader["sku_code"] + "',");
			Response.Write("}");
		}else if(select_option=="id_channel"){
			cotton.lib.MyDatabase databaseChannelCB = new cotton.lib.MyDatabase();
			System.Data.SqlClient.SqlDataReader readerChannelCB = databaseChannelCB.Query("select * from m_channel order by id");
			int channelnum2 = 1;
			while(readerChannelCB.Read() && channelnum2 < channelnum){
				if(reader["channel"+channelnum2].ToString().Trim() == "0"){
					Response.Write("<div class='checkbox'><label><input type='checkbox' id='id_channel"+ readerChannelCB["id"].ToString().Trim() +"'> " + readerChannelCB["channel"].ToString().Trim() + "</label></div>");
				}else{
					Response.Write("<div class='checkbox'><label><input type='checkbox' id='id_channel"+ readerChannelCB["id"].ToString().Trim() +"' checked> " + readerChannelCB["channel"].ToString().Trim() + "</label></div>");
				}
				channelnum2=channelnum2+1;
			}
			readerChannelCB.Close();
			databaseChannelCB.Close();
		}
	}
    reader.Close();
    database.Close();
%>