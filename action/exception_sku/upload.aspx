﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    foreach (String f in Request.Files.AllKeys) {
        var file = Request.Files[f];
        String []tmp = file.FileName.Split('\\');
        String name = tmp[tmp.Length - 1];
        file.SaveAs(Server.MapPath("~/Files/" + name));

        System.IO.FileInfo newFile = new System.IO.FileInfo(Server.MapPath("~/Files/" + name));
        OfficeOpenXml.ExcelPackage pck = new OfficeOpenXml.ExcelPackage(newFile);

        var ws = pck.Workbook.Worksheets[1];
        int index = 2;
        while (ws.Cells["A" + index].Value != null)
        {
            string sku_code = ws.Cells["A" + index].Value.ToString();
            string channel = "";
            if (ws.Cells["B" + index].Value == null)
            {
                channel = "";
            }
            else
            {
                channel = ws.Cells["B" + index].Value.ToString();
            }

            if (sku_code != "" && channel != "")
            {
                cotton.lib.MyDatabase databaseChannel = new cotton.lib.MyDatabase();
                System.Data.SqlClient.SqlDataReader readerChannel = databaseChannel.Query("select * from m_channel WHERE channel='" + channel + "' order by id");
                String id_channel = "";
                while (readerChannel.Read())
                {
                    id_channel = readerChannel["id"].ToString();
                }
                readerChannel.Close();
                databaseChannel.Close();
                if (id_channel != "")
                {
                    String sqlInsert = "";
                    sqlInsert = "Delete from m_mapping_exc_sku where sku_code = '" + sku_code + "' AND id_channel = '" + id_channel + "' ;";
                    sqlInsert += "insert  into m_mapping_exc_sku(sku_code, id_channel,created_by,created_date) values ('" + sku_code + "', '" + id_channel + "','" + Session["id"].ToString() + "',getdate());";
                    cotton.lib.MyDatabase.FastExec(sqlInsert);
                }
            }
            index++;
        }

        Response.Write(file.FileName);
    }
%>