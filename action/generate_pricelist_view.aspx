﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    System.Data.SqlClient.SqlDataReader reader = database.Query("select * from r_generate_pricelist");
    String Select = "t_pricelist.id,t_pricelist_detail.id_pricelist";
    while (reader.Read())
    {
        Select += ",(" + reader["value"] + ") " + reader["kolom_name"];
    }
    reader.Close();
    database.Close();
    String sql = "alter VIEW v_pricelist as select  " + Select
        + " from t_pricelist"
        + " inner join t_pricelist_items on t_pricelist_items.id_pricelist = t_pricelist.id"
        + " inner join t_proposal_detail on t_proposal_detail.id = t_pricelist_items.id_proposal_detail"
        + " inner join t_proposal_channel on t_proposal_channel.id_proposal_detail = t_proposal_detail.id"
        + " inner join m_callendar on m_callendar.id = id_callendar"
        + " inner join m_area on m_area.id = id_area"
        + " inner join m_channel on m_channel.id = t_proposal_channel.id_channel"
        + " inner join m_proposal_remarks on m_proposal_remarks.id = t_proposal_detail.remark"
        +"  inner join t_pricelist_detail on t_pricelist_detail.id_proposal  = t_proposal_detail.id_proposal"
        +"  left join m_mapping_channel_margin on m_mapping_channel_margin.id_channel = m_channel.id and m_mapping_channel_margin.id_division = t_pricelist.id_division"
        +" where t_proposal_detail.status in(5)";
//    Response.Write(sql);
//    Response.End();
    cotton.lib.MyDatabase.FastExec(sql);
    sql = "alter VIEW v_pricelist_all as select " + Select
        + " from t_proposal_pricelist"
        + " inner join t_proposal_detail on t_proposal_detail.id_proposal = t_proposal_pricelist.id"
        + " inner join t_proposal_channel on id_proposal_detail = t_proposal_detail.id"
        + " inner join m_callendar on m_callendar.id = id_callendar"
        + " inner join m_area on m_area.id = id_area"
        + " inner join m_channel on m_channel.id = t_proposal_channel.id_channel"
        + " inner join t_pricelist_detail on t_pricelist_detail.id_proposal = t_proposal_pricelist.id"
        + " inner join m_proposal_remarks on m_proposal_remarks.id = t_proposal_detail.remark"
        +"  inner join t_pricelist on t_pricelist.id = t_pricelist_detail.id_pricelist"
        +"  left join m_mapping_channel_margin on m_mapping_channel_margin.id_channel = m_channel.id and m_mapping_channel_margin.id_division = t_pricelist.id_division"
        + " where t_proposal_detail.status in(4,5)";
    cotton.lib.MyDatabase.FastExec(sql);
%>