﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<% 

    using (OfficeOpenXml.ExcelPackage pck = new OfficeOpenXml.ExcelPackage())
    {
        String sqlQuery = "select distinct id_job_title, job_title, segments, brncd,brnnm,seccd,secnm, "
             + "(select count(distinct m_user_segments.id_user) from m_user_segments join m_user on m_user.id=id_user "
             + "    where segments=a.segments and m_user.id_job_title=b.id_job_title) as count_segments "
             + "from m_user_segments a "
             + "left join m_user b on b.id=a.id_user "
             + "join m_job_title c on c.id=b.id_job_title "
             + "join m_hierarchy d on d.seccd=a.segments "
             + "order by segments";
        OfficeOpenXml.ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");
        ws.Cells["B2"].Value = "Segment Code";
        ws.Cells["C2"].Value = "Segment Name";
        ws.Cells["D2"].Value = "Brand Code";
        ws.Cells["E2"].Value = "Brand Name";
        ws.Cells["F2"].Value = "Job Title";
        ws.Cells["G2"].Value = "User Name";

        ws.Cells["B2:G2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        ws.Cells["B2:G2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightYellow);
        ws.Cells["B2:G2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        ws.Cells["B2:G2"].Style.Font.Bold = true;
        int rowIndex = 2;
        cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
        System.Data.SqlClient.SqlDataReader reader = database.Query(sqlQuery);
        while (reader.Read()) {
            if (Convert.ToInt32(reader["count_segments"].ToString()) > 1)
            {
                rowIndex++;
                ws.Cells["B" + rowIndex].Value = reader["seccd"].ToString();
                ws.Cells["C" + rowIndex].Value = reader["secnm"].ToString();
                ws.Cells["D" + rowIndex].Value = reader["brncd"].ToString();
                ws.Cells["E" + rowIndex].Value = reader["brnnm"].ToString();
                ws.Cells["F" + rowIndex].Value = reader["job_title"].ToString();
                String name = "";
                cotton.lib.MyDatabase databaseName = new cotton.lib.MyDatabase();
                System.Data.SqlClient.SqlDataReader readerName = databaseName.Query("select distinct name from m_user where id in (select id_user from m_user_segments where segments = '" + reader["seccd"] + "') and id_job_title = '" + reader["id_job_title"] + "'");
                while (readerName.Read())
                {
                    name = name + readerName["name"].ToString().Trim() + ",";
                }
                readerName.Close();
                databaseName.Close();
                if (name.Length > 0)
                {
                    name = name.Substring(0, name.Length - 1);
                }
                ws.Cells["G" + rowIndex].Value = name;
            }
        }
        ws.Cells["B2:G"+rowIndex].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["B2:G"+rowIndex].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["B2:G"+rowIndex].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["B2:G"+rowIndex].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        ws.Cells["B2:G"+rowIndex].AutoFitColumns();
        reader.Close();
        database.Close();
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;  filename=Double Role Segments.xlsx");
        Response.BinaryWrite(pck.GetAsByteArray());
        Response.End();
    }
%>