<%@ Page Language="C#" AutoEventWireup="true" %>
<% 
    cotton.lib.MyDatabase database = new cotton.lib.MyDatabase();
    String sqlQuery = "";
    sqlQuery = "select distinct id_job_title, job_title, segments, brncd,brnnm,seccd,secnm, "
             + "(select count(distinct m_user_segments.id_user) from m_user_segments join m_user on m_user.id=id_user "
             + "    where segments=a.segments and m_user.id_job_title=b.id_job_title) as count_segments "
             + "from m_user_segments a "
             + "left join m_user b on b.id=a.id_user "
             + "join m_job_title c on c.id=b.id_job_title "
             + "join m_hierarchy d on d.seccd=a.segments "
             + "order by segments";

    System.Data.SqlClient.SqlDataReader reader = database.Query(sqlQuery);
    Response.Write("<thead id='table-head'>");
    Response.Write("<tr bgcolor='#87AFC6'>");
    Response.Write("<th>Segment Code</th>");
    Response.Write("<th>Segment Name</th>");
    Response.Write("<th>Brand Code</th>");
    Response.Write("<th>Brand Name</th>");
    Response.Write("<th>Job Title</th>");
    Response.Write("<th>User Name</th>");
    Response.Write("</tr>");
    Response.Write("</thead>");
    Response.Write("<tbody id='table-body'>");
    while (reader.Read()){
        if(Convert.ToInt32(reader["count_segments"].ToString().Trim()) >= 2){
            Response.Write("<tr>");
            Response.Write("<td>" + reader["seccd"] + "</td>");
            Response.Write("<td>" + reader["secnm"] + "</td>");
            Response.Write("<td>" + reader["brncd"] + "</td>");
            Response.Write("<td>" + reader["brnnm"] + "</td>");
            Response.Write("<td>" + reader["job_title"] + "</td>");
            Response.Write("<td>");
            
            cotton.lib.MyDatabase databaseName = new cotton.lib.MyDatabase();
            System.Data.SqlClient.SqlDataReader readerName = databaseName.Query("select distinct name from m_user where id in (select id_user from m_user_segments where segments = '" + reader["seccd"] + "') and id_job_title = '" + reader["id_job_title"] + "'");
            while (readerName.Read()){
                Response.Write(readerName["name"].ToString().Trim() + ",");
            }
            readerName.Close();
            databaseName.Close();       

            Response.Write("</td>");
            Response.Write("</tr>");
        }
    }
    Response.Write("</tbody>");
    reader.Close();
    database.Close();
%>